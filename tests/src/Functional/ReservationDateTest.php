<?php

namespace Drupal\reservation\Tests\Functional;

use Drupal\reservation\Entity\ReservationDate;
use Drupal\Tests\BrowserTestBase;

/**
 * @todo .
 *
 * @ingroup reservation
 *
 * @group reservation
 * @group examples
 */
class ReservationDateTest extends BrowserTestBase {

  /**
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Test all paths exposed by the module, by permission.
   */
  public function testReservationDate() {
    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties(['type' => 'refuge']);
    foreach ($nodes as $node) {
      for ($i = 1; $i <= rand(20, 50); $i++) {
        // Generate a reservationDate so that we can test the paths against it.
        $reservationDate = ReservationDate::create(
          [
            'nid' => $node->id(),
            'day' => randomData(rand(1, 31)),
            'month' => randomData(rand(1, 12)),
            'year' => rand(2017, 2018),
            'Statut' => rand(1, 100) > 20 ? TRUE : FALSE,
            'horaire' => rand(1, 100) > 10 ? TRUE : FALSE,
          ]
        );
        $reservationDate->save();
      }
    }
  }

  /**
   *
   */
  protected function randomData($rand) {
    $array = [
      1 => '01',
      2 => '02',
      3 => '03',
      4 => '04',
      5 => '05',
      6 => '06',
      7 => '07',
      8 => '08',
      9 => '09',
      10 => '10',
      11 => '11',
      12 => '12',
      13 => '13',
      14 => '14',
      15 => '15',
      16 => '16',
      17 => '17',
      18 => '18',
      19 => '19',
      20 => '20',
      21 => '21',
      22 => '22',
      23 => '23',
      24 => '24',
      25 => '25',
      26 => '26',
      27 => '27',
      28 => '28',
      29 => '29',
      30 => '30',
      31 => '31',
    ];

    return $array[$rand];
  }

  /**
   * Data provider for testPaths.
   *
   * @param int $reservationDate_id
   *   The id of an existing ReservationDate entity.
   *
   * @return array
   *   Nested array of testing data. Arranged like this:
   *   - Expected response code.
   *   - Path to request.
   *   - Permission for the user.
   */
  protected function providerTestPaths($reservationDate_id) {
    return [
      [
        200,
        '/reservationdate/' . $reservationDate_id,
        'view reservationDate entity',
      ],
      [
        403,
        '/reservationdate/' . $reservationDate_id,
        '',
      ],
      [
        200,
        '/reservationdate/list',
        'view reservationDate entity',
      ],
      [
        403,
        '/reservationdate/list',
        '',
      ],
      [
        200,
        '/reservationdate/add',
        'add reservationDate entity',
      ],
      [
        403,
        '/reservationdate/add',
        '',
      ],
      [
        200,
        '/reservationdate/' . $reservationDate_id . '/edit',
        'edit reservationDate entity',
      ],
      [
        403,
        '/reservationdate/' . $reservationDate_id . '/edit',
        '',
      ],
      [
        200,
        '/reservationDate/' . $reservationDate_id . '/delete',
        'delete reservationDate entity',
      ],
      [
        403,
        '/reservationDate/' . $reservationDate_id . '/delete',
        '',
      ],
      [
        200,
        'admin/structure/reservationdate_settings',
        'administer reservationDate entity',
      ],
      [
        403,
        'admin/structure/reservationdate_settings',
        '',
      ],
    ];
  }

}
