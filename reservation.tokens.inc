<?php

/**
 * @file
 * Builds placeholder replacement tokens for node-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;

/**
 * Implements hook_token_info().
 */
function reservation_token_info() {
}

/**
 * Implements hook_tokens().
 */
function reservation_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if (!empty($data['reservation_demande']) && !empty($data['webform_submission'])) {
    if ($type == 'reservation_demande') {
      /** @var \Drupal\reservation\Entity\ReservationDemande $reservationDemande */
      $reservationDemande = $data['reservation_demande'];
      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'datecreneau':
            $replacements[$original] = $reservationDemande->getDateCreneau();
            break;

          case 'datedemande':
            $replacements[$original] = $reservationDemande->getCreatedFormat();
            break;

          case 'ressource':
            $replacements[$original] = $reservationDemande->getTitleNode();
            break;

          case 'webform_token':
            $replacements[$original] = $reservationDemande->getWebformToken();
            break;

          case 'cancel_url':
            $token = $reservationDemande->getWebformToken();
            $urlOptions = ['absolute' => TRUE];
            $url = Url::fromRoute('reservation.demande.user.cancel', ['token' => $token], $urlOptions);
            $replacements[$original] = $url->toString();
            break;
        }
      }
    }
    else {
      if ($type == 'webform_submission') {
        /** @var \Drupal\webform\Entity\WebformSubmission $webformSubmission */
        $webformSubmission = $data['webform_submission'];
        $webformSubmissionData = $webformSubmission->getData();
        if (!empty($webformSubmissionData)) {
          foreach ($tokens as $name => $original) {
            switch ($name) {
              case 'values:webform_reservation_nb_accompagnants':
              case 'values:webform_reservation_nb_accompagnants:reservation-accompagnateur-select':
                if (empty($webformSubmissionData['webform_reservation_nb_accompagnants'])) {
                  $replacements[$original] = '0';
                }
                break;
            }
          }
        }
      }
    }
  }
  return $replacements;
}
