(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.reservation_caution_element = {

    attach: function (context) {

      let cautionInputId = '#edit-reservation-caution';
      once('reservation-caution', cautionInputId, context).forEach(function () { // to avoid double binding

        function toggleElement(selector, state) {
          if (state) {
            $(selector).show();
          }
          else {
            $(selector).hide();
          }
        }

        function toggleCautionForm(state) {
          toggleElement('#edit-reservation-caution-form', state);
        }

        function toggleCarteBancaireLink(state) {
          toggleElement('#edit-reservation-caution-carte-bancaire-link', state);
        }

        function toggleCautionCarteBancaireOk(state) {
          toggleElement('#edit-reservation-caution-carte-bancaire-ok', state);
        }

        let form = $(cautionInputId).closest("form");

        function showNextButton() {
          $(form).find('button[data-drupal-selector="edit-wizard-next"]').show();
          $(form).find('button[data-drupal-selector="edit-preview-next"]').show();
          $(form).find('button[data-drupal-selector="edit-submit"]').show();
        }

        function toggleNextButtonDisabled(state) {
          $(form).find('button[data-drupal-selector="edit-wizard-next"]').prop('disabled', state);
          $(form).find('button[data-drupal-selector="edit-preview-next"]').prop('disabled', state);
          $(form).find('button[data-drupal-selector="edit-submit"]').prop('disabled', state);
          $(form).find('button[data-drupal-selector="edit-actions-submit"]').prop('disabled', state);
          showNextButton(); /* display:none by default in CSS */
        }

        function enableNextButton() {
          toggleNextButtonDisabled(false);
        }

        function disableNextButton() {
          toggleNextButtonDisabled(true);
        }

        function disablePreviousButton() {
          $(form).find('button[data-drupal-selector="edit-wizard-prev"]').prop('disabled', true);
        }

        function toggleNextButton(state) {
          if (state) {
            enableNextButton();
          }
          else {
            disableNextButton();
          }
        }

        function clickNextButton() {
          let next = $(form).find('button[data-drupal-selector="edit-wizard-next"]');
          if (next.length > 0) {
            next.trigger('click');
          }
          else {
            let preview = $(form).find('button[data-drupal-selector="edit-preview-next"]');
            if (preview.length > 0) {
              next.trigger('click');
            }
            else {
              let submit = $(form).find('button[data-drupal-selector="edit-submit"]');
              if (submit.length > 0) {
                submit.trigger('click');
              }
              else {
                let submit = $(form).find('button[data-drupal-selector="edit-actions-submit"]');
                if (submit.length > 0) {
                  submit.trigger('click');
                }
              }
            }
          }
        }

        // Allows setting margins
        $('#edit-reservation-caution-carte-bancaire-link').css('display', 'inline-block');

        let cautionInput = $(cautionInputId);
        let originalCautionValue = cautionInput.val();
        toggleNextButton(originalCautionValue > '');

        let cautionTypeCheque = $('#edit-reservation-caution-cheque');
        let cautionTypeCB = $('#edit-reservation-caution-carte-bancaire');

        if (originalCautionValue === 'carte') { // Caution CB déjà effectuée
          toggleCautionForm(false); // Should be alredy hidden by CSS
          toggleCautionCarteBancaireOk(true);
          $('#edit-reservation-caution-cheque').prop('disabled', true);
          $('#edit-reservation-caution-carte-bancaire').prop('checked', true);
        }
        else if (originalCautionValue === 'cheque') {
          $('#edit-reservation-caution-cheque').prop('checked', true);
          toggleCautionForm(true);
        }
        else {
          if (cautionTypeCheque && cautionTypeCB) {
            let originalCautionType = cautionTypeCheque.is(':checked') ? 'cheque' : 'carte';
            toggleCarteBancaireLink(originalCautionType === 'carte');
          }
          else { // Si aucun bouton radio, alors on est en mode CB par default
            toggleCarteBancaireLink(true);
          }
          toggleCautionForm(true);
        }

        if (cautionTypeCheque && cautionTypeCB) {
          cautionTypeCheque.change(function () {
            if (cautionInput.val() === '') {
              cautionInput.val('cheque');
            }
            toggleCarteBancaireLink(false);
            enableNextButton();
          });
          cautionTypeCB.change(function () {
            if (cautionInput.val() === '' || cautionInput.val() === 'cheque') {
              cautionInput.val(''); // Remise à zéro en attente retour Ogone
              toggleCarteBancaireLink(true);
              disableNextButton();
            }
          });
        }
        else {
          toggleCarteBancaireLink(false);
          enableNextButton();
        }

        let cautionDialog;
        $(window).on('dialog:aftercreate', function (event, dialog, element, settings) {
          cautionDialog = dialog;
          $('.ui-widget-overlay').bind('click', function () {
            $(this).siblings('.ui-dialog').find('.ui-dialog-content').dialog('close');
          });
        });

        let returnStatus = '';
        $(window).on('dialog:afterclose', (event, dialog, element) => {
          console.log(event, dialog, element);
          if (returnStatus === '') {
            // L'utilisateur a fermé la popup manuellement avant terme.
            let dmid = $('#webform-reservation-demande-id').val();
            let token = $('#edit-reservation-caution-token').val();
            $.ajax({
              url: '/reservation/demande/caution/decline_on_close_modal/' + dmid + '/' + token,
              type: 'GET',
              async: true,
              complete: (data) => {
                console.log(data);
              }
            });
          }
        });

        function handleCautionBancaireEvent(e) {
          console.log(e.detail);
          try {
            returnStatus = e.detail.status;
            cautionDialog.close(e);
          } catch (err) {
            console.log(err);
          }
          let status = e.detail.status;
          if (status === 'accept') {
            // On renseigne la valeur du type de caution choisi
            cautionInput.val('carte');
            // On désactive le radio bouton "chèque" TODO nécessaire ?
            $('#edit-reservation-caution-cheque').prop('disabled', true);
            // On masque les radios boutons et le bouton caution CB
            toggleCautionForm(false);
            toggleCarteBancaireLink(false);
            // On affiche le message de confirmation caution CB
            toggleCautionCarteBancaireOk(true);
            // On desactive le bouton "Précédent"
            disablePreviousButton();
            // On affiche le bouton "Suivant" et on l'actionne
            enableNextButton();
            clickNextButton();
          }
        }

        window.document.addEventListener(
          'cautionBancaireEvent', handleCautionBancaireEvent, false);

      });
    }
  };
})(jQuery, Drupal, drupalSettings);
