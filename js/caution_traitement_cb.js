(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.reservation_caution_traitement_cb = {

    attach: function (context) {

      var cautionTraitementCarteBancaireFormId = '#demande_caution_traitement_carte_bancaire_form';
      $(cautionTraitementCarteBancaireFormId, context).each(function () { // to avoid double binding

        var form = this;

        var etat = $(form).find('input[name=etat]').val();

        var data = {status: etat};
        var event = new CustomEvent('cautionBancaireEvent', {detail: data});
        window.parent.document.dispatchEvent(event);

      });
    }
  };
})(jQuery, Drupal, drupalSettings);
