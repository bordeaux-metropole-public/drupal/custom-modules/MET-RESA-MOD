(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.reservation_caution_autosubmit = {

    attach: function (context) {

      var cautionCarteBancaireFormId = '#demande_caution_bancaire_ng_form';
      $(cautionCarteBancaireFormId, context).each(function () { // to avoid double binding

        var form = this;

        var cb_start = $(form).find('button[data-drupal-selector="caution-cb-start"]');
        if (cb_start.length > 0) { // button for debugging purpose
          cb_start.click(function () {
            form.submit();
          });
        }
        else {
          form.submit();
        }

      });
    }
  };
})(jQuery, Drupal, drupalSettings);
