(function ($, Drupal) {

  Drupal.behaviors.reservation_ancrage = {

    attach: function (context) {

      if (document.getElementById("block-webform-reservation-ancrage")) {
        location.hash = '#block-webform-reservation-ancrage';
      }
      else if (document.getElementById("webform-reservation-ancrage")) {
        location.hash = '#webform-reservation-ancrage';
      }

    }
  }
})(jQuery, Drupal);
