(function ($, Drupal) {

  Drupal.behaviors.reservation_place_element = {

    attach: function (context) {
      $.ajax({
        url: '/reservation/place',
        dataType: "json",
        async: false,
        success: function (data) {
          $('#reservation-accompagnateur-select > option').each(function () {
            if ($(this).text() > data) {
              $("#reservation-accompagnateur-select option[value='" + $(this).val() + "']").remove();
            }
          });
        }
      });
    }
  };
})(jQuery, Drupal);
