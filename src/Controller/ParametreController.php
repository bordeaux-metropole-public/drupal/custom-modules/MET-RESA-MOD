<?php

namespace Drupal\reservation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\reservation\EventSubscriber\ReservationEntitySubscriber;
use Drupal\reservation\Form\ParametreNotificationFilterForm;
use Drupal\reservation\Form\ParametreNotificationForm;
use Drupal\reservation\Service\ReservationNotificationServices;
use Drupal\reservation\Service\ReservationRessourceServices;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ParametreController.
 *
 * @package Drupal\reservation\Controller
 */
class ParametreController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * @var \Drupal\reservation\Service\ReservationRessourceServices
   */
  protected $reservationRessource;

  /**
   * @var \Drupal\reservation\Service\ReservationNotificationServices
   */
  protected $reservationNotification;

  /**
   * ParametreController constructor.
   *
   * @param \Drupal\reservation\Service\ReservationRessourceServices $reservationRessource
   * @param \Drupal\reservation\Service\ReservationNotificationServices $reservationNotification
   */
  public function __construct(ReservationRessourceServices $reservationRessource, ReservationNotificationServices $reservationNotification) {
    $this->reservationRessource = $reservationRessource;
    $this->reservationNotification = $reservationNotification;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return ParametreController
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('reservation.ressource'),
      $container->get('reservation.notification')
    );
  }

  /**
   * Méthode permettant la génération de la page de paramétrage des
   * notifications URL :
   * /admin/reservation/parametre/notification/{nid}/attente
   *
   * @param string $nid
   * @param string $type_email
   *
   * @return mixed
   */
  public function notification($nid = 'nid', $type_email = 'type_email') {
    $ressources = $this->reservationRessource->getAvailableRessources();

    if (empty($ressources)) {
      $this->messenger()
        ->addMessage('!! Pas de ressource paramétré !!', 'error');
      return $this->redirect('reservation.parametre.ressource');
    }

    if ($nid === 'nid' || $type_email === 'type_email') {
      $nid = key($ressources);
      $statutstabs = $this->reservationNotification->generateStatutsTabs($nid);
      $type_email = key($statutstabs);

      return $this->redirect('reservation.parametre.notification', [
        'nid' => $nid,
        'type_email' => $type_email,
      ]);
    }

    $statutstabs = $this->reservationNotification->generateStatutsTabs($nid, $type_email);
    $notification = $this->reservationNotification->getNotificationByNidType($nid, $type_email);
    if (empty($notification)) {
      $notification = ReservationEntitySubscriber::createRessourceNotificationByTypeEmail($nid, $type_email);
    }

    $form_filter = new ParametreNotificationFilterForm($ressources, $statutstabs, $type_email, $nid);
    $form_email = new ParametreNotificationForm($notification->id());

    return [
      '#theme' => 'template_parametre_notification',
      '#form_email' => \Drupal::formBuilder()
        ->getForm($form_email, $notification),
      '#form_filter' => \Drupal::formBuilder()->getForm($form_filter),
      '#tabs' => $statutstabs,
      '#$type_email' => $type_email,
      '#nid' => $nid,
    ];
  }

}
