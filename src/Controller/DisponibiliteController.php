<?php

namespace Drupal\reservation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\reservation\Form\DisponibiliteCautionForm;
use Drupal\reservation\Form\DisponibiliteDateForm;
use Drupal\reservation\Form\DisponibiliteDelayForm;
use Drupal\reservation\Form\DisponibiliteHoraireForm;
use Drupal\reservation\Form\DisponibiliteHoraireSelectForm;
use Drupal\reservation\Form\DisponibiliteModeForm;
use Drupal\reservation\Form\DisponibiliteUserForm;
use Drupal\reservation\ReservationConstants;
use Drupal\reservation\ReservationUtils;
use Drupal\reservation\Service\ReservationDateServices;
use Drupal\reservation\Service\ReservationDemandeServices;
use Drupal\reservation\Service\ReservationHoraireServices;
use Drupal\reservation\Service\ReservationRessourceNodeServices;
use Drupal\reservation\Service\ReservationRessourceServices;
use Drupal\reservation\Service\ReservationRessourceUserServices;
use Drupal\webform\Entity\Webform;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Ce controller permet l'ouverture des dates et horaires depuis le panneau.
 *
 * Class DisponibiliteController.
 *
 * @package Drupal\reservation\Controller
 */
class DisponibiliteController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * @var \Drupal\reservation\Service\ReservationRessourceUserServices
   */
  protected $ressourceUserServices;

  /**
   * @var \Drupal\reservation\Service\ReservationDateServices
   */
  private $dateServices;

  /**
   * @var \Drupal\reservation\Service\ReservationHoraireServices
   */
  private $horaireServices;

  /**
   * @var \Drupal\reservation\Service\ReservationRessourceServices
   */
  private $ressourceServices;

  /**
   * @var \Drupal\reservation\Service\ReservationRessourceNodeServices
   */
  private $ressourceNodeServices;

  /**
   * @var \Drupal\reservation\Service\ReservationDemandeServices
   */
  private $demandeServices;

  /**
   * DisponibiliteController constructor.
   *
   * @param \Drupal\reservation\Service\ReservationDateServices $reservationDate
   * @param \Drupal\reservation\Service\ReservationHoraireServices $reservationHoraire
   * @param \Drupal\reservation\Service\ReservationRessourceServices $reservationRessource
   * @param \Drupal\reservation\Service\ReservationRessourceNodeServices $reservationRessourceNode
   * @param \Drupal\reservation\Service\ReservationRessourceUserServices $ressourceUserServices
   * @param \Drupal\reservation\Service\ReservationDemandeServices $reservationDemande
   */
  public function __construct(ReservationDateServices $reservationDate,
                              ReservationHoraireServices $reservationHoraire,
                              ReservationRessourceServices $reservationRessource,
                              ReservationRessourceNodeServices $reservationRessourceNode,
                              ReservationRessourceUserServices $ressourceUserServices,
                              ReservationDemandeServices $reservationDemande) {
    $this->dateServices = $reservationDate;
    $this->horaireServices = $reservationHoraire;
    $this->ressourceServices = $reservationRessource;
    $this->ressourceNodeServices = $reservationRessourceNode;
    $this->ressourceUserServices = $ressourceUserServices;
    $this->demandeServices = $reservationDemande;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return DisponibiliteController
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('reservation.date'),
      $container->get('reservation.horaire'),
      $container->get('reservation.ressource'),
      $container->get('reservation.ressource.node'),
      $container->get('reservation.ressource.user'),
      $container->get('reservation.demande')
    );
  }

  /**
   * Méthode permettant la génération de la page de liste des ressources
   * disponibles à la réservation.
   *
   * URL: /admin/reservation/disponibilite/liste.
   *
   * @return array
   */
  public function liste() {
    $choix = \Drupal::request()->query->get('choix');

    $user = \Drupal::currentUser();

    if (ReservationUtils::canManageAllRessourcesAvailabilities()) {
      if ($choix === 'user') {
        $rows = $this->listeRessources($user->id());
      }
      else {
        $rows = $this->listeRessources();
        $choix = 'all';
      }
    }
    else {
      if (ReservationUtils::canManageOwnResourcesReservations()) {
        $rows = $this->listeRessources($user->id());
      }
      else {
        $rows = [];
      }
    }

    $render = [
      '#theme' => 'template_disponibilite_liste',
      '#rows' => $rows,
      '#year' => date("Y"),
    ];

    if (ReservationUtils::canManageAllRessourcesAvailabilities()) {
      $formDispoUser = new DisponibiliteUserForm();
      $render['#form'] = \Drupal::formBuilder()
        ->getForm($formDispoUser, $choix);
    }

    return $render;
  }

  /**
   * Méthode appelée pour généré les lignes de la page liste, on y retrouve :
   * le nom de la ressource, nb de dates disponibles, paramétrage
   * caution...etc... et lien pour les dates / horaires
   *
   * @param string|null $uid
   *
   * @return array
   *
   * @throws \Exception
   */
  public function listeRessources(string $uid = NULL) {
    $date_now = new \DateTime();
    $year = $date_now->format('Y');

    $ids = [];
    $dates = [];

    if ($uid) {
      $ids = $this->ressourceUserServices->getRessourceUserNodeIdsByUid($uid);
    }

    if (!empty($ids)) {
      $resourceNodes = \Drupal::entityTypeManager()
        ->getStorage(
          ReservationRessourceNodeServices::RESERVATION_RESSOURCE_NODE_ENTITY_NAME)
        ->loadMultiple($ids);
    }
    else {
      $resourceNodes = $this->ressourceNodeServices->getRessourceNodes(1);
    }

    $types = $this->ressourceServices->getTypeStatut(1);
    $webforms = Webform::loadMultiple();

    foreach ($resourceNodes as $resourceNode) {
      $nid = $resourceNode->id();
      if (($resourceNode->getStatut() == '1') && ($uid == NULL || in_array($nid, $ids))) {
        $node = $resourceNode->getNode();
        if (!empty($node)) {
          if (in_array($node->getType(), $types)) {
            $dates[$nid]["nid"] = $nid;
            $dates[$nid]["title"] = $node->getTitle();
            $dates[$nid]["count"]["disponible"] = $this->dateServices->countOpenedDates($nid, $year, '1');
            $dates[$nid]["count"]["publie"] = $this->dateServices->countPublishedDates($nid, $year, '1');

            $form = new DisponibiliteModeForm($nid);
            $dates[$nid]["mode"] = \Drupal::formBuilder()
              ->getForm($form, $resourceNode);
            $form_delay = new DisponibiliteDelayForm($nid);
            $dates[$nid]["delay"] = \Drupal::formBuilder()
              ->getForm($form_delay, $resourceNode);
            $form_caution = new DisponibiliteCautionForm($nid);
            $dates[$nid]["caution"] = \Drupal::formBuilder()
              ->getForm($form_caution, $resourceNode);

            $webform = $this->ressourceNodeServices->getWebform($nid, $webforms);
            $dates[$nid]["webform"] = $webform ? $webform->id() : NULL;
          }
          else {
            \Drupal::logger(ReservationConstants::LOG_CHANNEL)
              ->notice(t('Ressource node type inactif: %nid', ['%nid' => $nid]));
          }
        }
        else {
          \Drupal::logger(ReservationConstants::LOG_CHANNEL)
            ->warning(t('Ressource node inexistant: %nid', ['%nid' => $nid]));
        }
      }
    }

    return $dates;
  }

  /**
   * Méthode générant la page de mise en réservation des dates pour une
   * ressource
   *
   * @return array
   */
  public function date() {

    $ressourceId = \Drupal::request()->query->get('nid');
    $year = \Drupal::request()->query->get('year');

    $formDispoDateForm = new DisponibiliteDateForm();

    return [
      '#theme' => 'template_disponibilite_date',
      '#tabs_disponibilite' => $this->getDisponibilitesTabs($ressourceId, $year, "date"),
      '#form' => \Drupal::formBuilder()->getForm($formDispoDateForm),
    ];
  }

  /**
   * Méthode permettant la génération des onglets ("Disponibilité", "Saisie des
   * Dates", "Saisie des Horaires") depuis les pages : URL:
   * /admin/reservation/disponibilite/date URL:
   * /admin/reservation/disponibilite/horaire
   *
   * @param $ressourceId
   * @param $year
   * @param $activeTab
   *
   * @return array
   */
  public function getDisponibilitesTabs($ressourceId, $year, $activeTab) {
    $tabs = [];
    $tabs["disponibilite"]['key'] = "disponibilite";
    $tabs["disponibilite"]['title'] = "Disponibilité";
    $tabs["disponibilite"]['link'] = Url::fromRoute('reservation.disponibilite.index',
      ['absolute' => TRUE]);

    $tabs["date"]['key'] = "date";
    $tabs["date"]['title'] = "Saise des Dates";
    $tabs["date"]['link'] = Url::fromRoute('reservation.disponibilite.date',
      ['nid' => $ressourceId, 'year' => $year], ['absolute' => TRUE]);

    $tabs["horaire"]['key'] = "horaire";
    $tabs["horaire"]['title'] = "Saise des Horaires";
    $tabs["horaire"]['link'] = Url::fromRoute('reservation.disponibilite.horaire',
      ['nid' => $ressourceId, 'year' => $year], ['absolute' => TRUE]);

    $tabs[$activeTab]['active'] = TRUE;

    return $tabs;
  }

  /**
   * Méthode générant la page de paramétrage des dates/horaires pour une
   * ressource
   *
   * @return array
   *
   * @throws \Exception
   */
  public function horaire() {

    $dateRows = [];
    $availableResources = $this->ressourceServices->getAvailableRessources();

    $ressourceId = \Drupal::request()->query->get('nid');
    $month = \Drupal::request()->query->get('month');
    $year = \Drupal::request()->query->get('year');

    if (($ressourceId == NULL) || !ReservationUtils::isAccessibleResource($ressourceId)) {
      $ressourceId = current(array_keys($availableResources));
    }

    $currentYear = (new \DateTime())->format('Y');
    if ($month == NULL || $year == NULL) {
      $year = ($year == NULL) ? $currentYear : $year;
      if ($year >= $currentYear) {
        $reservationDate = $this->dateServices->getFirstOpenedDateByRessourceId($ressourceId, $year, TRUE);
      }
      else {
        $reservationDate = $this->dateServices->getFirstDateByRessourceId($ressourceId, $year, TRUE);
      }
      $resaDateTime = $reservationDate ? new \DateTime($reservationDate->getDate()) : new \DateTime();
      $month = $resaDateTime->format('m');
      $year = $resaDateTime->format('Y');
    }

    if ($year >= $currentYear) {
      $monthTabs = $this->dateServices->getOpenedMonths($ressourceId, $month, $year, TRUE);
    }
    else {
      $monthTabs = $this->dateServices->getAllMonths($ressourceId, $month, $year, TRUE);
    }

    $datePrefix = $year . '-' . $month;
    if ($year >= $currentYear) {
      $ressourceDates = $this->dateServices->getOpenedDates($ressourceId, $datePrefix, TRUE);
    }
    else {
      $ressourceDates = $this->dateServices->getAllDates($ressourceId, $datePrefix, TRUE);
    }

    foreach ($ressourceDates as $ressourceDate) {
      $rdid = $ressourceDate->Id();
      $horaire = $ressourceDate->getHoraire();
      $dateDT = $ressourceDate->getDateTime();

      $dateRow = [];

      $dateRow["rdid"] = $rdid;
      $dateRow["date"] = $dateDT->format('d/m/Y');

      $formDispoHoraire = new DisponibiliteHoraireForm($rdid, $ressourceId, $month, $year);
      $dateRow["form_date"] = \Drupal::formBuilder()
        ->getForm($formDispoHoraire, $ressourceDate);

      if ($horaire == TRUE) {
        DisponibiliteHoraireForm::renderHoraireItem($dateRow, $rdid, $ressourceId, $month, $year);
      }

      $dateRows[$rdid] = $dateRow;
    }

    $formSelectDispoHoraire = new DisponibiliteHoraireSelectForm();
    $formSelect = \Drupal::formBuilder()->getForm(
      $formSelectDispoHoraire, $availableResources, $ressourceId, $month, $year);

    return [
      '#theme' => 'template_disponibilite_horaire',
      '#tabs_disponibilite' => $this->getDisponibilitesTabs($ressourceId, $year, "horaire"),
      '#form_select' => $formSelect,
      '#tabs' => $monthTabs,
      '#rows' => $dateRows,
      '#nid' => $ressourceId,
      '#year' => $year,
    ];
  }

}
