<?php

namespace Drupal\reservation\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\Entity\ReservationDemandeToken;
use Drupal\reservation\ReservationConstants;
use Drupal\reservation\ReservationUtils;
use Drupal\reservation\Service\ReservationDemandeFormServices;
use Drupal\reservation\Service\ReservationDemandeServices;
use Drupal\reservation\Service\ReservationDemandeTokenServices;
use Drupal\reservation\Service\ReservationMailServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Header\UnstructuredHeader;

/**
 * Ce controller permet la gestion des demande (création, envoi de mail...)
 *
 * Class DemandeController.
 *
 * @package Drupal\reservation\Controller
 */
class DemandeController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * ReservationDemandeServices.
   *
   * @var \Drupal\reservation\Service\ReservationDemandeServices
   */
  protected $demandeServices;

  /**
   * ReservationDemandeTokenServices.
   *
   * @var \Drupal\reservation\Service\ReservationDemandeTokenServices
   */
  protected $demandeTokenServices;

  /**
   * ReservationDemandeFormServices.
   *
   * @var \Drupal\reservation\Service\ReservationDemandeFormServices
   */
  protected $demandeFormServices;

  /**
   * ReservationMailServices.
   *
   * @var \Drupal\reservation\Service\ReservationMailServices
   */
  protected $mailServices;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $reservationSettings;

  /**
   * @var mixed
   */
  protected $expirationDelay;

  /**
   * @var mixed
   */
  protected $expirationCautionDelay;

  /**
   * @var mixed
   */
  protected $expirationDelayModeManuel;

  /**
   *
   * @param \Drupal\reservation\Service\ReservationDemandeServices $demandeServices
   * @param \Drupal\reservation\Service\ReservationMailServices $mailServices
   */
  public function __construct(
    ReservationDemandeServices $demandeServices,
    ReservationDemandeTokenServices $demandeTokenServices,
    ReservationMailServices $mailServices,
    ReservationDemandeFormServices $demandeFormServices) {
    $this->demandeServices = $demandeServices;
    $this->demandeTokenServices = $demandeTokenServices;
    $this->mailServices = $mailServices;
    $this->demandeFormServices = $demandeFormServices;
    $this->reservationSettings = \Drupal::config(ReservationConstants::MODULE_SETTINGS);
    $tokenSettings = $this->reservationSettings->get('token');
    $this->expirationDelay = $tokenSettings[ReservationDemandeToken::TOKEN_TIME_LIMIT];
    $this->expirationCautionDelay = $tokenSettings[ReservationDemandeToken::TOKEN_TIME_CAUTION];
    $this->expirationDelayModeManuel = $tokenSettings[ReservationDemandeToken::TOKEN_TIME_MANUELLE];
  }

  /**
   * Méthode permettant de changer le statut d'une demande et de l'envoi d'un
   * mail (suivant le statut) appellée par DemandeForm et DemandeMultipleForm
   *
   * @param string $rdmid
   * @param string $action
   * @param string $route
   *
   * @return mixed
   */
  public function action(string $rdmid = NULL, string $action = NULL, string $route = NULL) {
    $req_params = \Drupal::requestStack()->getCurrentRequest()->query->all();
    switch ($action) {
      case ReservationDemande::STATUT_CONFIRME:
        $this->demandeServices->setStatut($rdmid, ReservationDemande::STATUT_CONFIRME);
        $this->mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_CONFIRME, TRUE);
        break;

      case ReservationDemande::STATUT_ANNULE:
        $this->demandeServices->setStatut($rdmid, ReservationDemande::STATUT_ANNULE);
        $this->mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_ANNULE, TRUE);
        break;

      case ReservationDemande::STATUT_NO_SHOW:
      case ReservationDemande::STATUT_SHOW:
        $this->demandeServices->setStatut($rdmid, $action);
        break;

      case ReservationDemande::STATUT_ATTENTE:
        $this->demandeServices->setStatut($rdmid, ReservationDemande::STATUT_ATTENTE);
        $this->mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_ATTENTE, TRUE);
        break;

      case ReservationDemande::STATUT_CAUTION:
        $this->demandeServices->setStatut($rdmid, ReservationDemande::STATUT_CAUTION);
        break;

      case ReservationDemande::STATUT_REFUSE:
        $this->demandeServices->setStatut($rdmid, ReservationDemande::STATUT_REFUSE);
        $this->mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_REFUSE);
        break;

      case ReservationDemande::STATUT_ARCHIVE:
        $this->demandeServices->setStatut($rdmid, ReservationDemande::STATUT_ARCHIVE);
        break;

      case ReservationDemande::STATUT_RAPPEL:
        $this->mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_RAPPEL, TRUE);
        break;

      case 'delete':
        return $this->redirect('reservation.demande.delete',
          array_merge([
            'rdmid' => [$rdmid],
          ], $req_params));
    }

    return $this->redirect($route, [$req_params]);
  }

  /**
   * Méthode permettant la génération d'un CSV dans le cas d'une demande simple
   * URL: /admin/reservation/demande/simple
   *
   * @return mixed
   */
  public function exportDemandeSimple() {
    $demandeSettings = $this->reservationSettings->get('demande');
    $requestParams = \Drupal::request()->query->all();
    // $resourceIds = \Drupal::request()->query->get('ressource'); // Fails on D10
    $resourceIds = $requestParams['ressource'] ?? NULL;
    // $statut = \Drupal::request()->query->get('statut'); // Fails on D10
    $statut = $requestParams['statut'] ?? NULL;
    $choix_date_demande = \Drupal::request()->query->get('choix_date_demande');
    $date_demande_debut = \Drupal::request()->query->get('date_demande_debut');
    $date_demande_fin = \Drupal::request()->query->get('date_demande_fin');
    $choix_date_creneau = \Drupal::request()->query->get('choix_date_creneau');
    $date_creneau_debut = \Drupal::request()->query->get('date_creneau_debut');
    $date_creneau_fin = \Drupal::request()->query->get('date_creneau_fin');

    $userResourceIds = ReservationUtils::getUserRessourceIds();
    if ($resourceIds == NULL) {
      if (ReservationUtils::canViewOwnDemandes() ||
        ReservationUtils::canManageOwnResourcesReservations()) {
        // Set #default_value to user accessible resources.
        $resourceIds = $userResourceIds;
      }
    }
    else {
      if (ReservationUtils::canViewOnlyOwnDemandes() ||
        ReservationUtils::canManageOnlyOwnResourcesReservations()) {
        // Filter out non user accessible resources from #default_value.
        $resourceIds = array_intersect($resourceIds, $userResourceIds);
      }
    }

    if ($statut == NULL) {
      $statut = [
        ReservationDemande::STATUT_CONFIRME,
        ReservationDemande::STATUT_ATTENTE,
        ReservationDemande::STATUT_CAUTION,
      ];
    }

    if ($choix_date_demande === NULL) {
      $choix_date_demande = TRUE;
    }

    if ($date_demande_debut == NULL) {
      $date_demande_debut_format = new \DateTime();
      $date_demande_debut_format->sub(new \DateInterval('P1M'));
      $date_demande_debut = $date_demande_debut_format->format('Y-m-d');
    }

    if ($date_demande_fin == NULL) {
      $date_demande_fin_format = new \DateTime();
      $date_demande_fin_format->add(new \DateInterval('P1D'));
      $date_demande_fin = $date_demande_fin_format->format('Y-m-d');
    }

    $lineForCsv = [
      "Ressource",
      "Date Demande",
      "Demandeur",
      "Date Créneau",
      "Statut",
      "Email",
      "Téléphone",
      "Code postal",
    ];

    if ($demandeSettings['caution'] == TRUE) {
      $lineForCsv[] = "Caution";
    }
    if ($demandeSettings['inscrit'] == TRUE) {
      $lineForCsv[] = "Participants";
    }

    $webformFieldKeys = explode('|', $demandeSettings['webform_key']);
    $webformFieldLabels = explode('|', $demandeSettings['webform_label']);
    foreach ($webformFieldLabels as $webformFieldLabel) {
      $lineForCsv[] = $webformFieldLabel;
    }

    $header = $this->demandeFormServices->getHeaderTableSimple();
    if (count($resourceIds) > 0) {
      $demandes = $this->demandeServices->getDemandesFiltered(
        $header, 0, $resourceIds, $statut, $choix_date_demande,
        $date_demande_debut, $date_demande_fin, $choix_date_creneau,
        $date_creneau_debut, $date_creneau_fin);
    }
    else {
      $demandes = [];
    }

    $linesForCsv = [];
    $linesForCsv[] = $lineForCsv;

    foreach ($demandes as $demande) {
      $lineForCsv = [
        $demande->getTitleNode(),
        $demande->getCreatedFormat(),
        $demande->getDemandeur(),
        $demande->getDateCreneau(),
        $this->demandeFormServices->getStatutFormat($demande->getStatut()),
        $demande->getEmail(),
        $demande->getTelephone(),
        $demande->getPostcode(),
      ];

      if ($demandeSettings['caution'] == TRUE) {
        if ($demande->getStatut() == ReservationDemande::STATUT_CAUTION) {
          $label = $demande->getCautionLabel() . ' ' . $demande->getCautionDelay();
        }
        else {
          $label = $demande->getCautionLabel();
        }
        $lineForCsv[] = $label;
      }

      if ($demandeSettings['inscrit'] == TRUE) {
        $lineForCsv[] = $demande->getJauge();
      }

      $webform = $demande->getWebformSubmission();
      if ($webform) {
        $dataWebform = $webform->getData();
      }
      else {
        continue;
      }
      foreach ($webformFieldKeys as $webformFieldKey) {
        $lineForCsv[] = $dataWebform[$webformFieldKey] ?? '';
      }

      $linesForCsv[] = $lineForCsv;
    }

    return $this->exportCsv($linesForCsv, 'simple');
  }

  /**
   * Méthode permettant la construction du CSV (demande simple et multiple)
   *
   * @param array $datas
   * @param $mode
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function exportCsv($lines_for_csv = [], $mode = NULL) {

    $fp = fopen('php://temp', 'w+');

    // On ajoute le BOM pour l'encodage UTF-8.
    fwrite($fp, "\xEF\xBB\xBF");

    foreach ($lines_for_csv as $line_for_csv) {
      fputcsv($fp, $line_for_csv, ";");
    }

    rewind($fp);
    $csv_data = stream_get_contents($fp);

    fclose($fp);

    $filename = 'export_demande_' . $mode . '_' . date('Ymd_His') . '.csv';

    $headers = [
      'Content-Type' => 'text/csv; charset=utf-8',
      'Content-Disposition' => 'attachment; filename="' . $filename . '"',
      'Content-Length' => strlen($csv_data),
      'Pragma' => 'no-cache',
      'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
      'Expires' => '0',
    ];

    $response = new Response();

    $response->headers->add($headers);
    $response->setContent($csv_data);

    return $response;
  }

  /**
   * Création d'une demande.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Drupal container used for dependency injection.
   *
   * @return \static
   *   Instantiated controller.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('reservation.demande'),
      $container->get('reservation.demande.token'),
      $container->get('reservation.mail'),
      $container->get('reservation.demande.form')
    );
  }

  /**
   * Méthode permettant la génération d'un CSV dans le cas d'une demande
   * multiple URL: /admin/reservation/demande/multiple
   *
   * @return mixed
   */
  public function exportDemandeMultiple() {
    $demandeSettings = $this->reservationSettings->get('demande');

    $year = \Drupal::request()->query->get('year');
    $month = \Drupal::request()->query->get('month');

    $date_now = new \DateTime();

    if ($year == NULL) {
      $year = $date_now->format('Y');
    }

    if ($month == NULL) {
      $month = $date_now->format('m');
    }

    $lineForCsv = [
      "Email",
      "Téléphone",
      "Code postal",
      "Ressource",
      "Demandeur",
      "Date Demande",
      "Date Créneau",
      "Statut",
    ];

    if ($demandeSettings['caution'] == TRUE) {
      $lineForCsv[] = "Caution";
    }
    if ($demandeSettings['inscrit'] == TRUE) {
      $lineForCsv[] = "Participants";
    }

    $webformFieldKeys = explode('|', $demandeSettings['webform_key']);
    $webformFieldLabels = explode('|', $demandeSettings['webform_label']);
    foreach ($webformFieldLabels as $webformFieldLabel) {
      $lineForCsv[] = $webformFieldLabel;
    }

    if (ReservationUtils::canManageOnlyOwnResourcesReservations()) {
      $resourceIds = ReservationUtils::getUserRessourceIds();
    }
    else {
      $resourceIds = NULL;
    }

    $linesForCsv = [];
    $linesForCsv[] = $lineForCsv;

    $header = $this->demandeFormServices->getHeaderTableMultiple();
    $demandes = $this->demandeServices->getDemandeMultipleByFilter(
      $header, 0, $year, $month, NULL, $resourceIds);

    foreach ($demandes as $demande) {
      $lineForCsv = [
        $demande->getEmail(),
        $demande->getTelephone(),
        $demande->getPostcode(),
        $demande->getTitleNode(),
        $demande->getDemandeur(),
        $demande->getCreatedFormat(),
        $demande->getDateCreneau(),
        $this->demandeFormServices->getStatutFormat($demande->getStatut()),
      ];

      if ($demandeSettings['caution'] == TRUE) {
        if ($demande->getStatut() == ReservationDemande::STATUT_CAUTION) {
          $label = $demande->getCautionLabel() . ' ' . $demande->getCautionDelay();
        }
        else {
          $label = $demande->getCautionLabel();
        }
        $lineForCsv[] = $label;
      }
      if ($demandeSettings['inscrit'] == TRUE) {
        $lineForCsv[] = $demande->getJauge();
      }

      $webform = $demande->getWebformSubmission();
      if ($webform) {
        $dataWebform = $webform->getData();
      }
      else {
        continue;
      }

      foreach ($webformFieldKeys as $webformFieldKey) {
        $lineForCsv[] = $dataWebform[$webformFieldKey] ?? '';
      }

      $linesForCsv[] = $lineForCsv;
    }

    return $this->exportCsv($linesForCsv, 'multiple');
  }

  /**
   * Affichage de formulaires de Caution : Carte Bancaire ou Chèque
   * Page /reservation/demande/caution/{token}
   *
   * @param string $etat
   * @param string $token
   *
   * @return mixed
   */
  public function cautionRedirection(string $token = NULL) {
    $demande = $this->demandeTokenServices->loadDemandeByToken($token);
    if ($demande) {
      /** @var \Drupal\reservation\Entity\ReservationRessourceNode $resourceNode */
      $resourceNode = $demande->getDate()->getReservationRessourceNode();
      $form_bancaire = \Drupal::formBuilder()
        ->getForm('Drupal\reservation\Form\DemandeCautionBancaireForm', $token, $demande);
      if ($resourceNode->isChequeAccepte()) {
        $form_cheque = \Drupal::formBuilder()
          ->getForm('Drupal\reservation\Form\DemandeCautionChequeForm', $token);
      }
      else {
        $form_cheque = NULL;
      }
      $caution = $resourceNode->getCautionMontant();

      return [
        '#theme' => 'template_demande_caution',
        '#form_bancaire' => $form_bancaire,
        '#form_cheque' => $form_cheque,
        '#caution' => $caution,
      ];
    }
    else {
      return $this->traitementToken($token);
    }
  }

  /**
   * Traitement de la demande suite au retour depuis OGONE.
   *
   * @param string $etat
   * @param string $token
   *
   * @return mixed
   */
  public function traitementToken(string $token = NULL) {
    // Test si le token utilisé est toujours actif, sinon suppression du token
    // + demande (dans la fonction checkToken)
    if ($this->demandeTokenServices->checkToken($token) === TRUE) {
      $demande = $this->demandeTokenServices->loadDemandeByToken($token);
      $this->demandeTokenServices->destroyToken($token);
      if ($demande) {
        $demande->save();

        // Envoi Mail confirmation enregistrement Réservation.
        /** @var \Drupal\reservation\Service\ReservationMailServices $mailServices */
        $mailServices = \Drupal::service('reservation.mail');
        $mailServices->generateEmailById($demande->id(), $demande->getStatut());

        if ($demande->getStatut() == ReservationDemande::STATUT_CONFIRME) {
          return $this->affichageMessage('message_confirmation_acceptee');
        }
        else {
          return $this->affichageMessage('message_confirmation_pre_reservation');
        }
      }
      else {
        return $this->affichageMessage('message_temps_depasse');
      }
    }
    else {
      $this->demandeTokenServices->destroyDemandeByToken($token);
      return $this->affichageMessage('message_temps_depasse');
    }
  }

  /**
   * Traitement de la demande suite au retour depuis OGONE.
   *
   * @param string $etat
   * @param string $token
   *
   * @return mixed
   */
  public function affichageMessage($statut) {
    return [
      '#type' => 'markup',
      '#markup' => '<h2>' . $this->reservationSettings->get('message_validation')[$statut] . '</h2>',
    ];
  }

  /**
   * Affichage de formulaires de Caution : Carte Bancaire ou Chèque
   * Page /reservation/demande/caution/cartebancaire/{demandeid}
   *
   * @param string $etat
   *
   * @return mixed
   */
  public function cautionCarteBancaireV2(string $demandeid = NULL) {
    /** @var \Drupal\reservation\Entity\ReservationDemande $demande */
    $demande = $this->demandeServices->load($demandeid);

    if ($demande) {

      /* Extend expiration delay for caution handling */
      $currentExpirationTime = $demande->getWebformTokenExpiration();
      $newExpirationTime = ReservationUtils::addExpirationDelayToDateTime(
        new \DateTime(), $this->expirationCautionDelay);
      if ($newExpirationTime > $currentExpirationTime) {
        $demande->setWebformTokenExpiration($newExpirationTime);
      }
      $demande->setStatut(ReservationDemande::STATUT_CAUTION);
      $demande->setCaution(ReservationDemande::CAUTION_CARTE);
      $demande->save();

      /** @var \Drupal\reservation\Entity\ReservationRessourceNode $resourceNode */
      $resourceNode = $demande->getDate()->getReservationRessourceNode();
      $form_bancaire = \Drupal::formBuilder()
        ->getForm('Drupal\reservation\Form\DemandeCautionBancaireV2Form', $demande);

      // Debug info.
      if (ReservationUtils::isAdmin()) {
        $caution = $resourceNode->getCautionMontant();
      }
      else {
        $caution = NULL;
      }

      return [
        '#theme' => 'template_demande_caution_ng',
        '#form_bancaire' => $form_bancaire,
        '#caution' => $caution,
      ];
    }
    else {
      return [
        '#theme' => "caution_modal_ng",
        '#message' => t('Demande de réservation inconnue ou invalide'),
        '#attached' => ['library' => ['reservation/reservation.caution_modal']],
      ];
    }
  }

  /**
   * Traitement de la demande suite au retour depuis OGONE côté client.
   *
   * @param string $dmid
   * @param string $etat
   * @param string $token
   *
   * @return array
   */
  public function traitementCautionCarteV2(string $dmid, string $token = NULL, string $etat = NULL) {
    $demande = $this->demandeServices->load($dmid);

    if (!empty($demande) && $demande->getWebformToken() === $token) {

      if ($etat == 'accept') {
        // $demande->setCaution(ReservationDemande::CAUTION_CARTE);
        // $demande->save();
      }
      elseif ($etat == 'decline') {
        $this->declineCautionCarteV2($demande);
      }

    }
    else {

      $demande = NULL;

    }

    $form_traitement_cb = \Drupal::formBuilder()->getForm(
      'Drupal\reservation\Form\DemandeCautionTraitementCarteBancaireForm', $demande, $etat);

    return [
      '#theme' => 'template_demande_caution_traitement_carte_bancaire',
      '#form_traitement_cb' => $form_traitement_cb,
    ];
  }

  /**
   * Désactivation du statut caution CB sur fermeture modale.
   *
   * @param string $dmid
   * @param string $token
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function declineOnCloseModalV2(string $dmid, string $token) {
    $demande = $this->demandeServices->load($dmid);
    if (!empty($demande) && $demande->getWebformToken() === $token) {
      $this->declineCautionCarteV2($demande);
      $result = "OK";
    }
    else {
      $result = "KO";
    }
    return new Response($result, 200, []);
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   *   Objet Demande à remettre à l'état "formulaire".
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function declineCautionCarteV2(ReservationDemande $demande) {
    // Retour au statut "formulaire" et réinitialisation de la caution.
    $demande->setStatut(ReservationDemande::STATUT_FORMULAIRE);
    $demande->setCaution(ReservationDemande::CAUTION_NON_APPLICABLE);
    // Remise en place du délai d'expiration par défaut.
    $date_created = new \DateTime();
    $date_created->setTimestamp($demande->getCreatedTime());
    $delay = ReservationUtils::isGestionnaireManuel()
      ? $this->expirationDelayModeManuel : $this->expirationDelay;
    $new_expiration_time =
      ReservationUtils::addExpirationDelayToDateTime($date_created, $delay);
    $demande->setWebformTokenExpiration($new_expiration_time);
    $demande->save();
  }

  /**
   * Affichage de formulaires de Caution Carte Bancaire
   * Page /reservation/demande/caution/modal/{demandeid}
   *
   * @param string $demandeid
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function cautionModalV2(string $demandeid = NULL) {
    $carteBancaireURL = URL::fromRoute('reservation.demande.caution.carte_bancaire_v2', ['demandeid' => $demandeid]);
    $title = t('Enregistrement de la caution par carte bancaire');
    $html = '<iframe src="' . $carteBancaireURL->toString() . '" style="width:100%;height:100%;" frameBorder="0"></iframe>';
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand(
      $title, $html,
      [
        'width' => '500',
        'height' => '700',
        'closeOnEscape' => 'true',
        'dialogClass' => 'reservation-ui-dialog',
      ]
    ));
    return $response;
  }

  /**
   * Traitement de la demande suite au retour depuis OGONE.
   *
   * @param string $etat
   * @param string $token
   * @param string $caution
   *
   * @return array
   */
  public function caution(string $etat = NULL, string $token = NULL, $caution = ReservationDemande::CAUTION_NON_RENSEIGNEE) {
    // Test si le token utilisé est toujours actif, sinon suppression du token
    // + demande (dans la fonction checkToken)
    if ($this->demandeTokenServices->checkToken($token) === TRUE) {
      if ($caution == ReservationDemande::CAUTION_CHEQUE) {
        return $this->traitementCautionCheque($token);
      }
      else {
        return $this->traitementCautionCarte($token, $etat);
      }
    }
    else {
      return $this->traitementToken($token);
    }
  }

  /**
   * Traitement de la demande suite au retour depuis OGONE.
   *
   * @param string $etat
   * @param string $token
   * @param string $caution
   *
   * @return array
   */
  public function traitementCautionCheque(string $token = NULL) {
    // Test si le token utilisé est toujours actif, sinon suppression du token
    // + demande (dans la fonction checkToken)
    $statut_mail = ReservationDemande::STATUT_ATTENTE;
    $demande = $this->demandeTokenServices->loadDemandeByToken($token);
    if ($demande->getDate()->getReservationRessourceNode()->getAutomatique()) {
      $statut = ReservationDemande::STATUT_CONFIRME;
      $statut_mail = ReservationDemande::STATUT_CONFIRME;
      $message = 'message_confirmation_acceptee';
    }
    else {
      $statut = ReservationDemande::STATUT_ATTENTE;
      $message = 'message_confirmation_pre_reservation';
    }

    $demande->setStatut($statut);
    $demande->setCaution(ReservationDemande::CAUTION_CHEQUE);
    $demande->save();

    $webform_submission = $demande->getWebformSubmission();
    $webform_submission->set('in_draft', FALSE);
    $webform_submission->save();

    // Envoi Mail confirmation enregistrement Réservation.
    /** @var \Drupal\reservation\Service\ReservationMailServices $mailServices */
    $mailServices = \Drupal::service('reservation.mail');
    $mailServices->generateEmailById($demande->id(), $statut_mail);

    $this->demandeTokenServices->destroyToken($token);

    return $this->affichageMessage($message);
  }

  /**
   * Traitement de la demande suite au retour depuis OGONE.
   *
   * @param string $etat
   * @param string $token
   * @param string $caution
   *
   * @return array
   */
  public function traitementCautionCarte(string $token = NULL, string $etat = NULL) {

    $demande = $this->demandeTokenServices->loadDemandeByToken($token);

    if (!empty($demande) && $etat == 'accept') {

      if ($demande->getDate()->getReservationRessourceNode()->getAutomatique()) {
        $message = 'message_confirmation_acceptee';
      }
      else {
        $message = 'message_confirmation_pre_reservation';
      }

      $webform_submission = $demande->getWebformSubmission();
      $webform_submission->set('in_draft', FALSE);
      $webform_submission->save();

      $this->demandeTokenServices->destroyToken($token);

      return $this->affichageMessage($message);
    }
    else {
      return $this->cautionCancel($token);
    }

  }

  /**
   * Traitement de la demande suite à une annulation depuis OGONE.
   *
   * @param string $etat
   * @param string $token
   * @param string $caution
   *
   * @return array
   */
  public function cautionCancel(string $token = NULL) {
    $this->demandeTokenServices->destroyDemandeByToken($token);
    return $this->affichageMessage('message_annulee');
  }

  /**
   *
   */
  public function demandeManuelle() {
    $reservationRessource = \Drupal::service('reservation.ressource');
    return [
      '#theme' => 'template_demande_manuelle',
      '#ressources' => $reservationRessource->getAvailableRessources(),
    ];
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\Response
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function retourOgone(Request $request) {

    if ($request->getMethod() === 'GET') {
      // For testing and debugging purpose only.
      $demandeid = $request->query->get('orderID');
      $statut_ogone = $request->query->get('STATUS');
      $amount_ogone = $request->query->get('amount');
    }
    else {
      $demandeid = $request->request->get('orderID');
      $statut_ogone = $request->request->get('STATUS');
      $amount_ogone = $request->request->get('amount');
    }

    /** @var \Drupal\reservation\Entity\ReservationDemande $demande */
    $demande = $this->demandeServices->load($demandeid);

    // On vérifie que la demande existe et qu'elle est en attente de caution
    // ou en mode formulaire si l'utilisateur a fermé la fenêtre modale.
    if (empty($demande)) {
      return new Response("Invalid order ID", 400, []);
    }
    elseif (!($demande->getStatut() === ReservationDemande::STATUT_CAUTION
      || $demande->getStatut() === ReservationDemande::STATUT_FORMULAIRE)) {
      return new Response("Invalid order status", 400, []);
    }
    else {
      /** @var \Drupal\reservation\Entity\ReservationRessourceNode $resource_node */
      $resource_node = $demande->getDate()->getReservationRessourceNode();
      $caution_montant = $resource_node->getCautionMontant();
      // On vérifie que le montant payé correspond au montant demandé.
      if ($caution_montant != $amount_ogone) {
        return new Response("Invalid amount", 400, []);
      }

      /** @var \Drupal\webform\Entity\WebformSubmission $webformSubmission */
      $webform_submission = $demande->getWebformSubmission();
      if (empty($webform_submission)) {
        // La soumission Webform n'a pas encore été enregistrée dans la demande.
        $is_v2 = TRUE;
      }
      else {
        // L'élément WebformReservationCaution (V2) a été ajouté au formulaire.
        $caution_elt_name = ReservationUtils::getCautionElementName($webform_submission->getWebform());
        $caution = $webform_submission->getElementData($caution_elt_name);
        $is_v2 = ($caution === ReservationDemande::CAUTION_CARTE);
      }

      if ($is_v2) {
        // V2 version detected!
        // Accepted.
        if ($statut_ogone === '5') {
          if ($demande->getDate()->getReservationRessourceNode()->getAutomatique()) {
            $statut = ReservationDemande::STATUT_CONFIRME;
          }
          else {
            $statut = ReservationDemande::STATUT_ATTENTE;
          }
          $demande->setStatut($statut);
          $demande->setCaution(ReservationDemande::CAUTION_CARTE);
          $demande->save();
        }
      }
      else {

        // Accepted.
        if ($statut_ogone === '5') {
          if ($demande->getDate()->getReservationRessourceNode()->getAutomatique()) {
            $statut = ReservationDemande::STATUT_CONFIRME;
            $statut_mail = ReservationDemande::STATUT_CONFIRME;
          }
          else {
            $statut = ReservationDemande::STATUT_ATTENTE;
            $statut_mail = ReservationDemande::STATUT_ATTENTE;
          }
        }
        else {
          $statut = ReservationDemande::STATUT_CAUTION;
        }

        $demande->setStatut($statut);
        $demande->setCaution(ReservationDemande::CAUTION_CARTE);
        $demande->save();

        // Envoi Mail confirmation enregistrement Réservation.
        if ($statut != ReservationDemande::STATUT_CAUTION) {
          /** @var \Drupal\reservation\Service\ReservationMailServices $mailServices */
          $mailServices = \Drupal::service('reservation.mail');
          $mailServices->generateEmailById($demande->id(), $statut_mail);
        }
      }

      return new Response("OK", 200, []);
    }

  }

}
