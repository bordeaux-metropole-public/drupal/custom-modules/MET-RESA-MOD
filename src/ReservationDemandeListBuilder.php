<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of ReservationDemande.
 *
 * @ingroup reservation
 */
class ReservationDemandeListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ReservationDemande ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\reservation\Entity\ReservationDemande $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label() . "modif",
      'entity.reservation_demande.edit_form',
      ['reservation_demande' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
