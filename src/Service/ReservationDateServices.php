<?php

namespace Drupal\reservation\Service;

use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDate;

/**
 * Class ReservationDateServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationDateServices {

  /**
   *
   */
  const RESERVATION_DATE_ENTITY_NAME = 'reservation_date';

  const MOIS_FR = [
    "01" => 'Janvier',
    "02" => 'Février',
    "03" => 'Mars',
    "04" => 'Avril',
    "05" => 'Mai',
    "06" => 'Juin',
    "07" => 'Juillet',
    "08" => 'Août',
    "09" => 'Septembre',
    "10" => 'Octobre',
    "11" => 'Novembre',
    "12" => 'Décembre',
  ];

  /**
   * ReservationDateServices constructor.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public function getYearMax() {
    $date = new \DateTime($this->getDateMax());
    return $date->format('Y');
  }

  /**
   *
   * @return date Champ Date
   */
  public function getDateMax() {
    $query = \Drupal::entityQuery(self::RESERVATION_DATE_ENTITY_NAME);
    $query->range(0, 1);
    $query->sort('date', 'DESC');
    $query->accessCheck(TRUE);
    return $this->loadDemande($query->execute());
  }

  /**
   * Blabla.
   *
   * @return date Champ Date
   */
  public function loadDemande($rdid) {
    $reservationdate = ReservationDate::load(current($rdid));
    if ($reservationdate) {
      return $reservationdate->getDate();
    }
  }

  /**
   *
   * @param mixed $id
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function load($id = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_DATE_ENTITY_NAME)
      ->load($id);
  }

  /**
   *
   * @param mixed $nid
   * @param mixed $date
   * @param mixed $publie
   * @param mixed $statut
   *
   * @return mixed
   */
  public function getDate($nid, $date, $publie = FALSE, $statut = FALSE, $postDate = FALSE) {
    $query = $this->queryDate($nid, $date, $statut, $publie, $postDate);
    return $this->getAll($query->execute());
  }

  /**
   * @param $ressourceId
   * @param $datePrefix
   * @param $statut
   * @param $publie
   * @param bool $startDate
   * @param bool $startDateValue
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   * @throws \Exception
   */
  public function queryDate(
    $ressourceId,
    $datePrefix,
    $statut = NULL,
    $publie = NULL,
    $startDate = FALSE,
    $startDateValue = NULL) {
    $query = \Drupal::entityQuery(self::RESERVATION_DATE_ENTITY_NAME);
    if ($statut) {
      $query->condition('statut', $statut);
    }
    if ($publie) {
      $query->condition('publie', $publie);
    }
    if ($startDate) {
      if (empty($startDateValue)) {
        $startDateValue = new \DateTime();
      }
      $query->condition('date', $startDateValue->format('Y-m-d 00:00:00'), '>=');
    }
    if ($datePrefix) {
      $query->condition('date', $datePrefix . '%', 'LIKE');
    }
    $query->condition('nid', $ressourceId);
    $query->accessCheck(FALSE);
    $query->sort('date', 'ASC');

    return $query;
  }

  /**
   * @param $ids
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function getAll($ids = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_DATE_ENTITY_NAME)
      ->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getYearMin() {
    $date = new \DateTime($this->getDateMin());
    return $date->format('Y');
  }

  /**
   * {@inheritdoc}
   */
  public function getDateMin() {
    $query = \Drupal::entityQuery(self::RESERVATION_DATE_ENTITY_NAME);
    $query->range(0, 1);
    $query->sort('date', 'ASC');
    $query->accessCheck(TRUE);
    return $this->loadDemande($query->execute());
  }

  /**
   * @param $nid
   *
   * @return mixed
   */
  public function getRdidByNid($nid) {
    $query = \Drupal::entityQuery(self::RESERVATION_DATE_ENTITY_NAME);
    $query->condition('nid', $nid, 'IN');
    $query->accessCheck(TRUE);
    return $query->execute();
  }

  /**
   * @param $nid
   *
   * @return mixed
   */
  public function getRdidActiveByNid($nid) {
    $query = \Drupal::entityQuery(self::RESERVATION_DATE_ENTITY_NAME);
    $query->condition('nid', $nid, 'IN');
    $query->condition('statut', 1, '=');
    $query->accessCheck(TRUE);
    return $query->execute();
  }

  /**
   * @param $nid
   * @param $year
   *
   * @return array
   * @throws \Exception
   */
  public function getFormDatesExist($nid, $year) {
    $dates = [];
    $reservationDates = $this->getFormDates($nid, $year, NULL);
    foreach ($reservationDates as $reservationDate) {
      $row = [];
      $date = new \DateTime($reservationDate->get('date')->value);

      $row['rdid'] = $reservationDate->get('rdid')->value;
      $row['statut'] = $reservationDate->get('statut')->value;
      $row['publie'] = $reservationDate->get('publie')->value;
      $dates[$date->format('m')][$date->format('d')] = $row;
    }

    return $dates;
  }

  /**
   * @param $nid
   * @param $date
   * @param $statut
   *
   * @return mixed
   * @throws \Exception
   */
  public function getFormDates($nid, $date, $statut = NULL, $publie = NULL) {
    $query = $this->queryDate($nid, $date, $statut, $publie);
    return $this->getAll($query->execute());
  }

  /**
   * {@inheritdoc}
   */
  public function getPublieMonth($year, $month, $nid, $publie = TRUE) {
    $query = $this->queryDate($nid, $year . '-' . $month, TRUE, $publie);
    $query->range(0, 1);
    return $query->count()->execute() ? TRUE : FALSE;
  }

  /**
   *
   * @param mixed $nid
   * @param mixed $date
   * @param mixed $publie
   * @param mixed $statut
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function getPublishedDates($nid, $date, $publie = TRUE, $statut = TRUE) {
    $query = $this->queryDate($nid, $date, $statut, $publie, TRUE);
    return $this->getAll($query->execute());
  }

  /**
   * @param $nid
   * @param $date
   * @param $date
   *
   * @return mixed
   * @throws \Exception
   */
  public function getFirstOpenedDateByRessourceId($nid, $date, $statut) {
    $reservationDate = $this->getOpenedDates($nid, $date, $statut);
    return array_shift($reservationDate);
  }

  /**
   * @param $nid
   * @param $date
   * @param $statut
   *
   * @return mixed
   * @throws \Exception
   */
  public function getOpenedDates($nid, $date, $statut) {
    $query = $this->queryDate($nid, $date, $statut, NULL, TRUE);
    return $this->getAll($query->execute());
  }

  /**
   * @param $nid
   * @param $date
   * @param $statut
   *
   * @return mixed
   * @throws \Exception
   */
  public function getFirstDateByRessourceId($nid, $date, $statut) {
    $reservationDate = $this->getAllDates($nid, $date, $statut);
    return array_shift($reservationDate);
  }

  /**
   * @param $nid
   * @param $date
   * @param $statut
   *
   * @return mixed
   * @throws \Exception
   */
  public function getAllDates($nid, $date, $statut) {
    $query = $this->queryDate($nid, $date, $statut, FALSE, FALSE);
    return $this->getAll($query->execute());
  }

  /**
   * @param $nid
   * @param $month
   * @param $year
   * @param $statut
   *
   * @return array
   * @throws \Exception
   */
  public function getOpenedMonths($nid, $month, $year, $statut) {
    $openedDates = $this->getOpenedDates($nid, $year, $statut);
    return $this->buildMonthsFromDates($nid, $month, $year, $openedDates);
  }

  /**
   * @param $nid
   * @param $month
   * @param $year
   * @param $dates
   *
   * @return array
   * @throws \Exception
   */
  public function buildMonthsFromDates($nid, $month, $year, $dates) {
    $months = [];
    foreach ($dates as $date) {
      $dateValue = new \DateTime($date->get('date')->value);
      $dateMonth = $dateValue->format('m');
      $months[$dateMonth]['key'] = $dateMonth;
      $months[$dateMonth]['title'] = self::MOIS_FR[$dateMonth];
      $months[$dateMonth]['link'] = Url::fromRoute('reservation.disponibilite.horaire',
        [
          'nid' => $nid,
          'month' => $dateMonth,
          'year' => $year,
        ], ['absolute' => TRUE]);
      if ($dateMonth == $month) {
        $months[$dateMonth]['active'] = TRUE;
      }
    }
    return $months;
  }

  /**
   * @param $nid
   * @param $month
   * @param $year
   * @param $statut
   *
   * @return array
   * @throws \Exception
   */
  public function getAllMonths($nid, $month, $year, $statut) {
    $allDates = $this->getAllDates($nid, $year, $statut);
    return $this->buildMonthsFromDates($nid, $month, $year, $allDates);
  }

  /**
   * @param $ressourceId
   *
   * @return mixed
   */
  public function getDateByRessourceId($ressourceId) {
    $query = \Drupal::entityQuery(self::RESERVATION_DATE_ENTITY_NAME);
    $query->condition('nid', $ressourceId);
    $query->sort('rdid', 'ASC');
    $query->accessCheck(TRUE);
    return $this->getAll($query->execute());
  }

  /**
   * @param $nid
   *
   * @throws \Exception
   */
  public function setCancelDate($nid) {
    $rows = $this->getOpenedDates($nid, '', NULL);
    foreach ($rows as $node) {
      $node->statut->value = FALSE;
      $node->save();
    }
  }

  /**
   * @param $ressourceId
   * @param $year
   *
   * @return mixed
   * @throws \Exception
   */
  public function countOpenedDates($ressourceId, $year) {
    $query = $this->queryDate($ressourceId, $year, TRUE, NULL, TRUE);
    return $query->count()->execute();
  }

  /**
   * @param $ressourceId
   * @param $year
   *
   * @return mixed
   * @throws \Exception
   */
  public function countPublishedDates($ressourceId, $year) {
    $query = $this->queryDate($ressourceId, $year, TRUE, TRUE, TRUE);
    return $query->count()->execute();
  }

}
