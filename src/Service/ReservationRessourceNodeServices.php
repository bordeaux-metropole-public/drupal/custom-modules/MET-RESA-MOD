<?php

namespace Drupal\reservation\Service;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\webform\Entity\Webform;

/**
 * Class ReservationRessourceNodeServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationRessourceNodeServices {

  /**
   *
   */
  const RESERVATION_RESSOURCE_NODE_ENTITY_NAME = 'reservation_ressource_node';

  /**
   * ReservationRessourceNodeServices constructor.
   */
  public function __construct() {
  }

  /**
   * @param $nid
   *
   * @return \Drupal\Core\Entity\Entity|\Drupal\Core\Entity\EntityInterface|null
   */
  public static function getNodeById($nid) {
    $node = ReservationRessourceNode::load($nid);

    return $node;
  }

  /**
   * @param $rrid
   *
   * @return mixed
   */
  public static function queryNodeByRrid($rrid) {
    $query = \Drupal::entityQuery(self::RESERVATION_RESSOURCE_NODE_ENTITY_NAME);
    $query->condition('rrid', $rrid);
    $query->accessCheck(TRUE);
    return $query->execute();
  }

  /**
   * @param $nid
   * @param $statut
   */
  public static function setStatusById($nid, $statut) {
    $rows = \Drupal::entityTypeManager()
      ->getStorage(
        self::RESERVATION_RESSOURCE_NODE_ENTITY_NAME)
      ->loadByProperties(['nid' => $nid]);
    foreach ($rows as $row) {
      $row->statut->value = $statut;
      $row->save();
    }
  }

  /**
   * @param $statut
   *
   * @return mixed
   */
  public function getRessourceNodes($statut = NULL) {
    return $this->getAll($this->getRessourceNodeNids($statut));
  }

  /**
   *
   * @param mixed $ids
   *
   * @return mixed
   */
  public function getAll($ids = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_RESSOURCE_NODE_ENTITY_NAME)
      ->loadMultiple($ids);
  }

  /**
   * @param $statut
   *
   * @return array|int
   */
  public function getRessourceNodeNids($statut = NULL) {
    $query = \Drupal::entityQuery(self::RESERVATION_RESSOURCE_NODE_ENTITY_NAME);
    if ($statut) {
      $query->condition('statut', 1);
    }
    $query->accessCheck(TRUE);
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getCautionStatut() {
    return ($this->get('caution_statut')->value) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setCautionStatut($caution_statut) {
    $this->set('caution_statut', $caution_statut);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCautionMontant() {
    return $this->get('caution_montant')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCautionMontant($caution_montant) {
    $this->set('caution_montant', $caution_montant);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutomatique() {
    return $this->get('automatique')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAutomatique($automatique) {
    $this->set('automatique', $automatique);
    return $this;
  }

  /**
   * @param $nid
   *
   * @return array
   */
  public function getWebformAvailableTokens($nid) {
    $tokens = [];

    $tokens[] = [
      'title' => 'n° Demande',
      'token' => '[reservation_demande:rdid:entity:rdid]',
    ];
    $tokens[] = [
      'title' => 'Date Créneau',
      'token' => '[reservation_demande:datecreneau]',
    ];
    $tokens[] = [
      'title' => 'Date de la demande',
      'token' => '[reservation_demande:datedemande]',
    ];
    $tokens[] = [
      'title' => 'Titre de la ressource',
      'token' => '[reservation_demande:ressource]',
    ];
    $tokens[] = [
      'title' => 'Email du demandeur',
      'token' => '[reservation_demande:email]',
    ];
    $tokens[] = [
      'title' => 'Token du webform associé (V2+)',
      'token' => '[reservation_demande:webform_token]',
    ];
    $tokens[] = [
      'title' => 'URL d\'annulation de réservation (V2+)',
      'token' => '[reservation_demande:cancel_url]',
    ];

    $webform = $this->getWebform($nid);
    if (!empty($webform)) {
      // $tokens[] = ['title' => 'Token du webform',
      // 'token' => '[webform_submission:token]'];
      $webformElements = $webform->getElementsInitializedAndFlattened();
      $tokens = $this->recurseArray($webformElements, 'webform_submission:values', $tokens);
    }

    return $tokens;
  }

  /**
   * @todo travail d'optimisation à faire.
   *
   * @param $nid
   * @param $webforms
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface
   */
  public function getWebform($nid = NULL, $webforms = NULL) {
    if (empty($webforms)) {
      $webforms = Webform::loadMultiple();
    }
    foreach ($webforms as $webform) {
      $elements = $webform->getElementsDecoded() ?: [];
      foreach ($elements as $element) {
        if (isset($element["#type"])) {
          if ($element["#type"] === "webform_reservation"
            && isset($element["#value"]) && $element["#value"] === $nid) {
            return $webform;
          }
          else {
            if ($element["#type"] === "webform_reservation_config" &&
              isset($element["#webform_reservation_ressource_id"]) &&
              $element["#webform_reservation_ressource_id"] === $nid) {
              return $webform;
            }
          }
        }
      }
    }
  }

  /**
   * @param $webformElements
   * @param string $prefix
   * @param array $tokens
   *
   * @return array
   */
  private function recurseArray($webformElements, $prefix, $tokens = []): array {
    foreach ($webformElements as $name => $value) {
      if (is_array($value)) {
        if (isset($value["#title"])) {
          if (isset($value["#type"]) && $value["#type"] !== 'webform_wizard_page') {
            $title = $value['#title'];
            if ($title instanceof TranslatableMarkup) {
              $title = $title->render();
            }
            $title = strip_tags($title);
            $tokens[] = [
              'title' => $title,
              'token' => '[' . $prefix . ':' . $name . ']',
            ];
          }
          $tokens = $this->recurseArray($webformElements[$name], $prefix . ':' . $name, $tokens);
        }
        else {
          $newPrefix = (substr($name, 0, 1) === '#') ? $prefix : $prefix . ':' . $name;
          $tokens = $this->recurseArray($webformElements[$name], $newPrefix, $tokens);
        }
      }
    }
    return $tokens;
  }

}
