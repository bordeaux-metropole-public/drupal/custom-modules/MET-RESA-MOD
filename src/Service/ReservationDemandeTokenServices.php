<?php

namespace Drupal\reservation\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\Entity\ReservationDemandeToken;
use Drupal\reservation\ReservationConstants;

/**
 * Class ReservationDemandeTokenServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationDemandeTokenServices {

  /**
   *
   */
  const ID_RESERVATION_DEMANDE_TOKEN = 'reservation_demande_token';

  /**
   *
   */
  // In seconds.
  const DEFAULT_LOCK_TIME = 60;

  /**
   *
   */
  // In seconds.
  const DEFAULT_TOKEN_DESTROY_DELAY = 30;

  /**
   *
   * @var demandeServices
   */
  protected $demandeServices;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   *
   * @var int
   */
  protected $timeLimit;

  /**
   * @var int
   */
  protected $destroyTokenDelay;

  /**
   * @var bool
   */
  protected $debugEnabled;

  /**
   * ReservationDemandeTokenServices constructor.
   *
   * @param \Drupal\reservation\Entity\ReservationDemandeServices $demandeServices
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   */
  public function __construct(ReservationDemandeServices $demandeServices, CacheBackendInterface $cache) {
    $this->demandeServices = $demandeServices;
    $this->cache = $cache;
    $reservationSettings = \Drupal::config(ReservationConstants::MODULE_SETTINGS);
    $tokenSettings = $reservationSettings->get('token');
    $this->timeLimit = $tokenSettings;
    $this->destroyTokenDelay = self::DEFAULT_TOKEN_DESTROY_DELAY;
    $jsonCalendarSettings = $reservationSettings->get('json_calendar');
    $this->debugEnabled = ($jsonCalendarSettings['debug-enabled'] == 1);
  }

  /**
   *
   * @param mixed $token
   *
   * @return mixed
   */
  public static function destroyToken($token = NULL) {
    $demandeToken = ReservationDemandeToken::load($token);

    return $demandeToken->delete();
  }

  /**
   *
   * @param mixed $token
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function loadToken($token = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::ID_RESERVATION_DEMANDE_TOKEN)
      ->load($token);
  }

  /**
   *
   * @param mixed $ids
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function getAll($ids = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::ID_RESERVATION_DEMANDE)
      ->loadMultiple($ids);
  }

  /**
   *
   * @param mixed $token
   *
   * @return mixed
   */
  public function loadDemandeByToken($token = NULL) {
    $demandeToken = ReservationDemandeToken::load($token);
    if ($demandeToken) {
      return $demandeToken->getDemande();
    }
    else {
      return NULL;
    }
  }

  /**
   *
   * @param mixed $token
   *
   * @return mixed
   */
  public function destroyDemandeByToken($token = NULL) {
    $demandeToken = ReservationDemandeToken::load($token);

    if ($demandeToken == NULL) {
      return FALSE;
    }
    else {
      if ($demandeToken->getDemande()) {
        $this->demandeServices->deleteDemande($demandeToken->getDemande()
          ->id());
      }
      return $demandeToken->delete();
    }
  }

  /**
   * Vérfication des tokens Actifs.
   */
  public function checkToken($token) {
    $this->destroyTokenObsolete();

    $date_now = new \DateTime();

    $demandeToken = ReservationDemandeToken::load($token);
    if ($demandeToken) {
      $date_limit = $demandeToken->getCreatedFormat();
      $date_limit->add(new \DateInterval('PT' . $this->timeLimit[$demandeToken->getTimeout()] . 'M'));
      if ($date_limit->getTimestamp() > $date_now->getTimestamp()) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Vérfication des tokens Actifs et suppression des demandes.
   */
  public function destroyTokenObsolete() {
    $cacheAndLockIdentifier = __METHOD__;

    // Launch only if not already done recently (timestamp still in cache) and
    // not create manual demand.
    if (!$this->cache->get($cacheAndLockIdentifier)) {

      $this->cache->set($cacheAndLockIdentifier, time(), time() + $this->destroyTokenDelay);

      // Use locking to avoid launching multiple simultaneous cleanups.
      $lock = \Drupal::lock();
      if ($lock->acquire($cacheAndLockIdentifier, self::DEFAULT_LOCK_TIME)) {

        if ($this->debugEnabled) {
          \Drupal::logger(ReservationConstants::LOG_CHANNEL)
            ->debug(t("Obsolete tokens cleanup"));
        }

        $this->destroyTokenTimeLimit(ReservationDemandeToken::TOKEN_TIME_LIMIT);
        $this->destroyTokenTimeLimit(ReservationDemandeToken::TOKEN_TIME_CAUTION);
        $this->destroyTokenTimeLimit(ReservationDemandeToken::TOKEN_TIME_MANUELLE);

        $this->demandeServices->destroyExpiredWebformDemandes();

        $lock->release($cacheAndLockIdentifier);
      }
    }
  }

  /**
   * Suppression de masse des token/demandes dépassant un temps donné
   */
  public function destroyTokenTimeLimit($timeout = ReservationDemandeToken::TOKEN_TIME_LIMIT) {
    // Suppression des Tokens dépassant le temps limite de Purge.
    foreach ($this->getTokenByTimeout($timeout) as $token) {
      $demande = $token->getDemande();
      if ($demande) {
        $this->demandeServices->deleteDemande($demande->id());
      }
      $token->delete();
    }
  }

  /**
   *
   * @param mixed $time
   *
   * @return mixed
   */
  public function getTokenByTimeout($timeout = NULL) {
    $date_limit = new \DateTime();
    $date_limit->sub(new \DateInterval('PT' . $this->timeLimit[$timeout] . 'M'));

    $query = \Drupal::entityQuery('reservation_demande_token');
    $query->condition('created', $date_limit->getTimestamp(), '<');
    $query->condition('timeout', $timeout, '=');
    $query->accessCheck(FALSE);

    return ReservationDemandeToken::loadMultiple($query->execute());
  }

  /**
   * @param $demandeToken
   *
   * @throws \Exception
   */
  public function messageTime($demandeToken) {
    $date_limit = $demandeToken->getCreatedFormat();
    $date_limit->add(new \DateInterval('PT' . $this->timeLimit[$demandeToken->getTimeout()] . 'M'));
    \Drupal::messenger()
      ->addMessage('Vous avez jusqu\'à : ' . $date_limit->format('H:i') . ' pour procéder à la réservation.', 'error');
  }

  /**
   *
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   * @param mixed $token
   *
   * @return mixed
   */
  public function createDemandeToken(ReservationDemande $demande = NULL, $token = NULL, $timeout = ReservationDemandeToken::TOKEN_TIME_LIMIT) {
    $reservationDemandeToken = ReservationDemandeToken::create([
      'token' => $token,
      'rdmid' => $demande->id(),
      'timeout' => $timeout,
    ]);
    $reservationDemandeToken->save();
    $this->setCookie($reservationDemandeToken);

    return $reservationDemandeToken;
  }

  /**
   * Création d'un Cookie avec un temps d'activation de 10 min par défaut.
   *
   * @param mixed $formBuildId
   * @param mixed $seconds
   */
  public function setCookie($reservationDemandeToken) {
    $seconds = $this->timeLimit[$reservationDemandeToken->getTimeout()] * 60;
    if (isset($_COOKIE['formulaire_reservation'])) {
      unset($_COOKIE['formulaire_reservation']);
    }
    setcookie('formulaire_reservation', $reservationDemandeToken->getToken(), time() + $seconds, '/', "", FALSE, TRUE);
  }

  /**
   *
   * @return mixed
   */

  /**
   * Récupération du Cookie.
   */
  public function getCookie() {
    return \Drupal::request()->cookies->get('formulaire_reservation');
  }

}
