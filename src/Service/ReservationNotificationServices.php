<?php

namespace Drupal\reservation\Service;

/**
 * Class ReservationNotificationServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationNotificationServices {

  /**
   *
   */
  const RESERVATION_NOTIFICATION_ENTITY_NAME = 'reservation_notification';

  /**
   * ReservationNotificationServices constructor.
   */
  public function __construct() {
  }

  /**
   *
   * @param mixed $ids
   *
   * @return mixed
   */
  public function getAll($ids = NULL, $storage = self::RESERVATION_NOTIFICATION_ENTITY_NAME) {
    return \Drupal::entityTypeManager()
      ->getStorage($storage)
      ->loadMultiple($ids);
  }

  /**
   * @param $nid
   * @param string $storage
   *
   * @return mixed
   */
  public function getNotificationById($nid, $storage = self::RESERVATION_NOTIFICATION_ENTITY_NAME) {
    return \Drupal::entityTypeManager()
      ->getStorage($storage)
      ->load($nid);
  }

  /**
   * @param $nid
   * @param string $storage
   *
   * @return mixed
   */
  public function getNotificationsByNid($nid) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_NOTIFICATION_ENTITY_NAME)
      ->loadByProperties(['nid' => $nid]);
  }

  /**
   * @param string $type_email
   *
   * @return mixed
   */
  public function getNotificationByType($type_email = 'rappel') {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_NOTIFICATION_ENTITY_NAME)
      ->loadByProperties(['type_email' => $type_email]);
  }

  /**
   * @param $nid
   * @param $type_email
   *
   * @return array
   */
  public function generateStatutsTabs($nid = NULL, $type_email = NULL) {
    $tabs = [];
    $reservationSettings = \Drupal::config('reservation.settings');
    $rows = $reservationSettings->get('notification');
    foreach ($rows as $key => $row) {
      $tabs[$key]['key'] = $key;
      $tabs[$key]['title'] = $row['details']['title'];
      $tabs[$key]['title_statut'] = $row['details']['statut'];
      $reservation = $this->getNotificationByNidType($nid, $key);
      if (!empty($reservation)) {
        $tabs[$key]['statut'] = $reservation->statut->value;
      }
      else {
        $tabs[$key]['statut'] = 0;
      }
      $tabs[$key]['active'] = ($key === $type_email) ? TRUE : FALSE;
    }
    return $tabs;
  }

  /**
   * @param $nid
   * @param string $type_email
   *
   * @return mixed
   */
  public function getNotificationByNidType($nid, $type_email = 'demande') {
    $nodes = \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_NOTIFICATION_ENTITY_NAME)
      ->loadByProperties(['nid' => $nid, 'type_email' => $type_email]);

    return array_shift($nodes);
  }

}
