<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\ReservationUtils;

/**
 * Class ReservationHoraireServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationHoraireServices {

  /**
   *
   */
  const RESERVATION_HORAIRE_ENTITY_NAME = 'reservation_horaire';

  /**
   * ReservationHoraireServices constructor.
   */
  public function __construct() {

  }

  /**
   * @param $rdid
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getByDate($rdid) {
    $query = \Drupal::entityQuery(self::RESERVATION_HORAIRE_ENTITY_NAME);
    $query->condition('rdid', $rdid);
    $query->sort('heure_debut', 'ASC');
    $query->accessCheck(TRUE);
    return self::getAll($query->execute());
  }

  /**
   * @param $ids
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getAll($ids = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_HORAIRE_ENTITY_NAME)
      ->loadMultiple($ids);
  }

  /**
   * @param $rdid
   * @param bool $statut
   *
   * @return mixed
   */
  public static function getByRdid($rdid, $statut = TRUE) {

    $query = \Drupal::entityQuery(self::RESERVATION_HORAIRE_ENTITY_NAME);
    $query->condition('rdid', $rdid);
    if ($statut != NULL) {
      $query->condition('statut', $statut);
    }
    $query->sort('heure_debut', 'ASC');
    $query->accessCheck(FALSE);

    return self::getAll($query->execute());
  }

  /**
   * @param $id
   *
   * @return mixed
   */
  public function load($id = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_HORAIRE_ENTITY_NAME)
      ->load($id);
  }

  /**
   * @param $rdid
   * @param $heure_debut
   * @param $heure_fin
   *
   * @return bool
   */
  public function notExisteHoraire($rdid, $heureDebut, $heureFin) {
    $query = \Drupal::entityQuery(self::RESERVATION_HORAIRE_ENTITY_NAME);
    $heure_debut = ReservationUtils::getDateEntityFieldStringValue($heureDebut);
    $query->condition('heure_debut', $heure_debut);
    $heure_fin = ReservationUtils::getDateEntityFieldStringValue($heureFin);
    $query->condition('heure_fin', $heure_fin);
    $query->condition('rdid', $rdid);
    $query->range(0, 1);
    $query->accessCheck(TRUE);
    return $query->count()->execute() ? FALSE : TRUE;
  }

}
