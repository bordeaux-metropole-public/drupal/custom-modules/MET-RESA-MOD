<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\ReservationUtils;

/**
 * Class ReservationRessourceServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationRessourceServices {

  /**
   *
   */
  const RESERVATION_RESSOURCE_ENTITY_NAME = 'reservation_ressource';

  /**
   *
   */
  public function __construct() {
  }

  /**
   * @param $type
   * @param $statut
   *
   * @return bool
   */
  public function getTypeExist($type, $statut = NULL) {
    $query = \Drupal::entityQuery(self::RESERVATION_RESSOURCE_ENTITY_NAME);

    $query->condition('type', $type);
    if ($statut) {
      $query->condition('statut', $statut);
    }
    $query->accessCheck(TRUE);
    $rows = $query->execute();

    return $rows ? TRUE : FALSE;
  }

  /**
   * @param $type
   *
   * @return mixed|null
   */
  public function getRessourceByType($type) {
    $ressource = \Drupal::entityTypeManager()
      ->getStorage(
        self::RESERVATION_RESSOURCE_ENTITY_NAME)
      ->loadByProperties(['type' => $type]);

    return isset($ressource) ? array_shift($ressource) : NULL;
  }

  /**
   * @return array
   */
  public function getNodeTypes() {
    $entites = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    $contentTypesList = [];
    foreach ($entites as $entite) {
      $contentTypesList[$entite->id()] = $entite->label();
    }

    return $contentTypesList;
  }

  /**
   * Get user accessible resources.
   *
   * @return array
   */
  public function getAvailableRessources() {
    $rows = [];

    $types = $this->getTypeStatut(1);
    if (ReservationUtils::canManageAllRessourcesAvailabilities()) {
      /** @var \Drupal\reservation\Service\ReservationRessourceNodeServices $reservationRessourceNode */
      $reservationRessourceNode = \Drupal::service('reservation.ressource.node');
      $ids = $reservationRessourceNode->getRessourceNodeNids(1);
      $nodes = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadMultiple($ids);
    }
    else {
      if (ReservationUtils::canManageOwnResourcesReservations()) {
        $user = \Drupal::currentUser();
        if (!empty($user)) {
          /** @var \Drupal\reservation\Service\ReservationRessourceUserServices $ressourceUserServices */
          $ressourceUserServices = \Drupal::service('reservation.ressource.user');
          $ids = $ressourceUserServices->getRessourceUserNodeIdsByUid($user->id());
          $nodes = \Drupal::entityTypeManager()
            ->getStorage('node')
            ->loadMultiple($ids);
        }
      }
    }

    if (!empty($nodes)) {
      foreach ($nodes as $node) {
        if (in_array($node->getType(), $types)) {
          $rows[$node->Id()] = $node->getTitle();
        }
      }
    }

    return $rows;
  }

  /**
   * @param $statut
   *
   * @return array
   */
  public function getTypeStatut($statut = NULL) {
    $types = [];

    $node_types = $this->getRessources($statut);
    foreach ($node_types as $node_type) {
      $types[] = $node_type->get('type')->value;
    }

    return $types;
  }

  /**
   * @param $statut
   *
   * @return mixed
   */
  public function getRessources($statut = NULL) {
    $query = \Drupal::entityQuery(self::RESERVATION_RESSOURCE_ENTITY_NAME);
    if ($statut) {
      $query->condition('statut', 1);
    }
    $query->accessCheck(TRUE);
    return $this->getAll($query->execute());
  }

  /**
   *
   * @param mixed $ids
   *
   * @return mixed
   */
  public function getAll($ids = NULL) {
    return \Drupal::entityTypeManager()
      ->getStorage(self::RESERVATION_RESSOURCE_ENTITY_NAME)
      ->loadMultiple($ids);
  }

  /**
   * Liste les ressources pour lesquelles des demandes ont été effectuées.
   *
   * @return array
   */
  public function getAllRessourcesDemandees() {
    $query = \Drupal::database()->select('reservation_demande', 'dmd');
    $query->innerJoin('reservation_date', 'dt', 'dt.rdid = dmd.rdid');
    $query->fields('dt', ['nid']);
    $query->groupBy('dt.nid');
    $query->addExpression('COUNT(dmd.rdmid)', 'total');
    $query->having('COUNT(dmd.rdmid) > 0');

    $results = $query->execute()->fetchAll();

    $ressourceIds = [];
    foreach ($results as $result) {
      $ressourceIds[] = $result->nid;
    }

    $rows = [];

    $nodes = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadMultiple($ressourceIds);
    foreach ($nodes as $node) {
      $rows[$node->Id()] = $node->getTitle();
    }

    return $rows;
  }

}
