<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\Entity\ReservationMail;
use Drupal\reservation\ReservationConstants;

/**
 * Class ReservationMailServices.
 *
 * @package Drupal\reservation\Service
 */
class ReservationMailServices {

  /**
   *
   */
  const RESERVATION_MAIL_ENTITY_NAME = 'reservation_mail';

  /**
   *
   * @var demandeServices
   */
  protected $demandeServices;

  /**
   *
   * @var dateServices
   */
  protected $dateServices;

  /**
   *
   * @var notificationServices
   */
  protected $notificationServices;

  /**
   *
   * @param \Drupal\reservation\Service\ReservationDemandeServices $demandeServices
   * @param \Drupal\reservation\Service\ReservationDateServices $dateServices
   * @param \Drupal\reservation\Service\ReservationNotificationServices $notificationServices
   */
  public function __construct(
    ReservationDemandeServices $demandeServices,
    ReservationDateServices $dateServices,
    ReservationNotificationServices $notificationServices) {
    $this->demandeServices = $demandeServices;
    $this->dateServices = $dateServices;
    $this->notificationServices = $notificationServices;
  }

  /**
   * Génération d'un email en fonction en chargeant l'id de la demande.
   *
   * @param string $rdmid
   * @param string $type_email
   * @param bool $checked
   */
  public function generateEmailById(string $rdmid, string $type_email, bool $checked = NULL) {
    $demande = $this->demandeServices->load($rdmid);
    return $this->generateEmail($demande, $type_email, $checked);
  }

  /**
   * Génération d'un email.
   *
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   * @param string $type_email
   * @param bool $checked
   */
  public function generateEmail(ReservationDemande $demande, string $type_email, bool $checked = NULL) {
    if ($checked || $this->checkEmail($demande, $type_email)) {
      return $this->createEmail($demande, $type_email);
    }
  }

  /**
   * Vérification si un email a déjà été envoyé
   *
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   * @param mixed $type_email
   *
   * @return bool
   */
  public function checkEmail(ReservationDemande $demande, string $type_email) {
    if ($demande->getDate()) {
      $notification = $this->notificationServices->getNotificationByNidType(
        $demande->getDate()->getReservationRessourceNode()->id(), $type_email);
      if ($notification) {
        if ($this->checkAlreadySent($notification->id(), $demande->id())) {
          return FALSE;
        }
        if (!$notification->getStatut()) {
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   *
   * @param mixed $rnid
   * @param mixed $rdmid
   *
   * @return mixed
   */
  public function checkAlreadySent($rnid, $rdmid) {
    $query = \Drupal::entityQuery(self::RESERVATION_MAIL_ENTITY_NAME);
    $query->condition('rnid', $rnid);
    $query->condition('rdmid', $rdmid);
    $query->accessCheck(TRUE);
    return $query->count()->execute();
  }

  /**
   *
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   * @param mixed $type_email
   *
   * @return bool
   */
  public function createEmail(ReservationDemande $demande, $type_email) {
    /** @var \Drupal\reservation\Entity\ReservationNotification $notification */
    $notification = $this->notificationServices->getNotificationByNidType(
      $demande->getDate()->getReservationRessourceNode()->id(), $type_email);

    if ($notification) {

      $langcode = \Drupal::config('system.site')->get('langcode');
      $module = 'reservation';
      $key = 'reservation_demande_mail';

      // Vérification si la demande est liée à un webform ou est manuelle.
      if ($demande->getWebformSubmission()) {
        $to = $this->replaceElement($demande, $notification->getReservationRessourceNode()
          ->getEmailTo());
      }
      else {
        $to = $demande->getEmail();
      }

      $reply = $notification->getEmailFrom();
      $send = TRUE;

      $params['message'] = $this->replaceElement($demande, $notification->getEmailCorps());
      $params['subject'] = $this->replaceElement($demande, $notification->getEmailObjet());
      $params['options']['title'] = $notification->getEmailObjet();
      $params['options']['footer'] = $notification->getEmailObjet();
      $params['from'] = $notification->getEmailFrom();
      $params['bcc'] = $notification->getEmailCc();

      /** @var \Drupal\Core\Mail\MailManager $mailManager */
      $mailManager = \Drupal::service('plugin.manager.mail');
      $result = $mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);

      if ($result && isset($result['result'])) {
        $mail = ReservationMail::create([
          'rdmid' => $demande->id(),
          'rnid' => $notification->id(),
        ]);
        $mail->save();

        \Drupal::logger(ReservationConstants::LOG_CHANNEL)->info(
          'Email : %type_email - destination : %to  - demande n°%rdmid du %created',
          [
            '%type_email' => $type_email,
            '%to' => $to,
            '%rdmid' => $demande->id(),
            '%created' => $demande->getCreatedFormat(),
          ]);
        return TRUE;

      }
      else {

        \Drupal::logger(ReservationConstants::LOG_CHANNEL)->warning(
          'Email - Erreur lors de l\'envoi : %type_email - destination : %to  - demande n°%rdmid du %created',
          [
            '%type_email' => $type_email,
            '%to' => $to,
            '%rdmid' => $demande->id(),
            '%created' => $demande->getCreatedFormat(),
          ]);

        return FALSE;
      }

    }
    else {

      \Drupal::logger(ReservationConstants::LOG_CHANNEL)->warning(
        'Email - Erreur lors de l\'envoi : %type_email - demande n°%rdmid - type de notification non défini.',
        ['%type_email' => $type_email, '%rdmid' => $demande->id()]);

    }
  }

  /**
   *
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   * @param mixed $element
   *
   * @return mixed
   */
  public function replaceElement(ReservationDemande $demande, $element) {
    $token_service = \Drupal::token();
    return $token_service->replace($element,
      [
        'reservation_demande' => $demande,
        'webform_submission' => $demande->getWebformSubmission(),
      ]
    );
  }

  /**
   * Génération d'un email en fonction en chargeant les demandes.
   *
   * @param mixed $type_email
   * @param bool $checked
   */
  public function generateMultipleEmailByDemande($demandes, string $type_email, bool $checked = NULL) {
    $count = 0;
    foreach ($demandes as $demande) {
      if ($this->generateEmail($demande, $type_email, $checked)) {
        $count++;
      }
    }

    \Drupal::logger(ReservationConstants::LOG_CHANNEL)
      ->info('Nombre d\'email %type_email envoyés : %count',
        ['%type_email' => $type_email, '%count' => $count]);
  }

  /**
   *
   * @param array $ids
   * @param mixed $type_email
   * @param mixed $checked
   */
  public function generateMultipleEmailById(array $ids, string $type_email, bool $checked = NULL) {
    $count = 0;
    $demandes = $this->demandeServices->getAll($ids);
    foreach ($demandes as $demande) {
      if ($this->generateEmail($demande, $type_email, $checked)) {
        $count++;
      }
    }

    \Drupal::logger(ReservationConstants::LOG_CHANNEL)
      ->info('Nombre d\'email %type_email envoyés : %count',
        ['%type_email' => $type_email, '%count' => $count]);
  }

  /**
   * @param $rdmid
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteDemandeMailHistory($rdmid) {
    $query = \Drupal::entityQuery(self::RESERVATION_MAIL_ENTITY_NAME);
    $query->condition('rdmid', $rdmid);
    $query->accessCheck(TRUE);
    $ids = $query->execute();
    if (count($ids) > 0) {
      $storage_handler = \Drupal::entityTypeManager()
        ->getStorage(self::RESERVATION_MAIL_ENTITY_NAME);
      $entities = $storage_handler->loadMultiple($ids);
      $storage_handler->delete($entities);
    }
  }

}
