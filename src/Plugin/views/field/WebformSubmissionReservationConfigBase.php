<?php

namespace Drupal\reservation\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 *
 */
abstract class WebformSubmissionReservationConfigBase extends FieldPluginBase {

  const DEFAULT_RESA_CONF_ELT_NAME = 'webform_reservation_config';

  const RESA_CONFIG_ELEMENT_NAME = 'reservation_config_element_name';

  /**
   * {@inheritdoc}
   */
  public function query() {
    // D'après le code présent dans la classe StatisticsLastCommentName.
    $resa_config_element_name =
      $this->options[self::RESA_CONFIG_ELEMENT_NAME] ?: self::DEFAULT_RESA_CONF_ELT_NAME;
    $definition = [
      'table' => 'webform_submission_data',
      'field' => 'sid',
      'left_table' => 'webform_submission',
      'left_field' => 'sid',
      'extra' => [
        ['field' => 'name', 'value' => $resa_config_element_name],
        ['field' => 'property', 'value' => $this->getPropertyName()],
        ['field' => 'delta', 'value' => 0],
      ],
    ];
    $join = \Drupal::service('plugin.manager.views.join')->createInstance('standard', $definition);
    $wsd_table = $this->query->ensureTable($this->getPluginId(), $this->relationship, $join);
    $this->field_alias = $this->query->addField($wsd_table, 'value');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options[self::RESA_CONFIG_ELEMENT_NAME] = ['default' => self::DEFAULT_RESA_CONF_ELT_NAME];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form[self::RESA_CONFIG_ELEMENT_NAME] = [
      '#title' => $this->t('Reservation config element name'),
      '#type' => 'textfield',
      '#default_value' => $this->options[self::RESA_CONFIG_ELEMENT_NAME] ?: self::DEFAULT_RESA_CONF_ELT_NAME,
    ];
  }

  /**
   * @return string
   *   The reservation config property name.
   */
  abstract protected function getPropertyName(): string;

}
