<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'webform_reservation_demandeur' element.
 *
 * @WebformElement(
 *   id = "webform_reservation_demandeur",
 *   default_key = "webform_reservation_demandeur",
 *   label = @Translation("Réservation Demandeur (v2+ only)"),
 *   description = @Translation("Provides a webform reservation demandeur
 *   (handler v2+ only)."), category = @Translation("Reservation"), multiline =
 *   TRUE, composite = TRUE, states_wrapper = TRUE,
 * )
 */
class WebformReservationDemandeurPlugin extends WebformReservationPersonnePlugin {

  /**
   * {@inheritdoc}
   */
  public function defineDefaultProperties() {
    return [
      'title' => 'Webform Réservation Demandeur',
    ] + parent::defineDefaultProperties();
  }

  /**
   *
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // $form['element']['title']['#disabled'] = True;
    $form['element']['multiple'] = [];
    $form['multiple'] = [];

    return $form;
  }

}
