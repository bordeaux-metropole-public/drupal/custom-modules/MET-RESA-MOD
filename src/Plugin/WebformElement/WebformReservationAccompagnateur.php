<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 *
 * @WebformElement(
 *   id = "webform_reservation_accompagnateur",
 *   label = @Translation("Réservation Accompagnateur"),
 *   description = @Translation("Element proposant un nombre d'accompagnateurs
 *   à une réservation."), category = @Translation("Reservation"), composite =
 *   FALSE, states_wrapper = FALSE,
 * )
 */
class WebformReservationAccompagnateur extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {
    $element['#admin_title'] = "Accompagnateur Réservation";
    parent::initialize($element);
  }

  /**
   * {@inheritdoc}
   */
  public function defineDefaultProperties() {
    return [
      'title' => 'Webform Réservation Accompagnateur',
      'jauge' => 0,
    ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function getTableColumn(array $element) {
    $columns = parent::getTableColumn($element);
    unset($columns['element__webform_reservation_accompagnateur__reservation-accompagnateur-select']);
    $columns['element__webform_reservation_accompagnateur']['title'] = "Accompagnateur";
    return $columns;
  }

  /**
   *
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['title']['#disabled'] = TRUE;

    $form['element']['jauge'] = [
      '#type' => 'number',
      '#title' => 'Jauge',
      '#default_value' => 0,
      '#validated' => TRUE,
    ];

    $form['composite']['element'] = [];
    $form['composite']['flexbox'] = [];
    $form['access'] = [];
    $form['element_attributes'] = [];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);

    $lines = [];
    $lines[] = $value;

    return $lines;
  }

}
