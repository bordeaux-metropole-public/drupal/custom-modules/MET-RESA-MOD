<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Service\ReservationRessourceNodeServices;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'webform_caution_element' element.
 *
 * @WebformElement(
 *   id = "webform_reservation_caution",
 *   label = @Translation("Réservation Caution (v2+ only)"),
 *   description = @Translation("Element permettant de choisir le type de
 *   caution souhaitée (handler v2+ only)."), category =
 * @Translation("Reservation"),
 * )
 *
 * @see \Drupal\webform_datepicker_element\Element\WebformExampleElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformReservationCaution extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {
    $element['#admin_title'] = "Caution Réservation";
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    $properties = parent::getDefaultProperties();

    $properties['title'] = 'Webform Réservation Caution';

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    $webform_submission_data = $webform_submission->getData();
    if (isset($webform_submission_data['webform_reservation_config'])
      && isset($webform_submission_data['webform_reservation_config']['webform_reservation_ressource_id'])) {
      $resource_node_id = $webform_submission_data['webform_reservation_config']['webform_reservation_ressource_id'];

      /** @var \Drupal\reservation\Entity\ReservationRessourceNode $resource_node */
      $resource_node = ReservationRessourceNodeServices::getNodeById($resource_node_id);
      if (!empty($resource_node) && $resource_node->isChequeAccepte()) {
        $element['#cheque_accepte'] = TRUE;
      }
    }

    // Here you can customize the webform element's properties.
    // You can also customize the form/render element's properties via the
    // FormElement.
    //
    // @see \Drupal\webform_datepicker_element\Element\WebformExampleElement::processWebformElementExample
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['title']['#disabled'] = TRUE;

    $form['element']['multiple'] = [];
    $form['multiple'] = [];

    $form['composite']['element'] = [];
    $form['composite']['flexbox'] = [];
    $form['access'] = [];
    $form['element_attributes'] = [];

    return $form;
  }

  /**
   *
   */
  public function getValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    if (!isset($element['#webform_key']) && isset($element['#value'])) {
      return $element['#value'];
    }

    $webform_key = (isset($options['webform_key'])) ? $options['webform_key'] : $element['#webform_key'];
    $value = $webform_submission->getElementData($webform_key);
    // Is value is NULL and there is a #default_value, then use it.
    if ($value === NULL && isset($element['#default_value'])) {
      $value = $element['#default_value'];
    }

    return $value;
  }

}
