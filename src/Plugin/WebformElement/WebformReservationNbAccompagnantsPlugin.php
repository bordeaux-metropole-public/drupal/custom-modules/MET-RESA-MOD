<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\ReservationConstants;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 *
 * @WebformElement(
 *   id = "webform_reservation_nb_accompagnants",
 *   default_key = "webform_reservation_nb_accompagnants",
 *   label = @Translation("Réservation Nombre d'accompagnants (v2+ only)"),
 *   description = @Translation("Element proposant un nombre d'accompagnants à
 *   une réservation (handler v2+ only)."), category =
 * @Translation("Reservation"), composite = TRUE,
 * )
 */
class WebformReservationNbAccompagnantsPlugin extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {
    $element['#admin_title'] = "Réservation Nombre d'accompagnants";
    parent::initialize($element);
  }

  /**
   * {@inheritdoc}
   */
  public function defineDefaultProperties() {
    return [
      'title' => "Réservation Nombre d'accompagnants",
      'jauge' => 0,
    ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function getTableColumn(array $element) {
    $columns = parent::getTableColumn($element);
    unset($columns['element__webform_reservation_nb_accompagnants__reservation-nb_accompagnants-select']);
    $columns['element__webform_reservation_nb_accompagnants']['title'] = "Nb accompagnants";
    return $columns;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['title']['#disabled'] = TRUE;

    $form['element']['jauge'] = [
      '#type' => 'number',
      '#title' => 'Jauge',
      '#default_value' => ReservationConstants::DEFAULT_NB_ACCOMPAGNANTS,
      '#validated' => TRUE,
    ];

    $form['element']['multiple'] = [];
    $form['multiple'] = [];

    $form['composite']['element'] = [];
    $form['composite']['flexbox'] = [];
    $form['access'] = [];
    $form['element_attributes'] = [];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    $lines = [];
    if (isset($value['reservation-accompagnateur-select'])) {
      $lines[] = $value['reservation-accompagnateur-select'];
    }
    else {
      $lines[] = '0';
    }
    return $lines;
  }

}
