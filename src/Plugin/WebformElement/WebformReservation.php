<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'webform_reservation' element.
 *
 * @WebformElement(
 *   id = "webform_reservation",
 *   label = @Translation("Réservation (v1 only)"),
 *   description = @Translation("Element permettant le lien entre une ressource
 *   et un node (handler v1 only)."), category = @Translation("Reservation"),
 * )
 *
 * @see \Drupal\reservation\Element\WebformReservationTest
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformReservation extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function defineDefaultProperties() {
    return [
      'title' => 'Webform Réservation',
      'key' => 'webform_reservation_ressource_id',
      'machine-name' => 'webform_reservation_ressource_id',
      'value' => '',
    ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['title']['#disabled'] = TRUE;
    $form['element']['key']['#disabled'] = TRUE;

    /** @var \Drupal\reservation\Service\ReservationRessourceServices $ressourceServices */
    $ressourceServices = \Drupal::service('reservation.ressource');
    $availableResources = $ressourceServices->getAvailableRessources();
    $form['element']['value'] = [
      '#type' => 'select',
      '#title' => 'Ressource : ',
      '#options' => $availableResources,
    ];

    $form['options'] = [];

    $form['options_other'] = [];

    return $form;
  }

}
