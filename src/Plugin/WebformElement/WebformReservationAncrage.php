<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 *
 * @WebformElement(
 *   id = "webform_reservation_ancrage",
 *   label = @Translation("Réservation Ancrage"),
 *   description = @Translation("Element proposant un nombre d'ancrages à une
 *   réservation."), category = @Translation("Reservation"), composite = FALSE,
 *   states_wrapper = FALSE,
 * )
 */
class WebformReservationAncrage extends WebformCompositeBase {

  /**
   *
   */
  public function initialize(array &$element) {

    parent::initialize($element);

    $element['#admin_title'] = "Ancrage Réservation";
    $this->initializeCompositeElements($element);
  }

  /**
   *
   */
  public function getDefaultProperties() {
    // $properties = parent::getDefaultProperties();
    $properties['title'] = 'Webform Réservation Ancrage';

    return $properties;
  }

  /**
   *
   */
  public function getTableColumn(array $element) {
    $columns = parent::getTableColumn($element);
    unset($columns['element__webform_reservation_ancrage__reservation-ancrage-select']);
    $columns['element__webform_reservation_ancrage']['title'] = "Ancrage";
    return $columns;
  }

  /**
   *
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['title']['#disabled'] = TRUE;
    $form['element']['key']['#disabled'] = TRUE;

    $form['options'] = [];

    $form['options_other'] = [];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

}
