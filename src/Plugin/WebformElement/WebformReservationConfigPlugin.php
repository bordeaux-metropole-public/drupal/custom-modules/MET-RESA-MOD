<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;

/**
 * Provides a 'webform_reservation_config' element.
 *
 * @WebformElement(
 *   id = "webform_reservation_config",
 *   default_key = "webform_reservation_config",
 *   label = @Translation("Réservation Configuration (v2+ only)"),
 *   description = @Translation("Provides a webform reservation configuration
 *   (handler v2+ only)."), category = @Translation("Reservation"), composite =
 *   TRUE,
 * )
 */
class WebformReservationConfigPlugin extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {
    $element['#admin_title'] = "Réservation Configuration";
    parent::initialize($element);
  }

  /**
   * {@inheritdoc}
   */
  public function defineDefaultProperties() {
    return [
      'title' => 'Réservation Configuration',
      'webform_reservation_ressource_id' => '',
    ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['title']['#disabled'] = TRUE;

    /** @var \Drupal\reservation\Service\ReservationRessourceServices $ressourceServices */
    $ressourceServices = \Drupal::service('reservation.ressource');
    $ressourceEnable = $ressourceServices->getAvailableRessources();

    $form['element']['webform_reservation_ressource_id'] = [
      '#type' => 'select',
      '#title' => 'Ressource : ',
      '#options' => $ressourceEnable,
    ];

    $form['options'] = [];
    $form['options_other'] = [];

    $form['element']['multiple'] = [];
    $form['multiple'] = [];

    return $form;
  }

}
