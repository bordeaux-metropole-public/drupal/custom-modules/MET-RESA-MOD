<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'webform_example_composite' element.
 *
 * @WebformElement(
 *   id = "webform_reservation_personne",
 *   label = @Translation("Réservation Personne"),
 *   description = @Translation("Provides a webform reservation personne."),
 *   category = @Translation("Reservation"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 *
 * @see \Drupal\webform_example_composite\Element\WebformExampleComposite
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformReservationPersonnePlugin extends WebformCompositeBase {

  const FRENCH_PHONE_NUMBER_PATTERN = '^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$';

  const PHONE_NUMBER_PATTERN_ERROR = 'Le numéro de téléphone doit être de la forme 0X XX XX XX XX.';

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $element_properties = $form_state->get('element_properties');

    $form['validation']['elements_container'] = [
      '#type' => 'container',
      '#access' => TRUE,
    ];
    $form['validation']['elements_container']['phone__pattern'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Motif de validation du numéro de téléphone'),
      '#description' => $this->t('Si le motif (regex) est renseigné, on vérifiera que le numéro de téléphone correspond bien au motif.'),
      '#access' => TRUE,
    ];
    $form['validation']['elements_container']['phone__pattern_error'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Message d'erreur du motif du numéro de téléphone"),
      '#description' => $this->t("Si renseigné, ce message sera utilisé à la place du message par défaut si le motif n'est pas respecté."),
      '#access' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    $properties = [
      'phone__pattern' => self::FRENCH_PHONE_NUMBER_PATTERN,
      'phone__pattern_error' => self::PHONE_NUMBER_PATTERN_ERROR,
    ] + parent::defineDefaultProperties();
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);

    $lines = [];
    $lines[] = ($value['last_name'] ? $value['last_name'] : '') .
      ($value['first_name'] ? ' ' . $value['first_name'] : '') .
      ($value['postcode'] ? ' - ' . $value['postcode'] : '') .
      ($value['phone'] ? ' - ' . $value['phone'] : '') .
      ($value['email'] ? ' - ' . $value['email'] : '');
    return $lines;
  }

}
