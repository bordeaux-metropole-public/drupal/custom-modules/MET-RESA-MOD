<?php

namespace Drupal\reservation\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Task permettant l'envoi des mails pour les demandes ayant le statut
 * "Confirmé" pour les notifications "Rappel" et "Enquête"
 *
 * @QueueWorker(
 *   id = "reservation_mail",
 *   title = @Translation("Suppression des demandes dont la date est supérieure
 *   au paramètre en base"), cron = {"time" = 1}
 * )
 */
class TaskWorkerReservationMail extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $mailServices = \Drupal::service('reservation.mail');
    $demandeServices = \Drupal::service('reservation.demande');

    $demandes = $demandeServices->getDemandeRappel();
    $mailServices->generateMultipleEmailByDemande($demandes, 'rappel');

    $demandes = $demandeServices->getDemandeEnquete();
    $mailServices->generateMultipleEmailByDemande($demandes, 'enquete');
  }

}
