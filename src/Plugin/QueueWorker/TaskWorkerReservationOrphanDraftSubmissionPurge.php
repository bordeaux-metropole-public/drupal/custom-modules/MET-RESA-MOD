<?php

namespace Drupal\reservation\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\reservation\ReservationConstants;

/**
 * Task permettant la suppression automatique des soumissions webform
 * encore à l'état brouillon et non associées à des demandes.
 *
 * @QueueWorker(
 *   id = "reservation_submission_purge",
 *   title = @Translation("Suppression des soumissions webform obsolètes"),
 *   cron = {"time" = 1}
 * )
 */
class TaskWorkerReservationOrphanDraftSubmissionPurge extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    $settings = \Drupal::config(ReservationConstants::MODULE_SETTINGS);

    $settings = $settings->get('submission');
    $config_element_name = $settings['config_element_name'] ?? '';
    $purge_strtotime = $settings['purge_strtotime'] ?? '';

    if (empty($purge_strtotime) || empty($config_element_name)) {

      \Drupal::logger(ReservationConstants::LOG_CHANNEL)
        ->info('Configuration de la purge des soumissions obsolètes manquante.');

    }
    else {

      $sql = "SELECT DISTINCT ws.sid FROM webform_submission AS ws
              INNER JOIN webform_submission_data AS wsd
                ON ws.sid = wsd.sid
                     AND wsd.name = :config_element_name
                     AND wsd.property = 'webform_reservation_demande_id'
                     AND wsd.delta = 0
                     AND wsd.value = ''
                       WHERE ws.changed < :before AND ws.in_draft = 1";

      $some_time_ago = strtotime($purge_strtotime);
      $query = \Drupal::database()->query($sql, [
        ':config_element_name' => $config_element_name,
        ':before' => $some_time_ago,
      ]);

      $results = $query->fetchAll();

      $count = 0;
      if (count($results) > 0) {
        $webform_submission_storage =
          \Drupal::entityTypeManager()->getStorage('webform_submission');
        foreach ($results as $row) {
          $webform_submission = $webform_submission_storage->load($row->sid);
          if ($webform_submission) {
            $webform_submission->delete();
            $count++;
          }
        }
      }

      \Drupal::logger(ReservationConstants::LOG_CHANNEL)
        ->info('Nombre de soumissions webform obsolètes purgées : %count',
          ['%count' => $count]);
    }

  }

}
