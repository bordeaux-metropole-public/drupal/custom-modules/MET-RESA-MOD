<?php

namespace Drupal\reservation\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Task permettant de mettre à jour les entités réservation (deprecated)
 *
 * @QueueWorker(
 *   id = "reservation_tasks_entity_updates",
 *   title = @Translation("Mise à jour des entités réservation (deprecated)"),
 *   cron = {"time" = 1}
 * )
 */
class TaskWorkerReservationEntityUpdate extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Nothing.
  }

}
