<?php

namespace Drupal\reservation\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\Entity\ReservationDemandeToken;
use Drupal\reservation\ReservationConstants;
use Drupal\reservation\ReservationUtils;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform submission action handler.
 *
 * @todo .
 *
 * @WebformHandler(
 *   id = "reservation_ng",
 *   label = @Translation("Réservation V2 (Webform)"),
 *   category = @Translation("Réservation"),
 *   description = @Translation("Réservation V2 (Webform)"),
 *   cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class ReservationNGWebformHandler extends WebformHandlerBase {

  /**
   *
   * @var \Drupal\reservation\Service\ReservationDemandeServices
   */
  protected $demandeServices;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $reservationSettings;

  /**
   * @var mixed
   */
  private $expirationDelay;

  /**
   * @var mixed
   */
  private $expirationCautionDelay;

  /**
   * @var mixed
   */
  private $expirationDelayModeManuel;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->messenger = $container->get('messenger');
    $instance->demandeServices = $container->get('reservation.demande');
    $instance->reservationSettings = \Drupal::config(ReservationConstants::MODULE_SETTINGS);
    $tokenSettings = $instance->reservationSettings->get('token');
    $instance->expirationDelay = $tokenSettings[ReservationDemandeToken::TOKEN_TIME_LIMIT];
    $instance->expirationCautionDelay = $tokenSettings[ReservationDemandeToken::TOKEN_TIME_CAUTION];
    $instance->expirationDelayModeManuel = $tokenSettings[ReservationDemandeToken::TOKEN_TIME_MANUELLE];
    return $instance;
  }

  /**
   * @return array
   */
  public function getSummary() {
    // We don't need a summary template.
    return [];
  }

  /**
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function preSave(WebformSubmissionInterface $webform_submission) {
    parent::preSave($webform_submission);
  }

  /**
   * Acts on a saved webform submission before the insert or update hook is
   * invoked.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission.
   * @param bool $update
   *   TRUE if the entity has been updated, or FALSE if it has been inserted.
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    parent::postSave($webform_submission, $update);

    $demande = $this->loadDemandeFromWebformSubmission($webform_submission);
    if ($demande != NULL) {
      if (empty($demande->getSid()) && !empty($webform_submission->id())) {
        $demande->setSid($webform_submission->id());
        $demande->setWebformToken($webform_submission->getToken());
        $demande->save();
      }
    }
  }

  /**
   * @param $webform_submission
   *
   * @return \Drupal\reservation\Entity\ReservationDemande
   */
  private function loadDemandeFromWebformSubmission($webform_submission) {
    $demande_id = ReservationUtils::getWebformSubmissionDemandeId($webform_submission);
    return empty($demande_id) ? NULL : $this->demandeServices->load($demande_id);
  }

  /**
   * Confirm webform submission webform.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission.
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    parent::confirmForm($form, $form_state, $webform_submission);

    // On ne se trouve pas dans le cas d'une modification de notes.
    if (\Drupal::routeMatch()
      ->getRouteName() != 'entity.webform_submission.notes_form') {
      $demande = $this->loadDemandeFromWebformSubmission($webform_submission);
      if (!empty($demande)) {
        $this->clearAllMessages();
        $this->completeDemandeFromWebformSubmission($demande, $webform_submission);
        $demande->save();
        $this->proceedAfterConfirmation($demande, $webform_submission);
      }
    }
  }

  /**
   *
   */
  private function clearAllMessages() {
    // Pour masquer le message concernant le délai d'expiration du token.
    $this->messenger->deleteAll();
    $session = \Drupal::request()->getSession();
    $bag = $session->getBag('flashes');
    $bag->clear();
    $session->save();
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  private function completeDemandeFromWebformSubmission(
    ReservationDemande $demande,
    WebformSubmissionInterface $webform_submission) {
    if ($demande->getStatut() == ReservationDemande::STATUT_FORMULAIRE
      || $demande->getStatut() == ReservationDemande::STATUT_CAUTION) {
      $ressourceNode = $demande->getDate()->getReservationRessourceNode();
      if (empty($ressourceNode)) {
        \Drupal::logger(ReservationConstants::LOG_CHANNEL)
          ->error('Reservation date resource node missing.');
      }
      else {
        if ($ressourceNode->getCautionStatut()) {
          // On supprime le délai d'expiration de la demande une fois la
          // soumission du webform finalisée. Les éventuelles absences de
          // retour serveur Ogone ou autres problèmes concernant les CBs seront
          // gérés manuellement en back-office.
          $demande->setWebformTokenExpiration(NULL);
          $caution_elt_name = ReservationUtils::getCautionElementName($this->getWebform());
          $caution = $webform_submission->getElementData($caution_elt_name);
          if ($caution === ReservationDemande::CAUTION_CARTE) {
            // Le statut est défini à l'ouverture de la modale dans
            // la méthode cautionCarteBancaireV2 du DemandeController.
            // On ne modifie pas le statut ici, car le retour serveur Ogone a
            // potentiellement déjà eu lieu.
            $demande->setCaution(ReservationDemande::CAUTION_CARTE);
          }
          else {
            if ($caution === ReservationDemande::CAUTION_CHEQUE) {
              $demande->setCaution(ReservationDemande::CAUTION_CHEQUE);
            }
            else {
              $demande->setCaution(ReservationDemande::CAUTION_NON_RENSEIGNEE);
            }
            // Il faut passer la demande au statut en attente afin que le
            // mail de notification correspondant soit envoyé à l'utilisateur.
            $demande->setStatut(ReservationDemande::STATUT_ATTENTE);
          }
        }
        else {
          if ($ressourceNode->getAutomatique()) {
            $demande->setStatut(ReservationDemande::STATUT_CONFIRME);
          }
          else {
            $demande->setStatut(ReservationDemande::STATUT_ATTENTE);
          }
        }
      }
    }

    $this->updateDemandeDemandeurFromWebform($demande, $webform_submission);
    $submissionData = $webform_submission->getData();
    if (isset($submissionData['webform_reservation_demandeur'])) {
      $demandeur = $submissionData['webform_reservation_demandeur'];
      if ($demandeur != NULL) {
        $demande->setDemandeur(
          $demandeur["last_name"] ?? '',
          $demandeur["first_name"] ?? '');
        $demande->setTelephone($demandeur["phone"] ?? '');
        $demande->setEmail($demandeur["email"] ?? '');
        $demande->setPostcode($demandeur["postcode"] ?? '');
      }
    }
    if (isset($submissionData['webform_reservation_accompagnants'])) {
      $accompagnants = $submissionData['webform_reservation_accompagnants'];
      if (!empty($accompagnants)) {
        try {
          $accompagnantsStr = $this->buildAccompagnantsString($accompagnants);
          // Nom du champ paramétrable ?
          $demande->set('field_accompagnants', $accompagnantsStr);
        }
        catch (\Exception $error) {
          \Drupal::logger(ReservationConstants::LOG_CHANNEL)
            ->info($error->getMessage());
        }
      }
    }

    $this->writeWebformValuesToDemandeFields($demande, $webform_submission);
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return bool
   */
  private function updateDemandeDemandeurFromWebform(
    ReservationDemande $demande,
    WebformSubmissionInterface $webform_submission) {
    $result = FALSE;
    $submissionData = $webform_submission->getData();
    $demandeur = $submissionData['webform_reservation_demandeur'];
    if ($demandeur != NULL) {
      $demande->setDemandeur(
        $demandeur["last_name"] ?? '',
        $demandeur["first_name"] ?? '');
      $demande->setTelephone($demandeur["phone"] ?? '');
      $demande->setEmail($demandeur["email"] ?? '');
      $demande->setPostcode($demandeur["postcode"] ?? '');
      $result = TRUE;
    }
    return $result;
  }

  /**
   * @param $accompagnants
   *
   * @return string
   */
  private function buildAccompagnantsString($accompagnants) {
    $accompagnantsStr = '';
    $i = 0;
    foreach ($accompagnants as $accompagnant) {
      if ($i > 0) {
        $accompagnantsStr .= "\n";
      }
      $accompagnantsStr .= ($accompagnant['last_name'] ? $accompagnant['last_name'] : '') .
        ($accompagnant['first_name'] ? ' ' . $accompagnant['first_name'] : '') .
        ($accompagnant['postcode'] ? ' ' . $accompagnant['postcode'] : '') .
        ($accompagnant['phone'] ? ' ' . $accompagnant['phone'] : '') .
        ($accompagnant['email'] ? ' ' . $accompagnant['email'] : '');
      $i++;
    }
    return $accompagnantsStr;
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function writeWebformValuesToDemandeFields(
    ReservationDemande $demande,
    WebformSubmissionInterface $webform_submission) {
    $webform = $webform_submission->getWebform();
    $webformElements = $webform->getElementsInitializedAndFlattened();
    // $resaWebformConfigElt =
    // $webform->getElement('webform_reservation_config');
    $demandeFieldDefs = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions(
        'reservation_demande', 'reservation_demande');
    $webformElementNames = array_keys($webformElements);
    $demandeFieldNames = array_keys($demandeFieldDefs);
    // Exclude specifically handled reservation_demande fields to avoid
    // overwrites.
    $demandeFieldNames = array_diff($demandeFieldNames,
      [
        'rdmid',
        'rdid',
        'rhid',
        'sid',
        'statut',
        'jauge',
        'demandeur',
        'email',
        'telephone',
        'postcode',
        'created',
        'changed',
        'caution',
        'webform_token',
        'webform_token_expiration',
        'field_accompagnants',
      ]
    );
    $commonFieldNames = array_intersect($webformElementNames, $demandeFieldNames);
    if (count($commonFieldNames) > 0) {
      $data = $webform_submission->getData();
      foreach ($commonFieldNames as $name) {
        // $webformElt = $webform->getElement($name);
        /** @var \Drupal\field\Entity\FieldStorageConfig $field */
        $field = $demandeFieldDefs[$name];
        $value = $data[$name];
        if ($value) {
          if ($field->getType() == 'datetime') {
            $dateValue = new \DateTime($value);
            ReservationUtils::setDateEntityFieldValue($demande, $name, $dateValue);
          }
          else {
            $demande->set($name, $value);
          }
        }
      }
    }
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  private function proceedAfterConfirmation(
    ReservationDemande $demande,
    WebformSubmissionInterface $webform_submission) {
    if ($demande->getStatut() != ReservationDemande::STATUT_FORMULAIRE) {
      // Envoi Mail confirmation enregistrement réservation.
      /** @var \Drupal\reservation\Service\ReservationMailServices $mailServices */
      $mailServices = \Drupal::service('reservation.mail');
      $mailServices->generateEmailById($demande->id(), $demande->getStatut());
    }
  }

  /**
   * @param array $settings
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function overrideSettings(array &$settings, WebformSubmissionInterface $webform_submission) {
    parent::overrideSettings($settings, $webform_submission);
    if ($webform_submission->getCurrentPage() === 'webform_confirmation' && !$webform_submission->isNew()) {
      if (empty($settings['confirmation_message'])) {
        $demande = $this->loadDemandeFromWebformSubmission($webform_submission);
        if (!empty($demande)) {
          if ($demande->getStatut() == ReservationDemande::STATUT_CONFIRME) {
            $confirmationMessage = $this->confirmationMessage('message_confirmation_acceptee');
          }
          else {
            $confirmationMessage = $this->confirmationMessage('message_confirmation_pre_reservation');
          }
          $settings['confirmation_message'] = $confirmationMessage;
        }
      }
    }
  }

  /**
   * @param $messageId
   *
   * @return string
   */
  private function confirmationMessage($messageId) {
    $reservationSettings = \Drupal::config('reservation.settings');
    return '<h2>' . $reservationSettings->get('message_validation')[$messageId] . '</h2>';
  }

  /**
   * @param array $variables
   */
  public function preprocessConfirmation(array &$variables) {
    parent::preprocessConfirmation($variables);
  }

  /**
   * Lors du passage de la page_1 à la page_2, une demande temporaire est
   * générée avec un statut intermédiaire. Cela crée une limitation des dates
   * disponibles et enregistre les premières informations de la demande.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    parent::validateForm($form, $form_state, $webform_submission);

    if (!$form_state->hasAnyErrors()) {

      if ($this->isRessourceImpactingSubmit($form_state)) {

        // Lock d'un token pour assurer qu'une seule personne ne peut réserver
        // un même créneau.
        $resaLockId = $this->getResaLockId($webform_submission);

        $lock = \Drupal::lock();

        // 50 seconds lock max TODO paramétrable ?
        if ($lock->acquire($resaLockId, 50)) {

          $this->secureValidateForm($form, $form_state, $webform_submission);

        }
        else {

          $form_state->setErrorByName(
            'reservation-calendar-datepicker', 'Date ou horaire en cours de réservation par un autre utilisateur.');

        }

        $lock->release($resaLockId);

      }
      else {

        $demande = $this->getDemandeValideFromWebformSubmission($webform_submission);

        // Demande probablement expirée et purgée.
        if (empty($demande)) {
          $this->reset($form, $form_state, $webform_submission, FALSE);
          if (!ReservationUtils::isAdmin()) {
            $this->displayErrorMessageById('message_temps_depasse');
          }
        }
        else {
          if (!ReservationUtils::isAdmin()) {
            if ($this->isDemandeExpired($demande)) {
              $this->reset($form, $form_state, $webform_submission, FALSE);
              $this->displayErrorMessageById('message_temps_depasse');
            }
            else {
              if ($demande->getStatut() != ReservationDemande::STATUT_FORMULAIRE
                && $demande->getStatut() != ReservationDemande::STATUT_CAUTION) {
                // FM 20240130: commenté car bloque l'accès à l'écran de
                // confirmation après le traitement du retour Ogone.
                // $form_state->setErrorByName('reservation-calendar-datepicker', 'Votre demande n\'est plus modifiable.');
              }
              else {
                $this->updateDemandeIfNeeded($form_state, $webform_submission, $demande);
              }
            }
          }
          else {
            $this->updateDemandeIfNeeded($form_state, $webform_submission, $demande);
          }
        }
      }
    }
  }

  /**
   * Cette méthode permet d'identifier que la soumission en cours concerne des
   * éléments qui impactent la disponibilité des ressources : choix d'une date,
   * d'un horaire ou du nombre d'accompagnants. Dès que l'un de ces trois
   * champs est présent dans la soumission on effectuera une vérification des
   * disponibilités de la ressource demandée.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   */
  private function isRessourceImpactingSubmit(FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    return isset($input['webform_reservation_calendar']) ||
      isset($input['webform_reservation_horaire']) || isset($input['webform_reservation_nb_accompagnants']);
  }

  /**
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return string
   */
  private function getResaLockId(WebformSubmissionInterface $webform_submission) {
    $rd = $this->getRessourceDataFromWebformSubmission($webform_submission);
    return 'reservation_ReservationDemandeWebformHandler_verificationDisponibilite_lock_' . $rd['rdid'] . '_' . $rd['rhid'];
  }

  /**
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return array
   */
  private function getRessourceDataFromWebformSubmission(WebformSubmissionInterface $webform_submission) {
    $submissionData = $webform_submission->getData();
    $rdid = $submissionData['webform_reservation_calendar'];
    $rhid = isset($submissionData['webform_reservation_horaire'])
      ? ($submissionData['webform_reservation_horaire']['reservation-horaire-select'] ?? NULL) : NULL;
    return ['rdid' => $rdid, 'rhid' => $rhid];
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @throws \Exception
   */
  private function secureValidateForm(
    array &$form,
    FormStateInterface $form_state,
    WebformSubmissionInterface $webform_submission) {
    $demande_id = ReservationUtils::getWebformSubmissionDemandeId($webform_submission);

    // Nouvelle soumission car pas de demande associée.
    if (empty($demande_id)) {

      // On vérifie les disponibilités de la ressource demandée et on initialise
      // une demande pour bloquer le creneau.
      /** @var \Drupal\reservation\Entity\ReservationDemande $demande */
      $demande = $this->checkDisponibilitesAndCreateDemande($webform_submission);

      if ($demande == NULL) {

        $form_state->setErrorByName('reservation-calendar-datepicker', 'Date ou horaire indisponible.');

      }
      else {

        // Stockage du numéro de la demande dans la soumission webform.
        $webform_reservation_config = $webform_submission->getElementData('webform_reservation_config');
        $webform_reservation_config['webform_reservation_demande_id'] = $demande->id();
        $webform_submission->setElementData('webform_reservation_config', $webform_reservation_config);
        // REQUIRED do not remove!
        $form_state->setValue('webform_reservation_config', $webform_reservation_config);

        // On sauvegarde certaines informations de la page dans la demande.
        $this->updateDemandeIfNeeded($form_state, $webform_submission, $demande);

        // Affichage d'un message à l'utilisateur lui indiquant le délai requis
        // pour finaliser sa demande.
        $this->displayReservationExpirationAlertMessage($demande->getWebformTokenExpiration());

      }

    }
    else {

      $demande = $this->getDemandeValideFromWebformSubmission($webform_submission);

      // Demande probablement expirée et supprimée par le batch.
      if (empty($demande)) {
        if (!ReservationUtils::isAdmin()) {
          $this->reset($form, $form_state, $webform_submission, FALSE);
          $this->displayErrorMessageById('message_temps_depasse');
        }
      }
      else {
        $doCheckAndUpdate = FALSE;
        if (ReservationUtils::isAdmin()) {
          $doCheckAndUpdate = TRUE;
        }
        else {
          if ($this->isDemandeExpired($demande)) {
            $this->reset($form, $form_state, $webform_submission, FALSE);
            $this->displayErrorMessageById('message_temps_depasse');
          }
          else {
            if ($demande->getStatut() != ReservationDemande::STATUT_FORMULAIRE
              && $demande->getStatut() != ReservationDemande::STATUT_CAUTION) {
              $form_state->setErrorByName('reservation-calendar-datepicker', 'Votre demande n\'est plus modifiable.');
            }
            else {
              $doCheckAndUpdate = TRUE;
            }
          }
        }
        if ($doCheckAndUpdate) {
          if ($this->checkDisponibilitesAndUpdateDemande($webform_submission, $demande)) {
            // On sauvegarde certaines informations de la page dans la demande.
            $this->updateDemandeIfNeeded($form_state, $webform_submission, $demande);
          }
          else {
            $form_state->setErrorByName('reservation-calendar-datepicker', 'Date ou horaire non disponible.');
          }
        }
      }
    }
  }

  /**
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return mixed
   * @throws \Exception
   */
  private function checkDisponibilitesAndCreateDemande(WebformSubmissionInterface $webform_submission) {
    $demande = NULL;

    $nbAccompagnants = $this->getNombreAccompagnantsFromSubmission($webform_submission);
    // Demandeur + accompagnants.
    $nbPlacesDemandees = 1 + $nbAccompagnants;

    // Par défaut, le demandeur.
    $maxPlacesAutorisees = 1;
    $nbAccompElt = $webform_submission->getWebform()
      ->getElement('webform_reservation_nb_accompagnants');
    if (!empty($nbAccompElt)) {
      $maxPlacesAutorisees += $nbAccompElt['#jauge'] ?? ReservationConstants::DEFAULT_NB_ACCOMPAGNANTS;
    }

    if ($nbPlacesDemandees <= $maxPlacesAutorisees) {
      /** @var \Drupal\reservation\Service\ReservationCalendarServices $calendarServices */
      $calendarServices = \Drupal::service('reservation.calendar');
      $rd = $this->getRessourceDataFromWebformSubmission($webform_submission);
      if ($calendarServices->verificationDisponibilite($rd['rdid'], $rd['rhid'], $nbPlacesDemandees)) {
        // Si la ressource est disponible on initialise une demande pour
        // réserver la date/créneau.
        $delay = ReservationUtils::isGestionnaireManuel() ? $this->expirationDelayModeManuel : $this->expirationDelay;
        $tokenExpirationTime = ReservationUtils::addExpirationDelayToDateTime(new \DateTime(), $delay);
        $demande = $this->demandeServices->createDemandeFromWebform(
          $rd['rdid'], $rd['rhid'], ReservationDemande::STATUT_FORMULAIRE,
          $nbPlacesDemandees, $webform_submission->getToken(), $tokenExpirationTime);
      }
    }

    return $demande;
  }

  /**
   * @param array $submissionData
   *
   * @return int
   */
  private function getNombreAccompagnantsFromSubmission(WebformSubmissionInterface $webform_submission) {
    $nbAccompagnants = 0;
    $nbAccompagnantsData = $webform_submission->getElementData('webform_reservation_nb_accompagnants');
    if (!empty($nbAccompagnantsData)) {
      if (isset($nbAccompagnantsData['reservation-accompagnateur-select'])) {
        $nbAccompagnants = intval($nbAccompagnantsData['reservation-accompagnateur-select']);
      }
    }
    return $nbAccompagnants;
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   *
   * @return void
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function updateDemandeIfNeeded(
    FormStateInterface $form_state,
    WebformSubmissionInterface $webform_submission,
    ReservationDemande $demande) {
    $save_demande = $this->updateDemandeDemandeurIfNeeded(
      $form_state, $webform_submission, $demande);
    if ($save_demande) {
      $demande->save();
    }
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   *
   * @return bool
   */
  private function updateDemandeDemandeurIfNeeded(
    FormStateInterface $form_state,
    WebformSubmissionInterface $webform_submission,
    ReservationDemande $demande) {
    $result = FALSE;
    if ($this->isDemandeurImpactingSubmit($form_state)) {
      $result = $this->updateDemandeDemandeurFromWebform($demande, $webform_submission);
    }
    return $result;
  }

  /**
   * Cette méthode permet d'identifier que la soumission en cours concerne des
   * éléments qui impactent le demandeur de la réservation.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   */
  private function isDemandeurImpactingSubmit(FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    return isset($input['webform_reservation_demandeur']);
  }

  /**
   * @param \DateTime $expirationTime
   *
   * @throws \Exception
   */
  public function displayReservationExpirationAlertMessage(\DateTime $expirationTime) {
    $this->messenger->deleteAll();
    $this->messenger->addMessage(t("Vous avez jusqu'à @expirationTime pour finaliser votre réservation.",
      ['@expirationTime' => $expirationTime->format('H:i')]), MessengerInterface::TYPE_WARNING, FALSE);
  }

  /**
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return \Drupal\reservation\Entity\ReservationDemande|null
   */
  private function getDemandeValideFromWebformSubmission(WebformSubmissionInterface $webform_submission) {
    $demandeValide = NULL;
    $demande_id = ReservationUtils::getWebformSubmissionDemandeId($webform_submission);
    if (!empty($demande_id)) {
      /** @var \Drupal\reservation\Entity\ReservationDemande $demande */
      $demande = $this->demandeServices->load($demande_id);
      if (!empty($demande)) {
        if ($demande->getWebformToken() == $webform_submission->getToken() &&
          ($demande->getWebformTokenExpiration() >= new \DateTime() ||
            ($demande->getWebformTokenExpiration() === NULL && ReservationUtils::isAdmin()))) {
          $demandeValide = $demande;
        }
      }
    }
    return $demandeValide;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param bool $full
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function reset(
    array &$form,
    FormStateInterface $form_state,
    WebformSubmissionInterface $webform_submission,
    bool $full = TRUE) {
    // Reset submission data.
    $this->removeDemandeIdFromWebformSubmission($webform_submission, $form_state, $full);
    $this->removeDemandeIdFromFormState($form, $form_state, $full);

    // Ask for form rebuild.
    // Requis, ne fonctionne pas sans.
    $form_state->setRebuild();

    // Tentative de purge des messages, pas très concluant à priori.
    $this->clearAllMessages();
  }

  /**
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param bool $full
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function removeDemandeIdFromWebformSubmission(
    WebformSubmissionInterface $webform_submission,
    FormStateInterface $form_state,
    bool $full = FALSE) {
    if ($full) {
      // On supprimer toutes les informations de la soumission webform.
      $webform_submission->setData([]);
    }
    else {
      // On supprime uniquement la référence à la demande qui a expirée.
      $webform_reservation_config =
        $webform_submission->getElementData('webform_reservation_config');
      if (isset($webform_reservation_config['webform_reservation_demande_id'])) {
        unset($webform_reservation_config['webform_reservation_demande_id']);
        $webform_submission->setElementData('webform_reservation_config', $webform_reservation_config);
      }
    }

    // Reset current page.
    $webform_submission->setCurrentPage('');
    $this->setWebformSubmission($webform_submission);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param bool $full
   */
  private function removeDemandeIdFromFormState(
    array &$form,
    FormStateInterface $form_state,
    bool $full = FALSE) {

    if ($full) {
      // Reset user input but preserve form tokens.
      $form_state->setUserInput(array_intersect_key($form_state->getUserInput(), [
        'form_build_id' => 'form_build_id',
        'form_token' => 'form_token',
        'form_id' => 'form_id',
      ]));
      // Reset values.
      $form_state->setValues([]);
    }
    else {
      // On supprime uniquement la référence à la demande qui a expirée.
      $userInput = $form_state->getUserInput();
      if (isset($userInput['webform_reservation_config'])) {
        $webform_reservation_config = $userInput['webform_reservation_config'];
        unset($webform_reservation_config['webform_reservation_demande_id']);
        $userInput['webform_reservation_config'] = $webform_reservation_config;
        $form_state->setUserInput($userInput);
      }
      $webform_reservation_config = $form_state->getValue('webform_reservation_config');
      if (isset($webform_reservation_config['webform_reservation_demande_id'])) {
        unset($webform_reservation_config['webform_reservation_demande_id']);
        $form_state->setValue('webform_reservation_config', $webform_reservation_config);
      }
      if (isset($form['elements']['webform_reservation_config']['#default_value'])) {
        $formResaConfigDefaults = $form['elements']['webform_reservation_config']['#default_value'];
        if (isset($formResaConfigDefaults['webform_reservation_demande_id'])) {
          unset($formResaConfigDefaults['webform_reservation_demande_id']);
          $form['elements']['webform_reservation_config']['#default_value'] = $formResaConfigDefaults;
        }
      }
    }

    // Reset current page.
    $storage = $form_state->getStorage();
    unset($storage['current_page']);
    $form_state->setStorage($storage);
  }

  /**
   * @param $messageId
   *
   * @throws \Exception
   */
  public function displayErrorMessageById($messageId) {
    $message = $this->reservationSettings->get('message_validation')[$messageId];
    $this->displayErrorMessage($message);
  }

  /**
   * @param $messageId
   *
   * @throws \Exception
   */
  public function displayErrorMessage($message) {
    $this->displayMessage($message, MessengerInterface::TYPE_ERROR);
  }

  /**
   * @param $messageId
   * @param $type
   *
   * @throws \Exception
   */
  public function displayMessage($message, $type) {
    $this->messenger->deleteAll();
    $this->messenger->addMessage($message, $type);
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   *
   * @return bool
   * @throws \Exception
   */
  private function isDemandeExpired(ReservationDemande $demande) {
    $date_now = new \DateTime();
    $tokenExpiration = $demande->getWebformTokenExpiration();
    return ($tokenExpiration && $tokenExpiration < $date_now);
  }

  /**
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   *
   * @return bool
   */
  private function checkDisponibilitesAndUpdateDemande(
    WebformSubmissionInterface $webform_submission,
    ReservationDemande $demande) {

    $nbAccompagnants = $this->getNombreAccompagnantsFromSubmission($webform_submission);
    // Demandeur + accompagnants.
    $nbPlacesDemandees = 1 + $nbAccompagnants;

    // Par défaut, le demandeur.
    $maxPlacesAutorisees = 1;
    $nbAccompElt = $webform_submission->getWebform()
      ->getElement('webform_reservation_nb_accompagnants');
    if (!empty($nbAccompElt)) {
      $maxPlacesAutorisees += $nbAccompElt['#jauge'] ?? ReservationConstants::DEFAULT_NB_ACCOMPAGNANTS;
    }

    $ressourceDisponible = FALSE;

    if ($nbPlacesDemandees <= $maxPlacesAutorisees) {

      $rdwfs = $this->getRessourceDataFromWebformSubmission($webform_submission);
      $rddmd = $this->getRessourceDataFromDemande($demande);
      $ressourceChanged = !($rdwfs == $rddmd);

      $nbPlacesDejaReservees = $demande->getJauge();
      $deltaPlacesAReserver = $nbPlacesDemandees - $nbPlacesDejaReservees;

      // Autre ressource ou plus de places demandees.
      $checkRequired = $ressourceChanged || ($deltaPlacesAReserver > 0);
      // Autre ressource ou modification des places.
      $updateNeeded = $ressourceChanged || ($deltaPlacesAReserver != 0);

      if ($checkRequired) {
        $calendarServices = \Drupal::service('reservation.calendar');
        // Pas de changement de la ressource réservée.
        if ($rdwfs == $rddmd) {
          // On vérifie qu'il y a encore suffisamment de places disponibles
          // pour notre ressource.
          if ($calendarServices->verificationDisponibilite($rdwfs['rdid'], $rdwfs['rhid'], $deltaPlacesAReserver)) {
            $ressourceDisponible = TRUE;
          }
        }
        // Nouvelle ressource demandée.
        else {
          if ($calendarServices->verificationDisponibilite($rdwfs['rdid'], $rdwfs['rhid'], $nbPlacesDemandees)) {
            $ressourceDisponible = TRUE;
          }
        }
      }
      else {
        $ressourceDisponible = TRUE;
      }

      if ($ressourceDisponible && $updateNeeded) {
        $demande->setRdid($rdwfs['rdid']);
        $demande->setRhid($rdwfs['rhid']);
        $demande->setJauge($nbPlacesDemandees);
        $demande->save();
      }
    }

    return $ressourceDisponible;
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDemande $demande
   *
   * @return array
   */
  private function getRessourceDataFromDemande(ReservationDemande $demande) {
    $rdid = empty($demande->getDate()) ? NULL : $demande->getDate()->id();
    $rhid = empty($demande->getHoraire()) ? NULL : $demande->getHoraire()->id();
    return ['rdid' => $rdid, 'rhid' => $rhid];
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function alterForm(
    array &$form,
    FormStateInterface $form_state,
    WebformSubmissionInterface $webform_submission) {
    // Gestion du champ multivalué des accompagnants.
    if (isset($form['elements'])) {
      $form_element = &$this->getFormElementByPluginIdAndType(
        $form['elements'], 'webform_reservation_accompagnants');
      if (!empty($form_element)) {
        $cardinality = $this->getNombreAccompagnantsFromSubmission($webform_submission);
        if ($cardinality === 0) {
          // On masque le champ si aucun accompagnant n'est à renseigner.
          $form_element['#access'] = FALSE;
        }
        else {
          // On paramètre le champ multivalué en fonction du nb d'accompagnants
          // choisi
          // $form_element['#title'] .= ' (' . $cardinality . ')';.
          $form_element['#multiple'] = $cardinality;
          $form_element['#multiple__min_items'] = $cardinality;
          $form_element['#multiple__empty_items'] = $cardinality;
          $form_element['#element']['#webform_multiple'] = $cardinality;
          $form_element['#element']['#multiple__min_items'] = $cardinality;
          $form_element['#element']['#multiple__empty_items'] = $cardinality;
          $form_element['#cardinality'] = $cardinality;
          $form_element['#min_items'] = $cardinality;
          $form_element['#empty_items'] = $cardinality;
          $form_element_name = $form_element['#webform_key'];
          $storage = $form_state->getStorage();
          $storage['webform_multiple__' . $form_element_name . '__number_of_items'] = $cardinality;
          $form_state->setStorage($storage);
        }
      }
    }
  }

  /**
   * @param array $items
   * @param $plugin_id
   * @param $type
   *
   * @return array|mixed
   */
  private function &getFormElementByPluginIdAndType(array &$items, $plugin_id, $type = NULL) {
    $formElement = NULL;
    foreach ($items as $formitem_name => &$formitem_value) {
      if (is_array($formitem_value) && isset($formitem_value['#type'])
        && (($formitem_value['#type'] == 'container')
          || ($formitem_value['#type'] == 'webform_wizard_page'))) {
        $element = &$this->getFormElementByPluginIdAndType($formitem_value, $plugin_id, $type);
        if (!empty($element)) {
          if (empty($type) || (isset($element['#type']) && $element['#type'] == $type)) {
            $formElement =& $element;
            break;
          }
        }
      }
      else {
        if (isset($formitem_value['#webform_plugin_id']) && $formitem_value['#webform_plugin_id'] == $plugin_id) {
          if (empty($type) || (isset($formitem_value['#type']) && $formitem_value['#type'] == $type)) {
            $formElement =& $formitem_value;
          }
        }
      }
    }
    return $formElement;
  }

  /**
   * Gestion des actions suite à l'appui d'un des boutons d'actions lors de
   * l'édition d'une demande
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
    WebformSubmissionInterface $webform_submission) {
    parent::submitForm($form, $form_state, $webform_submission);

    // Vérification de l'expiration éventuelle de la demande sur actions
    // n'entraînant pas de validate (Previous par ex)
    if (in_array('::noValidate', $form_state->getTriggeringElement()['#validate'])) {
      $demande = $this->getDemandeValideFromWebformSubmission($webform_submission);
      // Demande probablement expirée et purgée.
      if (empty($demande)) {
        $this->reset($form, $form_state, $webform_submission, FALSE);
        $this->displayErrorMessageById('message_temps_depasse');
      }
    }

    // Suppression des données des accompagnants surnuméraires suite à réduction
    // du nombre d'accompagnants sinon webform affiche par défaut autant de
    // formulaires de saisie que de données disponibles.
    $form_element = &$this->getFormElementByPluginIdAndType(
      $form['elements'], 'webform_reservation_accompagnants');
    if (!empty($form_element)) {
      $form_element_name = $form_element['#webform_key'];
      $form_element_values = $webform_submission->getElementData($form_element_name);
      $cardinality = $this->getNombreAccompagnantsFromSubmission($webform_submission);
      if (is_array($form_element_values) && count($form_element_values) > $cardinality) {
        $form_element_values = array_slice($form_element_values, 0, $cardinality);
        $webform_submission->setElementData($form_element_name, $form_element_values);
      }
    }

  }

}
