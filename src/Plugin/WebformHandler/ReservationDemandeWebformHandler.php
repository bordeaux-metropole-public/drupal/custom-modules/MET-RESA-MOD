<?php

namespace Drupal\reservation\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\Entity\ReservationDemandeToken;
use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Webform submission action handler.
 *
 * Cet Handler permet de réaliser les tâches d'association entre un webform et
 * les demandes de réservation.
 * - Créer une demande
 * - Mettre à jour une demande
 * - Redirection vers une caution.
 *
 * @WebformHandler(
 *   id = "reservation_demande",
 *   label = @Translation("Réservation Demande"),
 *   category = @Translation("Réservation Demande"),
 *   description = @Translation("Création d'une demande de réservation suite à
 *   la soumission d'un webform"), cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class ReservationDemandeWebformHandler extends WebformHandlerBase {

  /**
   *
   * @var mixed
   */
  protected $caution = FALSE;

  /**
   *
   * @var mixed
   */
  protected $token = FALSE;

  /**
   * Séparation entre création et édition lors de la soumission d'un webform.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   * @param mixed $update
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    if (!$webform_submission->getOriginalData()) {
      if (\Drupal::currentUser()->hasPermission('creation manuelle demande')) {
        $this->creationDemandeManuelle($webform_submission);
      }
      else {
        $this->creationDemande($webform_submission);
      }
    }
    else {
      // l'utilisateur est administrateur et ne se trouve pas dans le cas d'une
      // modification de notes.
      if (\Drupal::currentUser()
        ->hasPermission('administer reservation demande')
        && \Drupal::routeMatch()
          ->getRouteName() != 'entity.webform_submission.notes_form') {
        $this->modificationDemande($webform_submission);
      }
    }
  }

  /**
   * Lors de la soumission finale, les informations du webform sont récupérées
   * pour être ajouté à la demande.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function creationDemandeManuelle(WebformSubmissionInterface $webform_submission) {
    $demandeTokenServices = \Drupal::service('reservation.demande.token');
    $submission = $webform_submission->getData();

    $token = $webform_submission->getToken();
    if ($demandeTokenServices->checkToken($token) === TRUE) {
      $this->token = $token;
      $demande = $demandeTokenServices->loadDemandeByToken($token);
      $ressourcenode = ReservationRessourceNode::load($submission["webform_reservation"]);
      if ($ressourcenode->getCautionStatut()) {
        $demande->setStatut(ReservationDemande::STATUT_CAUTION);
        $demande->setCaution(ReservationDemande::CAUTION_CHEQUE);
      }
      else {
        if ($ressourcenode->getAutomatique()) {
          $demande->setStatut(ReservationDemande::STATUT_CONFIRME);
        }
        else {
          $demande->setStatut(ReservationDemande::STATUT_ATTENTE);
        }
      }

      $rdid = $submission["webform_reservation_calendar"];
      $rhid = $this->getSubmissionHoraire($submission);
      $nbPersonnes = $this->calculNbPersonnes($this->getSubmissionAccompagnateur($submission));
      $demande->setRdid($rdid);
      $demande->setRhid($rhid);
      $demande->setDemandeur(
        $submission["nom"] ?? '', $submission["prenom"] ?? '');
      $demande->setJauge($nbPersonnes);
      $demande->setTelephone($submission["telephone"] ?? '');
      $demande->setEmail($submission["email"] ?? '');
      $demande->setSid($webform_submission->Id());
      $demande->setWebformToken($webform_submission->getToken());
      $demande->save();
    }
  }

  /**
   * @param array $submission
   *
   * @return mixed|null
   */
  private function getSubmissionHoraire(array $submission) {
    return $submission['webform_reservation_horaire']['reservation-horaire-select'] ?? NULL;
  }

  /**
   * @param mixed $accompagnants
   *
   * @return int
   */
  private function calculNbPersonnes($accompagnants = NULL) {
    if ($accompagnants == NULL) {
      return 1;
    }
    else {
      return intval($accompagnants) + 1;
    }
  }

  /**
   * @param array $submission
   *
   * @return mixed
   */
  private function getSubmissionAccompagnateur(array $submission) {
    return $submission['webform_reservation_accompagnateur']['reservation-accompagnateur-select'] ?? NULL;
  }

  /**
   * Lors de la soumission finale, les informations du webform sont récupérées
   * pour être ajouté à la demande.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function creationDemande(WebformSubmissionInterface $webform_submission) {
    $demandeTokenServices = \Drupal::service('reservation.demande.token');
    $submission = $webform_submission->getData();

    $token = $webform_submission->getToken();
    if ($demandeTokenServices->checkToken($token) === TRUE) {
      $this->token = $token;
      $demande = $demandeTokenServices->loadDemandeByToken($token);
      $ressourcenode = ReservationRessourceNode::load($submission["webform_reservation"]);
      if ($ressourcenode->getCautionStatut()) {
        $demande->setStatut(ReservationDemande::STATUT_CAUTION);
        $demande->setCaution(ReservationDemande::CAUTION_NON_RENSEIGNEE);
      }
      else {
        if ($ressourcenode->getAutomatique()) {
          $demande->setStatut(ReservationDemande::STATUT_CONFIRME);
        }
        else {
          $demande->setStatut(ReservationDemande::STATUT_ATTENTE);
        }
      }

      $rdid = $submission["webform_reservation_calendar"];
      $rhid = $this->getSubmissionHoraire($submission);
      $nbPersonnes = $this->calculNbPersonnes($this->getSubmissionAccompagnateur($submission));
      $demande->setRdid($rdid);
      $demande->setRhid($rhid);
      $demande->setDemandeur(
        $submission["nom"] ?? '', $submission["prenom"] ?? '');
      $demande->setJauge($nbPersonnes);
      $demande->setTelephone($submission["telephone"] ?? '');
      $demande->setEmail($submission["email"] ?? '');
      $demande->setSid($webform_submission->Id());
      $demande->setWebformToken($webform_submission->getToken());
      $demande->save();
    }
  }

  /**
   * Fonction d'édition d'une demande depuis le panneau "Réservation ->
   * demandes"
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function modificationDemande(WebformSubmissionInterface $webform_submission) {
    $reservationDemande = \Drupal::service('reservation.demande');
    $submission = $webform_submission->getData();

    $rdmid = $reservationDemande->getByWebform($webform_submission->id());
    if ($rdmid) {
      $demande = $reservationDemande->load($rdmid);
      if ($demande) {
        $rdid = $submission["webform_reservation_calendar"];
        $rhid = $this->getSubmissionHoraire($submission);
        $nbPersonnes = $this->calculNbPersonnes($this->getSubmissionAccompagnateur($submission));

        $demande->setRdid($rdid);
        $demande->setRhid($rhid);
        $demande->setDemandeur(
          $submission["nom"] ?? '', $submission["prenom"] ?? '');
        $demande->setJauge($nbPersonnes);
        $demande->setTelephone($submission["telephone"] ?? '');
        $demande->setEmail($submission["email"] ?? '');
        $demande->setSid($webform_submission->Id());
        $demande->setWebformToken($webform_submission->getToken());
        $demande->save();

        /** @var \Drupal\reservation\Service\ReservationMailServices $mailServices */
        $mailServices = \Drupal::service('reservation.mail');
        $mailServices->generateEmailById($demande->id(), 'modification');
      }
    }
  }

  /**
   * Lors du passage de la page_1 à la page_2, une demande temporaire  est
   * générée avec un statut intermédiaire. Cela créé une limitation des dates
   * disponibles et enregistre les premières informations de la demande.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    parent::validateForm($form, $form_state, $webform_submission);
    if ($form_state->hasAnyErrors()) {
      return;
    }
    $storage = $form_state->getStorage();

    if ($storage['current_page'] == 'page_1' && ($storage['form_display']->getMode() == 'add' || $storage['form_display']->getMode() == 'test')) {
      $demandeServices = \Drupal::service('reservation.demande');
      $demandeTokenServices = \Drupal::service('reservation.demande.token');
      $caldendarServices = \Drupal::service('reservation.calendar');

      $submission = $webform_submission->getData();
      $rdid = $submission["webform_reservation_calendar"];
      $rhid = $this->getSubmissionHoraire($submission);

      // Lock d'un token pour assurer qu'une seule personne ne peut réserver un
      // même créneau.
      $tokenLock = 'reservation_ReservationDemandeWebformHandler_verificationDisponibilite_lock_' . $rdid . "_" . $rhid;
      $lock = \Drupal::lock();
      if ($lock->acquire($tokenLock, 50)) {
        $nbPersonnesTotal = $this->calculNbPersonnes($this->getSubmissionAccompagnateur($submission));
        $mustValidateDispo = TRUE;
        $nbPersonnesNewToValidate = $nbPersonnesTotal;

        $token = $webform_submission->getToken();
        $demande = NULL;
        if ($demandeTokenServices->checkToken($token)) {
          $demande = $demandeTokenServices->loadDemandeByToken($token);
          $nbPersonnesPrecedentes = $demande->getJauge();

          if ($nbPersonnesPrecedentes <= $nbPersonnesTotal && $rdid == $demande->getRdid() && $rhid == $demande->getRhid()) {
            $mustValidateDispo = FALSE;
          }
          else {
            $nbPersonnesNewToValidate = $nbPersonnesTotal - $nbPersonnesPrecedentes;
          }
        }

        $verification = TRUE;
        if ($mustValidateDispo) {
          $verification = $caldendarServices->verificationDisponibilite($rdid, $rhid, $nbPersonnesNewToValidate);
        }

        if ($verification) {
          // Création d'une demande en début de formulaire, pour bloquer la
          // dispo pour la durée du panier.
          if ($demande != NULL) {
            $demandeToken = $demandeTokenServices->loadToken($token);
            $demande->setRdid($rdid);
            $demande->setRhid($rhid);
            $demande->setJauge($nbPersonnesTotal);

            $demande->save();
            $demandeToken->setCreatedTime($demande->getCreatedTime());
            $demandeToken->save();
          }
          else {
            $demande = $demandeServices->createDemande($rdid, $rhid, $webform_submission->Id(), ReservationDemande::STATUT_FORMULAIRE, '', $nbPersonnesTotal, '', '');

            if (\Drupal::currentUser()
              ->hasPermission('creation manuelle demande')) {
              $demandeToken = $demandeTokenServices->createDemandeToken($demande, $token, ReservationDemandeToken::TOKEN_TIME_MANUELLE);
            }
            else {
              $demandeToken = $demandeTokenServices->createDemandeToken($demande, $token, ReservationDemandeToken::TOKEN_TIME_LIMIT);
              $demandeTokenServices->messageTime($demandeToken);
            }
          }

          // Stockage du numéro de la demande dans la soumission webform.
          $submission['webform_reservation_demande_id'] = $demande->id();
          $webform_submission->setData($submission);

        }
        else {
          if ($rhid == NULL) {
            $form_state->setErrorByName('reservation-calendar-datepicker', t('Jour en cours de réservation par un autre utilisateur.'));
          }
          else {
            $form_state->setErrorByName('reservation-calendar-datepicker', t('Disponibilité insuffiante pour effectuer la réservation sur ce créneau.'));
          }
        }

        $lock->release($tokenLock);
      }
      else {
        if ($rhid == NULL) {
          $form_state->setErrorByName('reservation-calendar-datepicker', t('Jour en cours de réservation par un autre utilisateur.'));
        }
        else {
          $form_state->setErrorByName('reservation-calendar-datepicker', t('Créneau en cours de réservation par un autre utilisateur.'));
        }
      }
    }
  }

  /**
   * Redirection de la page du webform vers différentes page en fonction du
   * rôle et création/édition
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $storage = $form_state->getStorage();
    if (\Drupal::currentUser()
      ->hasPermission('administer reservation demande') && $storage['form_display']->getMode() == 'edit') {
      $url = \Drupal::request()->query->get('url');
      $current_request_query_parameters = \Drupal::requestStack()
        ->getCurrentRequest()->query->all();
      // The following parameters are specific to the demande edit url,
      // so we remove them to avoid duplication.
      unset($current_request_query_parameters['rdmid']);
      unset($current_request_query_parameters['url']);
      $url = $url == NULL ? 'reservation.demande.simple' : $url;
      $redirectUrl = Url::fromRoute($url, $current_request_query_parameters);
      $this->setRedirectUrl($form_state, $redirectUrl);
    }
    else {
      $token = $webform_submission->getToken();
      $demandeTokenServices = \Drupal::service('reservation.demande.token');
      $demande = $demandeTokenServices->loadDemandeByToken($token);
      if ($demande->getStatut() == ReservationDemande::STATUT_CAUTION) {
        $demandeToken = $demandeTokenServices->loadToken($token);
        $demandeToken->setTimeout(ReservationDemandeToken::TOKEN_TIME_CAUTION);
        $demandeToken->save();
        $redirectUrl = Url::fromRoute('reservation.demande.caution.redirection', ['token' => $token]);
        $this->setRedirectUrl($form_state, $redirectUrl);
      }
      else {
        if (\Drupal::currentUser()
          ->hasPermission('creation manuelle demande')) {
          $date_debut = new \DateTime();
          $date_fin = new \DateTime();
          $date_fin->add(new \DateInterval('P1D'));
          $redirectUrl = Url::fromRoute(
            'reservation.demande.simple',
            [
              'ressource' => [
                $demande->getDate()
                  ->getReservationRessourceNode()
                  ->getNode()
                  ->id(),
              ],
              'statut' => ['caution'],
              'choix_date_demande' => TRUE,
              'date_demande_debut' => $date_debut->format('Y-m-d'),
              'date_demande_fin' => $date_fin->format('Y-m-d'),
            ]
          );
          $this->setRedirectUrl($form_state, $redirectUrl);
          $demandeTokenServices->destroyToken($token);
        }
        else {
          $redirectUrl = Url::fromRoute('reservation.demande.traitement.token', ['token' => $token]);
          $this->setRedirectUrl($form_state, $redirectUrl);
        }
      }
    }
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Url $redirect_url
   */
  protected function setRedirectUrl(FormStateInterface $form_state, Url $redirect_url) {
    // $form_state->setRedirectUrl($redirect_url);
    $response = new TrustedRedirectResponse($redirect_url->toString());
    $form_state->setResponse($response);
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $storage = $form_state->getStorage();
    if (\Drupal::currentUser()
      ->hasPermission('administer reservation demande') && $storage['form_display']->getMode() == 'edit') {
      unset($form['information']);
      unset($form['navigation']);
      $demandeActionServices = \Drupal::service('reservation.demande.form');
      $form = $demandeActionServices->tableDemandeForm($form, $webform_submission);
      $form = $demandeActionServices->tableActionForm($form);
      $reservationSettings = \Drupal::config('reservation.settings');
      $demandeSettings = $reservationSettings->get('demande');
      if ($demandeSettings['caution'] == TRUE) {
        $form = $demandeActionServices->tableCautionForm($form);
      }
    }
  }

  /**
   * Gestion des actions suite à l'appui d'un des boutons d'actions lors de
   * l'édition d'une demande
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $storage = $form_state->getStorage();
    if (\Drupal::currentUser()
      ->hasPermission('administer reservation demande') && $storage['form_display']->getMode() == 'edit') {

      $trigger = $form_state->getTriggeringElement();
      /** @var \Drupal\reservation\Service\ReservationDemandeServices $demandeServices */
      $demandeServices = \Drupal::service('reservation.demande');
      /** @var \Drupal\reservation\Service\ReservationMailServices $mailServices */
      $mailServices = \Drupal::service('reservation.mail');

      $rdmid = $demandeServices->getByWebform($webform_submission->id());

      $url = \Drupal::request()->query->get('url');
      $current_request_query_parameters = \Drupal::requestStack()
        ->getCurrentRequest()->query->all();
      // The following parameters are specific to the demande edit url, so we
      // remove them to avoid duplication.
      unset($current_request_query_parameters['rdmid']);
      unset($current_request_query_parameters['url']);

      $url = $url == NULL ? 'reservation.demande.simple' : $url;
      $redirectUrl = Url::fromRoute($url, $current_request_query_parameters);
      $do_redirect = TRUE;

      /*
       * Modification du statut lors de l'édition par un gestionnaire
       */
      switch ($trigger['#name']) {
        case ReservationDemande::STATUT_CONFIRME:
          $demandeServices->setStatut($rdmid, ReservationDemande::STATUT_CONFIRME);
          $mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_CONFIRME);
          break;

        case ReservationDemande::STATUT_ANNULE:
          $demandeServices->setStatut($rdmid, ReservationDemande::STATUT_ANNULE);
          $mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_ANNULE);
          break;

        case ReservationDemande::STATUT_ATTENTE:
          $demandeServices->setStatut($rdmid, ReservationDemande::STATUT_ATTENTE);
          $mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_ATTENTE);
          break;

        case ReservationDemande::STATUT_CAUTION:
          $demandeServices->setStatut($rdmid, ReservationDemande::STATUT_CAUTION);
          break;

        case ReservationDemande::STATUT_REFUSE:
          $demandeServices->setStatut($rdmid, ReservationDemande::STATUT_REFUSE);
          $mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_REFUSE);
          break;

        case ReservationDemande::STATUT_ARCHIVE:
          $demandeServices->setStatut($rdmid, ReservationDemande::STATUT_ARCHIVE);
          break;

        case ReservationDemande::STATUT_NO_SHOW:
        case ReservationDemande::STATUT_SHOW:
          $demandeServices->setStatut($rdmid, $trigger['#name']);
          break;

        case ReservationDemande::STATUT_RAPPEL:
          $mailServices->generateEmailById($rdmid, ReservationDemande::STATUT_RAPPEL);
          break;

        case 'delete':
          $redirectUrl = Url::fromRoute(
            'reservation.demande.delete',
            array_merge([
              'rdmid' => [$rdmid],
              'type' => 'simple',
            ], $current_request_query_parameters)
          );
          break;

        default:
          $do_redirect = FALSE;
      }

      /*
       * Modification du type de caution lors de l'édition par un gestionnaire
       */
      $reservationSettings = \Drupal::config('reservation.settings');
      $demandeSettings = $reservationSettings->get('demande');
      if ($demandeSettings['caution'] == TRUE) {
        $do_redirect = TRUE;
        switch ($trigger['#name']) {
          case 'caution_na':
            $demandeServices->setCaution($rdmid, ReservationDemande::CAUTION_NON_APPLICABLE);
            break;

          case 'caution_nr':
            $demandeServices->setCaution($rdmid, ReservationDemande::CAUTION_NON_RENSEIGNEE);
            break;

          case 'caution_carte':
            $demandeServices->setCaution($rdmid, ReservationDemande::CAUTION_CARTE);
            break;

          case 'caution_cheque':
            $demandeServices->setCaution($rdmid, ReservationDemande::CAUTION_CHEQUE);
            break;

          default:
            $do_redirect = FALSE;
        }
      }

      if ($do_redirect) {
        $this->setRedirectUrl($form_state, $redirectUrl);
      }
    }

    /*
     * Dans le cas d'une demande dépassée, le webform va être positionné en
     * draft, pour ne pas le prendre en compte dans les traitement futurs
     * (notification...)
     */
    $demandeTokenServices = \Drupal::service('reservation.demande.token');
    $token = $webform_submission->getToken();
    if ($demandeTokenServices->checkToken($token) === FALSE) {
      $webform_submission->set('in_draft', TRUE);
    }

    $submission = $webform_submission->getData();
    $ressourcenode = ReservationRessourceNode::load($submission["webform_reservation"]);
    if (!empty($ressourcenode) && $ressourcenode->getCautionStatut()) {
      $webform_submission->set('in_draft', TRUE);
    }
  }

  /**
   * Affichage des boutons d'actions au dessus de la demande, lors d'une
   * édition.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return string
   */
  public function actionDemandeForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $form['table_action'] = [
      '#type' => 'table',
      '#tableselect' => FALSE,
      '#weight' => 0,
    ];

    $form['table_action'][0]['confirme'] = [
      '#type' => 'submit',
      '#name' => 'confirme',
      '#value' => 'Confirmer',
    ];

    $form['table_action'][0]['annule'] = [
      '#type' => 'submit',
      '#name' => 'annule',
      '#value' => 'Annuler',
    ];

    $form['table_action'][0]['attente'] = [
      '#type' => 'submit',
      '#name' => 'attente',
      '#value' => 'En attente',
    ];

    $form['table_action'][0]['refuse'] = [
      '#type' => 'submit',
      '#name' => 'refuse',
      '#value' => 'Refuser',
    ];

    $form['table_action'][0]['rappel'] = [
      '#type' => 'submit',
      '#name' => 'rappel',
      '#value' => 'Mail Rappel',
    ];

    $form['table_action'][0]['archive'] = [
      '#type' => 'submit',
      '#name' => 'archive',
      '#value' => 'Archive',
    ];

    $form['table_action'][0]['show'] = [
      '#type' => 'submit',
      '#name' => 'show',
      '#value' => t('Resa Show'),
    ];

    $form['table_action'][0]['noshow'] = [
      '#type' => 'submit',
      '#name' => 'noshow',
      '#value' => 'NoShow',
    ];

    $form['table_action'][0]['delete'] = [
      '#type' => 'submit',
      '#name' => 'delete',
      '#value' => 'Supprimer',
    ];

    return $form;
  }

}
