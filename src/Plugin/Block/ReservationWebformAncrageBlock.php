<?php

namespace Drupal\reservation\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "reservation_webform_ancrage",
 *   admin_label = @Translation("Webform Block Ancrage"),
 *   category = @Translation("Reservation")
 * )
 */
class ReservationWebformAncrageBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<div id="block-webform-reservation-ancrage"></div>',
    ];
  }

}
