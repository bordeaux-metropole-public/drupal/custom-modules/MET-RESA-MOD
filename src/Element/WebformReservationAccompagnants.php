<?php

namespace Drupal\reservation\Element;

/**
 * Provides a 'webform_reservation_accompagnants'.
 *
 * @FormElement("webform_reservation_accompagnants")
 */
class WebformReservationAccompagnants extends WebformReservationPersonne {

}
