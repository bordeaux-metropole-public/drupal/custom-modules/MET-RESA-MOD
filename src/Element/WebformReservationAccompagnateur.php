<?php

namespace Drupal\reservation\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Element Webform Custom permettant d'afficher un champ select, contenant une
 * jauge dynamique de place.
 *
 * @FormElement("webform_reservation_accompagnateur")
 */
class WebformReservationAccompagnateur extends WebformCompositeBase {

  /**
   *
   */
  public static function initializeCompositeElements(array &$element) {
    $composite_elements = static::getCompositeElements($element);
    if (!\Drupal::currentUser()->hasPermission('creation manuelle demande')) {
      // $element['#attached']['library'][] = 'reservation/reservation.place';
      // @todo corriger l'appel qui devrait fonctionner autant en premiere page du formulaire que dans les pages suivantes
    }
    static::initializeCompositeElementsRecursive($element, $composite_elements);
    return $composite_elements;
  }

  /**
   *
   * @param array $element
   *
   * @return string
   */
  public static function getCompositeElements(array $element) {
    $elements = [];
    if (isset($element["#jauge"])) {
      $options = array_combine(range(1, $element["#jauge"]), range(1, $element["#jauge"]));
    }
    else {
      $options = [];
    }

    $defaultValue = $element["#default_value"] ?? 0;

    $elements['reservation-accompagnateur-select'] = [
      '#id' => 'reservation-accompagnateur-select',
      '#type' => 'select',
      '#title' => 'Nombre d\'accompagnants',
      '#options' => $options,
      "#default_value" => $defaultValue,
      '#validated' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderWebformCompositeFormElement($element) {
    $element = parent::preRenderCompositeFormElement($element);

    $jauge = $element['#jauge'];
    $element['reservation-accompagnateur-select']['#attributes']['data-maximum'] = $jauge;

    $value = $element['#value']['reservation-accompagnateur-select'] ?? NULL;
    $element['reservation-accompagnateur-select']['#attributes']['data-original'] = $value;

    return $element;
  }

  /**
   *
   * @return string
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#id'] = 'reservation-accompagnateur';

    return $info;
  }

}
