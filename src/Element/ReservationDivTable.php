<?php

namespace Drupal\reservation\Element;

use Drupal\Core\Render\Element\Table;

/**
 * Class ReservationDivTable.
 *
 * @FormElement("reservation_div_table")
 */
class ReservationDivTable extends Table {

  /**
   * {@inheritdoc}
   */
  public static function preRenderTable($element) {
    $element['#attached']['library'][] = 'reservation/reservation_div_table';
    return parent::preRenderTable($element);
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'reservation_div_table',
    ] + parent::getInfo();
  }

}
