<?php

namespace Drupal\reservation\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a 'webform_reservation_calendar'.
 *
 * Element Webform Custom permet d'afficher le calendrier de type "datepicker",
 * avec un jeu de statut "disponible, clos, en attente, vide".
 *
 * @FormElement("webform_reservation_calendar")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 * @see \Drupal\webform_datepicker_element\Element\WebformExampleElement
 */
class WebformReservationCalendar extends FormElement {

  /**
   * Processes a 'webform_datepicker_element' element.
   */
  public static function processCalendar(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add and manipulate your element's properties and callbacks.
    return $element;
  }

  /**
   * Webform element validation handler for #type 'webform_datepicker_element'.
   */
  public static function validateCalendar(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add custom validation logic.
  }

  /**
   * Prepares a #type 'email_multiple' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderCalendar(array $element) {

    $element['#attributes']['type'] = 'hidden';
    $element['#attributes']['data-original'] = $element['#value'];
    $element['#title'] = '';
    $element['#attached']['library'][] = 'reservation/reservation.calendar';
    Element::setAttributes($element, [
      'id',
      'name',
      'value',
      'size',
      'maxlength',
      'placeholder',
    ]); /*défini les attributs de l'éléments si défini dans $element*/

    $attributes = ['form-text', 'webform-reservation-calendar'];

    if (isset($element['#showElements']) && $element['#showElements'] === 1) {
      $attributes[] = 'showElements';
    }

    if (\Drupal::routeMatch()->getRouteName() != NULL && \Drupal::routeMatch()
      ->getRouteName() != 'entity.webform_submission.edit_form') {

      if (isset($element['#showplacedate']) && $element['#showplacedate'] === 1) {
        $attributes[] = 'showplacedate';
      }

      if (isset($element['#showplacehoraire']) && $element['#showplacehoraire'] === 1) {
        $attributes[] = 'showplacehoraire';
      }

      if (isset($element['#autorefresh']) && $element['#autorefresh'] === 1) {
        $attributes[] = 'autorefresh';
      }

    }
    else {

      $attributes[] = 'showplacedate';
      $attributes[] = 'showplacehoraire';
      $attributes[] = 'autorefresh';

    }

    static::setAttributes($element, $attributes); /* Définit le nom des classes de l'élément créé */

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this); /*renvoi: 'Drupal\reservation\Element\WebformReservationCalendar'*/

    return [
      '#input' => FALSE,
      '#id' => 'reservation-calendar',
      '#process' => [
        [$class, 'processCalendar'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateCalendar'],
      ],
      '#pre_render' => [
        [$class, 'preRenderCalendar'],
      ],
      '#theme' => 'input__webform_reservation_calendar',
      '#theme_wrappers' => ['form_element'],
      '#prefix' => '<div id="reservation-calendar-datepicker">',
      '#suffix' => '</div>',
    ];
  }

}
