<?php

namespace Drupal\reservation\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a webform element for an address element.
 *
 * Element Webform Custom permet d'afficher un select d'horaire disponibles.
 *
 * @FormElement("webform_reservation_horaire")
 */
class WebformReservationHoraire extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
  }

  /**
   *
   * @param array $element
   *
   * @return string
   */
  public static function getCompositeElements(array $element) {
    $elements = [];
    $options = [];
    $default_value = NULL;

    if (!empty($element["#default_value"]) && is_string($element["#default_value"])) {
      $calendarServices = \Drupal::service('reservation.calendar');
      $horaireServices = \Drupal::service('reservation.horaire');
      $reservationHoraire = $horaireServices->load($element["#default_value"]);
      if (!empty($reservationHoraire)) {
        $horaires = $horaireServices->getByRdid($reservationHoraire->getDate()
          ->id());

        foreach ($horaires as $horaire) {
          $date = $calendarServices->getDemandeHoraireDetail($horaire);
          $place = $date["place"] + 0;
          $options[$horaire->id()] = $horaire->getHeureDebutFormat('H:i') . $horaire->getHeureFinFormat(' - H:i') . " " . $place . " place(s) restante(s)";
        }
        $default_value = $reservationHoraire->id();
      }
    }

    $elements['reservation-horaire-select'] = [
      '#id' => 'reservation-horaire-select',
      '#type' => 'select',
      '#title' => 'Choix de l\'horaire pour le <span id="span-reservation-horaire-title"></span>',
      '#validated' => TRUE,
      '#options' => $options,
      '#default_value' => $default_value,
    ];

    return $elements;
  }

  /**
   *
   */
  public static function preRenderWebformCompositeFormElement($element) {
    $element = parent::preRenderCompositeFormElement($element);

    if (isset($element['#value'])) {
      if (isset($element['#value']['reservation-horaire-select'])) {
        $horaire_id_value = $element['#value']['reservation-horaire-select'];
        $element['reservation-horaire-select']['#attributes']['data-original'] = $horaire_id_value;
      }
    }

    return $element;
  }

  /**
   *
   * @return string
   */
  public function getInfo() {
    return [
      '#id' => 'reservation-horaire',
      '#prefix' => '<div id="reservation-horaire-wrapper">',
      '#suffix' => '</div>',
    ] + parent::getInfo();
  }

}
