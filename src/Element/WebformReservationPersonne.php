<?php

namespace Drupal\reservation\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a 'webform_reservation_personne'.
 *
 * @FormElement("webform_reservation_personne")
 */
class WebformReservationPersonne extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public static function getCompositeElements(array $element) {
    $elements = [];
    $elements['first_name'] = [
      '#type' => 'textfield',
      '#title' => t('First name'),
    ];
    $elements['last_name'] = [
      '#type' => 'textfield',
      '#title' => t('Last name'),
    ];
    $elements['postcode'] = [
      '#type' => 'textfield',
      '#title' => t('Postal code'),
      // Use #after_build to add #states.
      // '#after_build' => [[get_called_class(), 'afterBuild']],.
    ];
    $elements['phone'] = [
      '#type' => 'textfield',
      '#title' => t('Phone'),
      // Use #after_build to add #states.
      // '#after_build' => [[get_called_class(), 'afterBuild']],.
    ];
    $elements['email'] = [
      '#type' => 'email',
      '#title' => t('Email'),
      // Use #after_build to add #states.
      // '#after_build' => [[get_called_class(), 'afterBuild']],.
    ];

    return $elements;
  }

  /**
   * Performs the after_build callback.
   */
  public static function afterBuild(array $element, FormStateInterface $form_state) {
    // Add #states targeting the specific element and table row.
    preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
    $composite_name = $match[1];
    $element['#states']['disabled'] = [
      [':input[name="' . $composite_name . '[first_name]"]' => ['empty' => TRUE]],
      [':input[name="' . $composite_name . '[last_name]"]' => ['empty' => TRUE]],
    ];
    // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
    // disabling the entire table row when this element is disabled.
    $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
    return $element;
  }

  /**
   *
   */
  public static function preRenderWebformCompositeFormElement($element) {
    $element = parent::preRenderCompositeFormElement($element);
    $element['first_name']['#attributes']['class'][] = 'reservation-personne-first-name';
    $element['last_name']['#attributes']['class'][] = 'reservation-personne-last-name';
    $element['postcode']['#attributes']['class'][] = 'reservation-personne-postcode';
    $element['phone']['#attributes']['class'][] = 'reservation-personne-phone';
    $element['email']['#attributes']['class'][] = 'reservation-personne-email';
    return $element;
  }

  /**
   *
   */
  public static function processWebformCompositeElementsRecursive(&$element, array &$composite_elements, FormStateInterface $form_state, &$complete_form) {
    parent::processWebformCompositeElementsRecursive($element, $composite_elements, $form_state, $complete_form);
  }

  /**
   *
   */
  public static function processWebformComposite(&$element, FormStateInterface $form_state, &$complete_form) {
    return parent::processWebformComposite($element, $form_state, $complete_form);
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'webform_reservation_personne',
    ] + parent::getInfo();
  }

}
