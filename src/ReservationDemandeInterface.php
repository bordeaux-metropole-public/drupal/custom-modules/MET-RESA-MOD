<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a ReservationDate entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup reservation
 */
interface ReservationDemandeInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the ReservationDemande name.
   *
   * @return string
   *   Name of the ReservationDemande.
   */
  public function getName();

  /**
   * Sets the ReservationDemande name.
   *
   * @param string $name
   *   The ReservationDemande name.
   *
   * @return \Drupal\reservation\ReservationDemandeInterface
   *   The called ReservationDemande entity.
   */
  public function setName($name);

}
