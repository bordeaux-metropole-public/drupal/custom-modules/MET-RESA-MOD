<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a ReservationRessource entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup reservation
 */
interface ReservationRessourceUserInterface extends ContentEntityInterface, EntityChangedInterface {


}
