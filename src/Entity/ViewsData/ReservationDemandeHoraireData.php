<?php

namespace Drupal\reservation\Entity\ViewsData;

/**
 * Provides the views data for the entity ReservationDemande.
 */
class ReservationDemandeHoraireData extends EntityViewsDataBase {

  /**
   *
   */
  protected function getDatetimeColumnsAsStringArray() {
    $datetime_columns = [
      'heure_debut',
      'heure_fin',
    ];

    return $datetime_columns;
  }

}
