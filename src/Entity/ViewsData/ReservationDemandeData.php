<?php

namespace Drupal\reservation\Entity\ViewsData;

/**
 * Provides the views data for the entity ReservationDemande.
 */
class ReservationDemandeData extends EntityViewsDataBase {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['reservation_demande']['sid_raw'] = [
      'title' => $this->t('Webform Submission ID'),
      'help' => $this->t('The sid of the related webform submission.'),
      'real field' => 'sid',
      'filter' => [
        'id' => 'numeric',
        'allow empty' => TRUE,
      ],
    ];

    return $data;
  }

  /**
   *
   */
  protected function getDatetimeColumnsAsStringArray() {
    $datetime_columns = [
      'webform_token_expiration',
    ];

    return $datetime_columns;
  }

}
