<?php

namespace Drupal\reservation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\reservation\ReservationRessourceInterface;

/**
 * Defines the ContentEntityExample entity.
 *
 * Paramétrage permettant de rendre une ressource réservable.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_ressource",
 *   label = @Translation("ressource entity"),
 *   base_table = "reservation_ressource",
 *   entity_keys = {
 *     "id" = "rrid"
 *   }
 * )
 */
class ReservationRessource extends ContentEntityBase implements ReservationRessourceInterface {

  use EntityChangedTrait;

  /**
   *
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['rrid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the reservationhoraire entity.'))
      ->setReadOnly(TRUE);

    // Standard field, used as unique if primary index.
    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Ressource'))
      ->setRequired(TRUE)
      ->setDescription(t('Type node connecté à une réservation'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['rgpd'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('RGPD'))
      ->setDefaultValue(2)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['statut'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('statut'))
      ->setDescription(t('The statut of the reservationressource entity.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['caution_statut'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Statut de la Caution'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['caution_montant'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Montant de la Caution'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatut() {
    return $this->get('statut')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRgpd() {
    return $this->get('rgpd')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCautionStatut() {
    return ($this->get('caution_statut')->value) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCautionMontant() {
    return $this->get('caution_montant')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCautionStatut($caution_statut) {
    $this->set('caution_statut', $caution_statut);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCautionMontant($caution_montant) {
    $this->set('caution_montant', $caution_montant);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatut($statut) {
    $this->set('statut', $statut);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setRgpd($rgpd) {
    $this->set('rgpd', $rgpd);
    return $this;
  }

}
