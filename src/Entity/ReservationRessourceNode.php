<?php

namespace Drupal\reservation\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\reservation\ReservationRessourceNodeInterface;

/**
 * Defines the ContentEntityExample entity.
 *
 * Paramétrage d'une ressource réservable (caution, automatique....).
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_ressource_node",
 *   label = @Translation("ressource node entity"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "reservation_ressource_node",
 *   entity_keys = {
 *     "id" = "nid"
 *   }
 * )
 */
class ReservationRessourceNode extends ContentEntityBase implements ReservationRessourceNodeInterface {

  use EntityChangedTrait;

  /**
   *
   */
  public static function getNodeById($nid) {
    $node = ReservationRessourceNode::load($nid);
    return $node;
  }

  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *
   * @return array|\Drupal\Core\Field\FieldDefinitionInterface[]|mixed
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Node ID'))
      ->setDescription(t('Node lié à la réservation.'))
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setReadOnly(FALSE);

    $fields['rrid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Ressource ID'))
      ->setDescription(t('Ressource liée au node.'))
      ->setSetting('target_type', 'reservation_ressource')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'title',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setReadOnly(FALSE);

    $fields['automatique'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Automatique'))
      ->setDescription(t('Reservation Automatique'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['minimum_delay'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Minimum delay'))
      ->setDescription(t('Minimum reservation delay in days'))
      ->setDefaultValue(0)
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['minimum_delay_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Minimum delay type'))
      ->setDescription(t('Minimum reservation delay type'))
      ->setDefaultValue('D')
      ->setRequired(TRUE)
      ->setSetting('max_length', 1)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['statut'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('statut'))
      ->setDescription(t('Reservation Node statut'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['caution_statut'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Statut de la Caution'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['caution_cheque_accepte'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Chèque de caution accepté'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['caution_montant'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Montant de la Caution'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Standard field, used as unique if primary index.
    $fields['email_to'] = BaseFieldDefinition::create('email')
      ->setLabel(t('To Email'))
      ->setRequired(TRUE)
      ->setDefaultValue('[reservation_demande:email]')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 21,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 21,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMinimumDelay($minimum_delay) {
    $this->set('minimum_delay', $minimum_delay);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setMinimumDelayType($minimum_delay_type) {
    $this->set('minimum_delay_type', $minimum_delay_type);
    return $this;
  }

  /**
   * @return mixed
   */
  public function getStatut() {
    return $this->get('statut')->value;
  }

  /**
   * @param $statut
   *
   * @return $this
   */
  public function setStatut($statut) {
    $this->set('statut', $statut);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCautionStatut() {
    return ($this->get('caution_statut')->value) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setCautionStatut($caution_statut) {
    $this->set('caution_statut', $caution_statut);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isChequeAccepte() {
    return ($this->get('caution_cheque_accepte')->value) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setChequeAccepte($cheque_accepte) {
    $this->set('caution_cheque_accepte', $cheque_accepte);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCautionMontant() {
    return $this->get('caution_montant')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCautionMontant($caution_montant) {
    $this->set('caution_montant', $caution_montant);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutomatique() {
    return $this->get('automatique')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAutomatique($automatique) {
    $this->set('automatique', $automatique);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailTo() {
    return $this->get('email_to')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getNode() {
    return $this->get('nid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmailTo($email_to) {
    $this->set('email_to', $email_to);
    return $this;
  }

  /**
   * @param $date
   */
  public function isOffLimit($resaDate) {
    try {
      $minimumDelay = $this->getMinimumDelay();
      $minimumDelayType = $this->getMinimumDelayType();
      $minimumDelay = $minimumDelay ?? 0;
      $minimumDelayType = $minimumDelayType ?? 'H';
    }
    catch (\Exception $e) {
      $minimumDelay = 0;
      $minimumDelayType = 'H';
    }
    if ($minimumDelay > 0) {
      $now = new \DateTime();
      // Hours.
      if ($minimumDelayType == 'H') {
        $resaMinDate = $now->add(new \DateInterval('PT' . $minimumDelay . 'H'));
      }
      else {
        // Days.
        if ($minimumDelayType == 'D') {
          $today = $now->setTime(0, 0, 0, 0);
          $resaMinDate = $today->add(new \DateInterval('P' . $minimumDelay . 'D'));
        }
        else {
          $resaMinDate = $now;
        }
      }
      $offLimit = $resaMinDate > $resaDate;
    }
    else {
      $offLimit = FALSE;
    }
    return $offLimit;
  }

  /**
   * {@inheritdoc}
   */
  public function getMinimumDelay() {
    return $this->get('minimum_delay')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMinimumDelayType() {
    return $this->get('minimum_delay_type')->value;
  }

}
