<?php

namespace Drupal\reservation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a ReservationDate entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup reservation
 */
interface ReservationMailInterface extends ContentEntityInterface, EntityChangedInterface {


}
