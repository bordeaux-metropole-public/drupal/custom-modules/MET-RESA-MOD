<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DemandeDeleteForm extends FormBase {

  /**
   * @var mixed
   */
  protected $rdmid;

  /**
   * @var mixed
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'demande_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $query_parameters = \Drupal::request()->query->all();
    $this->rdmid = $query_parameters['rdmid'];
    $this->type = $query_parameters['type'];

    $form['validation'] = [
      '#type' => 'item',
      '#markup' => 'Etes vous sûr de vouloir supprimer ?',
    ];

    $form['supprimer'] = [
      '#type' => 'submit',
      '#name' => 'supprimer',
      '#value' => 'Supprimer',
    ];

    $form['annuler'] = [
      '#type' => 'submit',
      '#name' => 'annuler',
      '#value' => 'Annuler',
    ];

    return $form;
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $query_parameters = \Drupal::request()->query->all();
    $trigger = $form_state->getTriggeringElement();

    if ($trigger['#name'] == 'supprimer') {
      $demandeServices = \Drupal::service('reservation.demande');
      $count = $demandeServices->deleteMultipleDemande($this->rdmid);

      $this->messenger()
        ->addMessage($count . ' demande(s) supprimée(s)', 'info');
    }

    unset($query_parameters['rdmid']);
    $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.' . $this->type, [$query_parameters]));
  }

}
