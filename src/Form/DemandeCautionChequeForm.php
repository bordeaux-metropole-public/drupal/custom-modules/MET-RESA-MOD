<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class DemandeCautionChequeForm.
 *
 * @package Drupal\reservation\Form
 */
class DemandeCautionChequeForm extends FormBase {

  /**
   *
   * @var mixed
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'demande_caution_cheque_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $token = NULL) {

    $this->token = $token;

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Chèque'),
    ];

    return $form;
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.caution.traitement', [
      'etat' => 'accept',
      'caution' => 'cheque',
      'token' => $this->token,
    ]));
  }

}
