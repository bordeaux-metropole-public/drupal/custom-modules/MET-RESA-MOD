<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDemande;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DemandeMultipleForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'demande_multiple_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'reservation/reservation.demande';

    /** @var \Drupal\reservation\Service\ReservationDemandeServices $reservationDemande */
    $reservationDemande = \Drupal::service('reservation.demande');
    /** @var \Drupal\reservation\Service\ReservationDemandeFormServices $demandeFormServices */
    $demandeFormServices = \Drupal::service('reservation.demande.form');
    $tokenServices = \Drupal::service('reservation.demande.token');
    $tokenServices->destroyTokenObsolete();
    $reservationSettings = \Drupal::config('reservation.settings');
    $demandeSettings = $reservationSettings->get('demande');

    $mois_fr = [
      1 => 'Janvier',
      2 => 'Février',
      3 => 'Mars',
      4 => 'Avril',
      5 => 'Mai',
      6 => 'Juin',
      7 => 'Juillet',
      8 => 'Août',
      9 => 'Septembre',
      10 => 'Octobre',
      11 => 'Novembre',
      12 => 'Décembre',
    ];

    $route_action = 'reservation.demande.action';
    $route_retour = 'reservation.demande.multiple';

    $year = \Drupal::request()->query->get('year');
    $month = \Drupal::request()->query->get('month');
    $caution = \Drupal::request()->query->get('caution');

    $request = \Drupal::requestStack()->getCurrentRequest();
    $requestParams = $request->query->all();

    $date_now = new \DateTime();

    if ($year == NULL) {
      $year = $date_now->format('Y');
    }

    if ($month == NULL) {
      $month = $date_now->format('m');
    }

    $form['table_filter'] = [
      '#type' => 'table',
      '#tableselect' => FALSE,
    ];

    $period_year = [];
    $year_min = $reservationDemande->getYearMin();
    $year_max = $reservationDemande->getYearMax();

    foreach (range($year_min, $year_max) as $period) {
      if ($date_now->format('Y') == $period) {
        $en_cours = " - En cours - ";
      }
      else {
        $en_cours = NULL;
      }
      $period_year[$period] = $period . $en_cours;
    }

    $form['table_filter'][1]['year'] = [
      '#type' => 'select',
      '#options' => $period_year,
      '#default_value' => $year,
    ];

    $mois = [];
    $start = new \DateTime(date("Y-01-01"));
    $interval_month = new \DateInterval("P1M");
    $periods = new \DatePeriod($start, $interval_month, 11);

    foreach ($periods as $period) {
      if ($date_now->format('m') == $period->format("m")) {
        $en_cours = " - En cours - ";
      }
      else {
        $en_cours = NULL;
      }
      $mois[$period->format("m")] = $mois_fr[$period->format("n")] . $en_cours;
    }

    $form['table_filter'][1]['month'] = [
      '#type' => 'select',
      '#options' => $mois,
      '#default_value' => $month,
    ];

    if ($demandeSettings['caution'] == TRUE) {
      $selections = [
        ReservationDemande::CAUTION_NON_APPLICABLE => t('N/A'),
        ReservationDemande::CAUTION_NON_RENSEIGNEE => t('Non Renseigné'),
        ReservationDemande::CAUTION_CARTE => t('Carte Bancaire'),
        ReservationDemande::CAUTION_CHEQUE => t('Chèque'),
      ];
      $form['table_filter'][1]['caution'] = [
        '#type' => 'select',
        '#multiple' => 'true',
        '#title' => 'Ressource : ',
        '#options' => $selections,
        '#empty_value' => '',
        '#default_value' => $caution,
      ];
    }

    $form['table_filter'][1]['filtre'] = [
      '#type' => 'submit',
      '#name' => 'filtre',
      '#value' => 'Filtrer',
    ];

    $form['table_filter'][1]['reset'] = [
      '#type' => 'submit',
      '#name' => 'reset',
      '#value' => 'Reset',
    ];

    $demandeActionServices = \Drupal::service('reservation.demande.form');
    $form = $demandeActionServices->tableActionForm($form);

    // La méthode submitForm fait un redirect pour l'affichage des demandes
    // Donc aucun intérêt de faire un pré-rendu avant le redirect.
    // On affiche donc les demandes que pour les requêtes GET.
    if ($request->getMethod() !== 'POST') {

      $form['table_action'][0]['print'] = [
        $this->createButtonAction('reservation.demande.export.multiple', 'Export CSV des demandes issues du filtre', 'download', [$requestParams]),
      ];

      $output = [];
      $header = $demandeFormServices->getHeaderTableMultiple();
      $demandes = $reservationDemande->getDemandeMultipleByFilter($header, 15, $year, $month, $caution);
      foreach ($demandes as $demande) {
        $webform_submission = $demande->getWebformSubmission();
        if ($webform_submission) {
          $route_show = 'entity.webform_submission.canonical';
          $route_edit = 'entity.webform_submission.edit_form';
          $button_parameter = array_merge([
            'webform' => 'reservation_le_hamac',
            'webform_submission' => $webform_submission->id(),
            'rdmid' => $demande->Id(),
            'url' => $route_retour,
          ], $requestParams);
        }
        else {
          $route_show = 'reservation.demande.edit';
          $route_edit = 'reservation.demande.edit';
          $button_parameter = array_merge([
            'rdmid' => $demande->Id(),
            'url' => $route_retour,
          ], $requestParams);
        }
        $button_show = $this->createButtonAction($route_show, 'Voir la demande', 'eye', $button_parameter);
        $button_edition = $this->createButtonAction($route_edit, 'Editer', 'pencil', $button_parameter);

        $output[$demande->Id()]['#attributes'] = ['class' => ['row-statut-' . $demande->getStatut()]];
        $output[$demande->Id()]['email'] = $demande->getEmail();
        $output[$demande->Id()]['telephone'] = $demande->getTelephone();
        $output[$demande->Id()]['postcode'] = $demande->getPostcode();
        $output[$demande->Id()]['ressource'] = $demande->getTitleNode();
        $output[$demande->Id()]['datedemande'] = $demande->getCreatedFormat();
        $output[$demande->Id()]['demandeur'] = $demande->getDemandeur();
        $output[$demande->Id()]['datecreneau'] = $demande->getDateCreneau();
        if ($demandeSettings['caution'] == TRUE) {
          if ($demande->getCaution() == ReservationDemande::CAUTION_NON_APPLICABLE) {
            $label = $demande->getCautionLabel();
          }
          elseif ($demande->getStatut() == 'caution') {
            $label = Markup::create('<em class="fa fa-exclamation-triangle fa-2x fa-fw"></em> ' . $demande->getCautionLabel() . ' ' . $demande->getCautionDelay());
          }
          else {
            $label = $demande->getCautionLabel();
          }
          $output[$demande->Id()]['caution'] = $label;
        }
        if ($demandeSettings['inscrit'] == TRUE) {
          $output[$demande->Id()]['jauge'] = $demande->getJauge();
        }
        $output[$demande->Id()]['statut'] = $demandeFormServices->getStatutFormat($demande->getStatut());

        $webformFields = explode('|', $demandeSettings['webform_key']);
        $data = $demande->getWebformSubmissionData();
        foreach ($webformFields as $webformField) {
          $output[$demande->Id()][$webformField] = $data[$webformField] ?? '';
        }

        $output[$demande->Id()]['actions'] = [
          'data' =>
            [
              $button_show,
              $button_edition,
              $this->createButtonAction($route_action, 'Confirmer', 'check-circle',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'confirme',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Mettre en Attente', 'hourglass-half',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'attente',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Caution', 'credit-card',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'caution',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Refuser', 'times-circle',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'refuse',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Rappel', 'envelope',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'rappel',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Archiver', 'save',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'archive',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, 'Mode NoShow', 'eye-slash',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'noshow',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction($route_action, t('Mode Show'), 'thumbs-up',
                array_merge([
                  'rdmid' => $demande->Id(),
                  'action' => 'show',
                  'route' => $route_retour,
                ], $requestParams)),
              $this->createButtonAction('reservation.demande.delete', 'Supprimer la demande', 'trash-alt',
                array_merge([
                  'rdmid' => [$demande->Id()],
                  'type' => 'simple',
                  'url' => $route_retour,
                ], $requestParams)),
            ],
        ];
      }

      $form['table_demande'] = [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $output,
        '#empty' => 'Aucunes demandes liées à cette recherche',
      ];

      $form['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $form;
  }

  /**
   *
   * @param mixed $url_action
   * @param mixed $icon
   * @param mixed $parameter
   *
   * @return mixed
   */
  public function createButtonAction($url_action, $action, $icon, $parameter) {
    return [
      '#type' => 'link',
      '#title' => Markup::create(' <div class="tooltip"><em class="fa fa-' . $icon . ' fa-2x fa-fw"></em><span class="tooltiptext">' . $action . '</span></div>'),
      '#url' => Url::fromRoute($url_action, $parameter),
    ];
  }

  /**
   *
   * @param mixed $form_state
   */
  public function filteDemandeMultiple($form_state) {
    $year = $form_state->getValue(['table_filter', '1', 'year']);
    $month = $form_state->getValue(['table_filter', '1', 'month']);

    $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.multiple',
      [
        'year' => $year,
        'month' => $month,
      ]
    ));
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Appel de la méthode commune (formulaire demande simple et multiple) pour
    // agir sur les demandes.
    /** @var \Drupal\reservation\Service\ReservationDemandeFormServices $demandeFormServices */
    $demandeFormServices = \Drupal::service('reservation.demande.form');
    $demandeFormServices->submitActionForm($form_state, 'multiple');
  }

}
