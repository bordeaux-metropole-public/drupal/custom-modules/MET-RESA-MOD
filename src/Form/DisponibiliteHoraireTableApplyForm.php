<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDate;
use Drupal\reservation\Entity\ReservationHoraire;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireTableApplyForm extends FormBase {

  /**
   * @var null
   */
  public $ressourceId = NULL;

  /**
   * @var null
   */
  public $rdid = NULL;

  /**
   * @var null
   */
  public $rhid = NULL;

  /**
   * @var null
   */
  public $month = NULL;

  /**
   * @var null
   */
  public $year = NULL;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_horaire_table_apply_form';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $nid
   *   Identifiant de la ressource à réserver.
   * @param $rdid
   *   Identifiant de la journée de réservation.
   * @param $rhid
   *   Identifiant de l'horaire de réservation.
   * @param $year
   * @param $month
   *
   * @return array
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
                       $nid = NULL,
                       $rdid = NULL,
                       $rhid = NULL,
                       $year = NULL,
                       $month = NULL) {
    $this->ressourceId = $nid;
    $this->rdid = $rdid;
    $this->rhid = $rhid;
    $this->month = $month;
    $this->year = $year;

    if ($this->rhid > 0) {
      $reservationHoraire = ReservationHoraire::load($this->rhid);
      $heureDebut = $reservationHoraire->getHeureDebutFormat('H:i');
      $heureFin = $reservationHoraire->getHeureFinFormat('H:i');
      $label = 'Horaire à appliquer ' . $heureDebut . ' - ' . $heureFin;
    }
    else {
      $reservationDate = ReservationDate::load($this->rdid);
      $date = $reservationDate->getDateFormat('d/m/Y');
      $label = 'Horaires du ' . $date . ' à répliquer';
    }

    $form['affichage'] = [
      '#type' => 'item',
      '#markup' => $label,
    ];

    $mode = \Drupal::request()->query->get('mode');
    if ($mode == NULL) {
      $mode = 'date';
    }

    $options = [
      'date' => 'Par Date',
      'mois' => 'Par Mois',
    ];

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => 'Mode : ',
      '#options' => $options,
      '#default_value' => $mode,
      '#ajax' => [
        'callback' => '::choixMode',
        'wrapper' => 'mode',
      ],
    ];

    $dates = [];
    $months = [];

    $mois_fr = [
      1 => 'Janvier',
      2 => 'Février',
      3 => 'Mars',
      4 => 'Avril',
      5 => 'Mai',
      6 => 'Juin',
      7 => 'Juillet',
      8 => 'Août',
      9 => 'Septembre',
      10 => 'Octobre',
      11 => 'Novembre',
      12 => 'Décembre',
    ];

    /** @var  \Drupal\Core\DateTime\DateFormatter $dateFormatter */
    $dateFormatter = \Drupal::service('date.formatter');
    /** @var \Drupal\reservation\Service\ReservationDateServices $reservationDateService */
    $reservationDateService = \Drupal::service('reservation.date');
    $datesPubliees = $reservationDateService->getPublishedDates(
      $this->ressourceId, NULL, FALSE, TRUE);
    $previousMonth = NULL;
    foreach ($datesPubliees as $datePubliee) {
      $datePublieeDate = $datePubliee->getDateTime();
      $dates[$datePubliee->id()] = $dateFormatter->format(
        $datePublieeDate->getTimestamp(), 'custom', 'l j F Y');
      $currentMonth = $datePubliee->getDateFormat('m');
      if ($currentMonth != $previousMonth) {
        $months[$datePubliee->getDateFormat('Y-m')] =
          $datePubliee->getDateFormat('Y - ') . $mois_fr[$datePubliee->getDateFormat('n')];
        $previousMonth = $currentMonth;
      }
    }

    if ($mode == 'mois') {
      $form['month'] = [
        '#type' => 'checkboxes',
        '#title' => 'Mois',
        '#options' => $months,
      ];
    }
    else {
      $form['date'] = [
        '#type' => 'checkboxes',
        '#title' => 'Dates',
        '#options' => $dates,
      ];
    }

    $form['Valider'] = [
      '#type' => 'submit',
      '#name' => 'valider',
      '#value' => 'Valider',
    ];

    $form['retour'] = [
      '#type' => 'submit',
      '#name' => 'retour',
      '#value' => 'Retour',
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function choixMode(array $form, FormStateInterface $form_state) {
    $mode = $form_state->getValue('mode');

    $response = new AjaxResponse();
    $url = Url::fromRoute('reservation.disponibilite.horaire.appliquer',
      [
        'mode' => $mode,
        'nid' => $this->ressourceId,
        'rdid' => $this->rdid,
        'rhid' => $this->rhid,
        'year' => $this->year,
        'month' => $this->month,
      ],
      ["absolute" => TRUE])->toString();

    $response->addCommand(new RedirectCommand($url));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#name'] == 'valider') {
      $mode = $form_state->getValue('mode');
      if ($mode == 'date') {
        $this->setHoraireDate($form_state);
      }
      else {
        $this->setHoraireMonth($form_state);
      }
    }

    $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.horaire',
      [
        'nid' => $this->ressourceId,
        'month' => $this->month,
        'year' => $this->year,
      ]));
  }

  /**
   *
   */
  public function setHoraireDate($form_state) {
    $values = $form_state->getValues();
    foreach ($values['date'] as $selectedRdid => $selectedDate) {
      if ($selectedDate) {
        $reservationsDate = ReservationDate::load($selectedRdid);
        $this->setRessourceHoraire($reservationsDate);
      }
    }
  }

  /**
   *
   */
  public function setRessourceHoraire(ReservationDate $reservationDate) {
    /** @var \Drupal\reservation\Service\ReservationHoraireServices $horaireServices */
    $horaireServices = \Drupal::service('reservation.horaire');

    $reservationDate->setHoraire(TRUE);
    $reservationDate->save();

    if ($this->rhid > 0) {
      $reservationHoraire = ReservationHoraire::load($this->rhid);
      $this->createRessourceHoraire($reservationDate, $reservationHoraire);
    }
    else {
      $horaires = $horaireServices->getByRdid($this->rdid, FALSE);
      foreach ($horaires as $horaire) {
        $this->createRessourceHoraire($reservationDate, $horaire);
      }
    }
  }

  /**
   * @param \Drupal\reservation\Entity\ReservationDate $reservationDate
   * @param \Drupal\reservation\Entity\ReservationHoraire $horaire
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createRessourceHoraire(
    ReservationDate $reservationDate,
    ReservationHoraire $sourceReservationHoraire) {
    /** @var \Drupal\reservation\Service\ReservationHoraireServices $horaireServices */
    $horaireServices = \Drupal::service('reservation.horaire');

    $resaDate = $reservationDate->getDateTime();

    $sourceHeureDebut = $sourceReservationHoraire->getHeureDebut();
    $newHeureDebut = new \DateTime($resaDate->format('Y-m-d') . 'T' . $sourceHeureDebut->format('H:i:s'));
    $sourceHeureFin = $sourceReservationHoraire->getHeureFin();
    $newHeureFin = new \DateTime($resaDate->format('Y-m-d') . 'T' . $sourceHeureFin->format('H:i:s'));

    $notExist = $horaireServices->notExisteHoraire($reservationDate->id(), $newHeureDebut, $newHeureFin);

    if ($notExist) {
      $newReservationHoraire = ReservationHoraire::create([
        'rdid' => $reservationDate->id(),
        'statut' => $sourceReservationHoraire->getStatut(),
        'jauge_statut' => $sourceReservationHoraire->getJaugeStatut(),
        'jauge_nombre' => $sourceReservationHoraire->getJaugeNombre(),
      ]
      );
      $newReservationHoraire->setHeureDebut($newHeureDebut);
      $newReservationHoraire->setHeureFin($newHeureFin);
      $newReservationHoraire->save();
    }
  }

  /**
   *
   */
  public function setHoraireMonth($form_state) {
    /** @var \Drupal\reservation\Service\ReservationDateServices $dateServices */
    $dateServices = \Drupal::service('reservation.date');
    $values = $form_state->getValues();
    foreach ($values['month'] as $month) {
      if ($month) {
        $reservationsDates = $dateServices->getPublishedDates(
          $this->ressourceId, $month, FALSE, TRUE);
        foreach ($reservationsDates as $reservationsDate) {
          $this->setRessourceHoraire($reservationsDate);
        }
      }
    }
  }

}
