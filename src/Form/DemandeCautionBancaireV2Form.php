<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\ReservationUtils;

/**
 * Class DemandeCautionBancaireForm.
 *
 * @package Drupal\reservation\Form
 */
class DemandeCautionBancaireV2Form extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ReservationDemande $demande = NULL) {
    $reservationSettings = \Drupal::config('reservation.settings');
    $caution_settings = $reservationSettings->get('caution');

    $route = 'reservation.demande.caution.traitement_carte_v2';
    $dmid = $demande->id();
    $token = $demande->getWebformToken();

    $fields = [
      'ORDERID' => $dmid,
      'AMOUNT' => $demande->getDate()
        ->getReservationRessourceNode()
        ->getCautionMontant() * 100,
      'CN' => $demande->getDemandeur(),
      'EMAIL' => $demande->getEmail(),
      'ACCEPTURL' => $this->urlReturn($route, ReservationDemande::CAUTION_ACCEPT, $dmid, $token, $caution_settings),
      'DECLINEURL' => $this->urlReturn($route, ReservationDemande::CAUTION_DECLINE, $dmid, $token, $caution_settings),
      'EXCEPTIONURL' => $this->urlReturn($route, ReservationDemande::CAUTION_EXCEPT, $dmid, $token, $caution_settings),
      'CANCELURL' => $this->urlReturn($route, ReservationDemande::CAUTION_CANCEL, $dmid, $token, $caution_settings),
    ];
    $form = $this->parametreForm($form, $fields);

    $form = $this->parametreForm($form, $caution_settings['input']);

    $form['#attached']['library'][] = 'reservation/reservation.caution_cb_autosubmit';
    $form['#id'] = $this->getFormId();

    if (ReservationUtils::isAdmin()) {

      $form['cb_start_button'] = [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#id' => 'caution_cb_start',
        '#value' => 'CB Start',
        '#attributes' => ['type' => 'button'],
      ];

      $form['cb_ok'] = [
        '#type' => 'link',
        '#title' => 'CB OK',
        '#url' => Url::fromRoute($route, [
          'etat' => 'accept',
          'dmid' => $dmid,
          'token' => $token,
        ]),
        '#attributes' => [
          'class' => [
            'button',
            // 'button-action',
          ],
        ],
      ];

      $form['cb_ko'] = [
        '#type' => 'link',
        '#title' => 'CB KO',
        '#url' => Url::fromRoute($route, [
          'etat' => 'decline',
          'dmid' => $dmid,
          'token' => $token,
        ]),
        '#attributes' => [
          'class' => [
            'button',
            // 'button-action',
          ],
        ],
      ];
    }

    $form['#action'] = $caution_settings['url']['send'];

    return $form;
  }

  /**
   * @param $route
   * @param $etat
   * @param $token
   * @param $caution_settings
   *
   * @return string
   */
  public function urlReturn($route, $etat, $dmid, $token, $caution_settings) {
    $scheme_host = $caution_settings['url']['return_host'];
    if (empty($scheme_host)) {
      $scheme = \Drupal::request()->getScheme();
      $host = \Drupal::request()->getHost();
      $scheme_host = $scheme . '://' . $host;
    }
    return $scheme_host . Url::fromRoute($route, [
      'etat' => $etat,
      'dmid' => $dmid,
      'token' => $token,
    ])->toString();
  }

  /**
   * @param $form
   * @param $rows
   *
   * @return mixed
   */
  public function parametreForm($form, $rows) {
    foreach ($rows as $key => $row) {
      $form = $this->hiddenForm($form, $key, $row);
    }

    return $form;
  }

  /**
   * @param $form
   * @param $key
   * @param $row
   *
   * @return mixed
   */
  public function hiddenForm($form, $key, $row) {
    $form[$key] = [
      '#type' => 'hidden',
      '#value' => $row,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'demande_caution_bancaire_ng_form';
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
