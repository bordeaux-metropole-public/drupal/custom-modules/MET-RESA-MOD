<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDate;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireForm extends FormBase {

  /**
   * @var mixed
   */
  protected $rdid;

  /**
   * @var mixed
   */
  protected $nid;

  /**
   * @var mixed
   */
  protected $month;

  /**
   * @var mixed
   */
  protected $year;

  /**
   * DisponibiliteHoraireForm constructor.
   *
   * @param $rdid
   * @param $nid
   * @param $month
   * @param $year
   */
  public function __construct($rdid, $nid, $month, $year) {
    $this->rdid = $rdid;
    $this->nid = $nid;
    $this->month = $month;
    $this->year = $year;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_horaire_form_' . $this->rdid;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ReservationDate $reservationDate = NULL) {
    $horaire = $reservationDate->getHoraire();

    $today = (new \DateTime())->setTime(0, 0, 0, 0);
    $disabled = !$reservationDate || $today > $reservationDate->getDateTime();

    $options = [
      '0' => 'Toute la journée',
      '1' => 'Créneaux horaires',
    ];

    $form['table_date'] = [
      '#type' => 'table',
      '#tableselect' => FALSE,
    ];

    $form['table_date'][$this->rdid]['horaire'] = [
      '#type' => 'radios',
      '#title' => 'Mode de réservation',
      '#options' => $options,
      '#default_value' => $horaire,
      '#disabled' => $disabled,
      '#ajax' => [
        'callback' => [$this, 'setHoraire'],
        'event' => 'change',
        'wrapper' => 'edit-output-' . $this->rdid,
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['table_date'][$this->rdid]['jauge_statut'] = [
      '#type' => 'checkbox',
      '#title' => 'Jauge',
      '#size' => 2,
      '#default_value' => $reservationDate->getJaugeStatut(),
      '#disabled' => $disabled,
      '#ajax' => [
        'event' => 'change',
        'callback' => '::setJaugeStatut',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['table_date'][$this->rdid]['jauge_nombre'] = [
      '#type' => 'number',
      '#size' => 2,
      '#value' => $reservationDate->getJaugeNombre(),
      '#disabled' => $disabled || !$reservationDate->getJaugeStatut(),
      '#ajax' => [
        'event' => 'change',
        'callback' => '::setJaugeNombre',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    if ($horaire !== '0') {
      $form['table_date'][$this->rdid]['jauge_statut']['#wrapper_attributes'] = ['class' => ['resa-hide']];
      $form['table_date'][$this->rdid]['jauge_nombre']['#wrapper_attributes'] = ['class' => ['resa-hide']];
    }

    $form['table_date'][$this->rdid]['rappel'] = [
      '#type' => 'checkbox',
      '#title' => 'Rappel',
      '#default_value' => $reservationDate->getRappel(),
      '#disabled' => $disabled,
      '#ajax' => [
        'event' => 'change',
        'callback' => '::setRappel',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['table_date'][$this->rdid]['rappel_jour'] = [
      '#type' => 'number',
      '#title' => 'J-',
      '#size' => 2,
      '#label_attributes' => ['style' => 'display:inline'],
      '#value' => $reservationDate->getRappelJour(),
      '#disabled' => $disabled || !$reservationDate->getRappel(),
      '#ajax' => [
        'event' => 'change',
        'callback' => '::setRappelJour',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['table_date'][$this->rdid]['enquete'] = [
      '#type' => 'checkbox',
      '#title' => 'Enquête',
      '#default_value' => $reservationDate->getEnquete(),
      '#disabled' => $disabled,
      '#ajax' => [
        'event' => 'change',
        'callback' => '::setEnquete',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['table_date'][$this->rdid]['enquete_jour'] = [
      '#type' => 'number',
      '#title' => 'J+',
      '#size' => 2,
      '#label_attributes' => ['style' => 'display:inline'],
      '#value' => $reservationDate->getEnqueteJour(),
      '#disabled' => $disabled || !$reservationDate->getEnquete(),
      '#ajax' => [
        'event' => 'change',
        'callback' => '::setEnqueteJour',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['table_date'][$this->rdid]['publie'] = [
      '#type' => 'checkbox',
      '#title' => 'Publier',
      '#default_value' => $reservationDate->getPublie(),
      '#disabled' => $disabled,
      '#ajax' => [
        'event' => 'change',
        'callback' => '::setPublie',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['table_date'][$this->rdid]['appliquer'] = [
      '#type' => 'link',
      '#title' => Markup::create('<em class="fa fa-share fa-2x fa-fw"></em>'),
      '#url' => Url::fromRoute('reservation.disponibilite.horaire.appliquer',
        [
          'rdid' => $this->rdid,
          'rhid' => 0,
          'nid' => $this->nid,
          'year' => $this->year,
          'month' => $this->month,
        ]),
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setHoraire(array $form, FormStateInterface $form_state) {
    $horaire = $form_state->getValue(['table_date', $this->rdid, 'horaire']);

    $reservationDate = ReservationDate::load($this->rdid);
    $reservationDate->setHoraire($horaire);
    $reservationDate->save();

    $response = new AjaxResponse();

    if ($horaire) {

      $horaireRender['#theme'] = 'template_disponibilite_horaire_item';
      $horaireRender['#rdid'] = $this->rdid;

      self::renderHoraireItem($horaireRender, $this->rdid, $this->nid, $this->month, $this->year, '#');

      $response->addCommand(new InvokeCommand('#disponibilite-horaire-' . $this->rdid, 'empty'));
      $response->addCommand(new AppendCommand('#disponibilite-horaire-' . $this->rdid, $horaireRender));
      $response->addCommand(new InvokeCommand('.form-item-table-date-' . $this->rdid . '-jauge-statut', 'hide'));
      $response->addCommand(new InvokeCommand('.form-item-table-date-' . $this->rdid . '-jauge-nombre', 'hide'));

    }
    else {

      $response->addCommand(new InvokeCommand('#disponibilite-horaire-' . $this->rdid, 'empty'));
      $response->addCommand(new InvokeCommand('.form-item-table-date-' . $this->rdid . '-jauge-statut', 'show'));
      $response->addCommand(new InvokeCommand('.form-item-table-date-' . $this->rdid . '-jauge-nombre', 'show'));

    }

    return $response;
  }

  /**
   * @param $horaireRender
   * @param $rdid
   * @param $nid
   * @param $month
   * @param $year
   */
  public static function renderHoraireItem(&$horaireRender, $rdid, $nid, $month, $year, $prefix = '') {
    /** @var \Drupal\reservation\Service\ReservationHoraireServices $horaireServices */
    $horaireServices = \Drupal::service('reservation.horaire');
    $reservationHoraires = $horaireServices->getByDate($rdid);

    $reservationDate = ReservationDate::load($rdid);
    $today = (new \DateTime())->setTime(0, 0, 0, 0);
    $disabled = !$reservationDate || $today > $reservationDate->getDateTime();

    if (!$disabled) {
      $formAddDispoHoraire = new DisponibiliteHoraireTableAddForm($rdid, $nid, $month, $year);

      $horaireRender[$prefix . 'form_horaire_add'] = \Drupal::formBuilder()
        ->getForm($formAddDispoHoraire);
    }

    $header = [
      '*',
      'actif',
      'Heure début',
      'Heure fin',
      'Jauge',
      '',
      'Edition',
      'Partage',
      'Supprimer',
    ];

    $horaireRender[$prefix . 'form_horaire_line_header'] = [
      '#type' => 'reservation_div_table',
      '#tableselect' => FALSE,
      '#header' => $header,
      '#table' => FALSE,
      '#thead' => FALSE,
    ];

    $creneau = 1;
    foreach ($reservationHoraires as $reservationHoraire) {

      $rhid = $reservationHoraire->id();
      $formEditDispoHoraire = new DisponibiliteHoraireTableEditForm($rdid, $rhid, $nid, $month, $year);

      $horaireRender[$prefix . 'form_horaire_line'][$rhid] =
        \Drupal::formBuilder()
          ->getForm($formEditDispoHoraire, $reservationHoraire, $creneau);

      $creneau++;
    }

    if ($creneau === 1) {
      $horaireRender[$prefix . 'form_horaire_line_header']['#attributes']['class'] = ['resa-hide'];
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setJaugeStatut(array $form, FormStateInterface $form_state) {
    return $this->setStatutNombre($form_state, 'jauge_statut', 'jauge-nombre');
  }

  /**
   * @param $form_state
   * @param $field_statut
   * @param $field_nombre
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setStatutNombre($form_state, $field_statut, $field_nombre) {
    $value = $form_state->getValue(['table_date', $this->rdid, $field_statut]);

    $reservationDate = ReservationDate::load($this->rdid);
    $reservationDate->set($field_statut, $value);
    $reservationDate->save();

    $response = new AjaxResponse();
    $id_field = '#edit-table-date-' . $this->rdid . '-' . $field_nombre;

    if ($value) {
      $response->addCommand(new InvokeCommand($id_field, 'css', [
        'background-color',
        '#ffffff',
      ]));
      $response->addCommand(new InvokeCommand($id_field, 'attr', [
        'disabled',
        FALSE,
      ]));
    }
    else {
      $response->addCommand(new InvokeCommand($id_field, 'css', [
        'background-color',
        '#e5e5e5',
      ]));
      $response->addCommand(new InvokeCommand($id_field, 'attr', [
        'disabled',
        TRUE,
      ]));
    }

    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setJaugeNombre(array $form, FormStateInterface $form_state) {
    return $this->setValue($form_state, 'jauge_nombre');
  }

  /**
   * @param $form_state
   * @param $field
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setValue($form_state, $field) {
    $value = $form_state->getValue(['table_date', $this->rdid, $field]);

    $reservationDate = ReservationDate::load($this->rdid);
    $reservationDate->set($field, $value);
    $reservationDate->save();

    $response = new AjaxResponse();

    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setRappel(array $form, FormStateInterface $form_state) {
    return $this->setStatutNombre($form_state, 'rappel', 'rappel-jour');
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setRappelJour(array $form, FormStateInterface $form_state) {
    return $this->setValue($form_state, 'rappel_jour');
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setEnquete(array $form, FormStateInterface $form_state) {
    return $this->setStatutNombre($form_state, 'enquete', 'enquete-jour');
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setEnqueteJour(array $form, FormStateInterface $form_state) {
    return $this->setValue($form_state, 'enquete_jour');
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setPublie(array $form, FormStateInterface $form_state) {
    return $this->setValue($form_state, 'publie');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.date',
      ['nid' => $this->nid, 'year' => $this->year]));
  }

}
