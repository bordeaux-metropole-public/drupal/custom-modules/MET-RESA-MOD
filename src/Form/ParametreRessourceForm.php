<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationRessource;
use Drupal\reservation\Event\EntityEvent;

/**
 * Class ParametreRessourceForm.
 *
 * @ingroup reservation
 */
class ParametreRessourceForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'parametre_ressource_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\reservation\Service\ReservationRessourceServices $reservationRessource */
    $reservationRessource = \Drupal::service('reservation.ressource');
    $types = $reservationRessource->getNodeTypes();

    $form['table_ressource'] = [
      '#type' => 'table',
      '#header' => ['Ressource', 'RGPD', 'Caution', 'Montant'],
      '#tableselect' => FALSE,
    ];

    foreach ($types as $key => $type) {
      /** @var \Drupal\reservation\Entity\ReservationRessource $ressource */
      $ressource = $reservationRessource->getRessourceByType($key);

      if (isset($ressource)) {
        $rrid = $ressource->Id();
        $statut = $ressource->getStatut();
        $rgpd = $ressource->getRgpd();
        $caution_statut = $ressource->getCautionStatut();
        $caution_montant = $ressource->getCautionMontant();
      }
      else {
        $rgpd = 1;
        $statut = FALSE;
        $rrid = FALSE;
        $caution_statut = FALSE;
        $caution_montant = 0;
      }

      $form['table_ressource'][$key]['statut'] = [
        '#type' => 'checkbox',
        '#title' => $type,
        '#default_value' => $statut,
      ];

      $form['table_ressource'][$key]['rgpd'] = [
        '#type' => 'number',
        '#title' => 'RGPD',
        '#title_display' => 'invisible',
        '#default_value' => $rgpd,
      ];

      $form['table_ressource'][$key]['caution_statut'] = [
        '#type' => 'checkbox',
        '#default_value' => $caution_statut,
      ];

      $form['table_ressource'][$key]['caution_montant'] = [
        '#type' => 'number',
        '#default_value' => $caution_montant,
      ];

      $form['table_ressource'][$key]['rrid'] = [
        '#type' => 'hidden',
        '#value' => $rrid,
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $rows = $form_state->getValues();
    foreach ($rows['table_ressource'] as $key => $ressource) {
      if ($ressource['caution_statut'] && $ressource['caution_montant'] <= 0) {
        $form_state->setErrorByName('caution_montant', $this->t('Le montant de la caution pour <b>%key</b> doit être supérieur à zéro.', ['%key' => $key]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $rows = $form_state->getValues();
    foreach ($rows['table_ressource'] as $key => $ressource) {
      $statut = $ressource["statut"] ? 1 : 0;
      $caution_statut = $ressource["caution_statut"] ? 1 : 0;

      if ($ressource["rrid"]) {
        $entite = ReservationRessource::load($ressource["rrid"]);
        $entite->setRgpd($ressource["rgpd"]);
        $entite->setStatut($statut);
        $entite->setCautionStatut($caution_statut);
        $entite->setCautionMontant($ressource["caution_montant"]);
        $entite->save();
      }
      else {
        $entite = ReservationRessource::create([
          'type' => $key,
          'statut' => $statut,
          'rgpd' => $ressource["rgpd"],
          'caution_statut' => $caution_statut,
          'caution_montant' => $ressource["caution_montant"],
        ]);
        $entite->save();
      }

      if ($statut) {
        \Drupal::service('event_dispatcher')
          ->dispatch(new EntityEvent($entite), EntityEvent::RESERVATION_RESSOURCE_SAVE);
      }
    }
  }

}
