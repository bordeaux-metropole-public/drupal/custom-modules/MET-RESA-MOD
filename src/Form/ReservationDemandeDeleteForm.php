<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting ReservationDemandee entities.
 *
 * @ingroup reservation
 */
class ReservationDemandeDeleteForm extends ContentEntityDeleteForm {


}
