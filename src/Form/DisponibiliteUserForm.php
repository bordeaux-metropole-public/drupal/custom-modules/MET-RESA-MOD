<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteUserForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_user_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $choix = NULL) {

    $options = [
      'user' => 'Mes ressources',
      'all' => 'Toutes les ressources',
    ];

    $form['choix'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $choix,
      '#ajax' => [
        'callback' => '::redirectPage',
        'wrapper' => 'mode',
      ],
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function redirectPage(array $form, FormStateInterface $form_state) {

    $choix = $form_state->getValue('choix');

    $response = new AjaxResponse();
    $url = Url::fromRoute('reservation.disponibilite.index',
      ['choix' => $choix],
      ["absolute" => TRUE])->toString();
    $response->addCommand(new RedirectCommand($url));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
