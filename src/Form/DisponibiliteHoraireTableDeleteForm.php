<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationHoraire;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireTableDeleteForm extends FormBase {

  /**
   * @var null
   */
  public $ressourceId = NULL;

  /**
   * @var null
   */
  public $rhid = NULL;

  /**
   * @var null
   */
  public $month = NULL;

  /**
   * @var null
   */
  public $year = NULL;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_horaire_table_apply_form';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $nid
   *   Identifiant de la ressource à réserver.
   * @param $rhid
   *   Identifiant de l'horaire de disponibilité.
   * @param $year
   * @param $month
   *
   * @return array
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
                       $nid = NULL,
                       $rhid = NULL,
                       $year = NULL,
                       $month = NULL) {
    $this->ressourceId = $nid;
    $this->rhid = $rhid;
    $this->month = $month;
    $this->year = $year;

    $reservationHoraire = ReservationHoraire::load($this->rhid);

    $label = 'Supprimer l\'horaire ' . $reservationHoraire->getHeureDebutFormat('H:i');
    $label .= ' - ' . $reservationHoraire->getHeureFinFormat('H:i');
    $label .= ' du ' . $reservationHoraire->getDate()
      ->getDateFormat('d/m/Y') . ' ?';

    $form['affichage'] = [
      '#type' => 'item',
      '#markup' => $label,
    ];

    $form['supprimer'] = [
      '#type' => 'submit',
      '#name' => 'supprimer',
      '#value' => 'Oui',
    ];

    $form['annuler'] = [
      '#type' => 'submit',
      '#name' => 'annuler',
      '#value' => 'Non',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();

    if ($trigger['#name'] == 'supprimer') {
      $reservationHoraire = ReservationHoraire::load($this->rhid);
      $reservationHoraire->delete();
    }

    $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.horaire',
      [
        'nid' => $this->ressourceId,
        'month' => $this->month,
        'year' => $this->year,
      ]));
  }

}
