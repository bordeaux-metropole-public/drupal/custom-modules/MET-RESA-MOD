<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\ReservationUtils;

/**
 * Class DemandeCautionBancaireForm.
 *
 * @package Drupal\reservation\Form
 */
class DemandeCautionTraitementCarteBancaireForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ReservationDemande $demande = NULL,
                       $etat = NULL) {
    $form['#attached']['library'][] = 'reservation/reservation.caution_traitement_cb';
    $form['#id'] = $this->getFormId();

    if (ReservationUtils::isAdmin()) {
      $form['message']['#type'] = 'item';
      $form['message']['#markup'] = '<p>Etat de la transaction : ' . $etat . '</p>';
    }

    $form['etat']['#type'] = 'hidden';
    $form['etat']['#value'] = $etat;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'demande_caution_traitement_carte_bancaire_form';
  }

  /**
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
