<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationRessourceNode;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteModeForm extends FormBase {


  /**
   * @var mixed
   */
  protected $nid;

  /**
   * DisponibiliteModeForm constructor.
   *
   * @param $nid
   */
  public function __construct($nid) {
    $this->nid = $nid;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_mode_form_' . $this->nid;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ReservationRessourceNode $ressourceNode = NULL) {

    $options = [
      0 => 'Pré-Réservé',
      1 => 'Automatique',
    ];

    $form['mode'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $ressourceNode ? $ressourceNode->getAutomatique() : '',
      '#ajax' => [
        'callback' => '::setAutomatique',
        'wrapper' => 'mode',
      ],
    ];

    $form['node'] = [
      '#type' => 'hidden',
      '#value' => $ressourceNode ? $ressourceNode->id() : '',
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setAutomatique(array $form, FormStateInterface $form_state) {
    $node = $form_state->getValue('node');
    $mode = $form_state->getValue('mode');

    $ressourcenode = ReservationRessourceNode::load($node);
    $ressourcenode->setAutomatique($mode);
    $ressourcenode->save();

    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
