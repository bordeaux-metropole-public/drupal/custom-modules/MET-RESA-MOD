<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\reservation\ReservationConstants;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteDelayForm extends FormBase {

  /**
   * @var mixed
   */
  protected $nid;

  /**
   * DisponibiliteCautionForm constructor.
   *
   * @param $nid
   */
  public function __construct($nid) {
    $this->nid = $nid;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_delay_form_' . $this->nid;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ReservationRessourceNode $ressourceNode = NULL) {
    try {
      $minimum_delay = $ressourceNode ? $ressourceNode->getMinimumDelay() : '0';
      $minimum_delay_type = $ressourceNode ? $ressourceNode->getMinimumDelayType() : 'D';
    }
    catch (\Exception $e) {
      \Drupal::logger(ReservationConstants::LOG_CHANNEL)
        ->warning($e->getMessage());
      $minimum_delay = '0';
      $minimum_delay_type = 'D';
    }
    $form['minimum_delay'] = [
      '#type' => 'number',
      '#id' => 'edit-minimum-delay--' . $this->nid,
      '#attributes' => [
        'type' => 'number',
        'min' => '0',
        'max' => '999',
        'maxlength' => '3',
      ],
      '#default_value' => $minimum_delay,
      '#ajax' => [
        'callback' => '::setMinimumDelay',
        'wrapper' => 'mode',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          // Graphic shown to indicate ajax. Options: 'throbber' (def.), 'bar'.
          'message' => NULL,
          // Message to show along progress graphic. Default: 'Please wait...'.
        ],
      ],
    ];
    $form['minimum_delay_type'] = [
      '#type' => 'select',
      '#id' => 'edit-minimum-delay-type--' . $this->nid,
      '#options' => [
        'D' => $this->t('Jour(s)'),
        'H' => $this->t('Heure(s)'),
      ],
      '#default_value' => $minimum_delay_type,
      '#ajax' => [
        'callback' => '::setMinimumDelay',
        'wrapper' => 'mode',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          // Graphic shown to indicate ajax. Options: 'throbber' (def.), 'bar'.
          'message' => NULL,
          // Message to show along progress graphic. Default: 'Please wait...'.
        ],
      ],
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function setMinimumDelay(array $form, FormStateInterface $form_state) {
    $minimum_delay = $form_state->getValue('minimum_delay');
    $minimum_delay_type = $form_state->getValue('minimum_delay_type');

    $ressourcenode = ReservationRessourceNode::load($this->nid);
    $ressourcenode->setMinimumDelay($minimum_delay);
    $ressourcenode->setMinimumDelayType($minimum_delay_type);
    $ressourcenode->save();

    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
