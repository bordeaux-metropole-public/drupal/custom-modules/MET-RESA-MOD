<?php

namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationHoraire;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireTableEditForm extends FormBase {

  /**
   * @var mixed
   */
  protected $rdid;

  /**
   * @var mixed
   */
  protected $rhid;

  /**
   * @var mixed
   */
  protected $nid;

  /**
   * @var mixed
   */
  protected $month;

  /**
   * @var mixed
   */
  protected $year;

  /**
   * DisponibiliteHoraireTableEditForm constructor.
   *
   * @param $rdid
   * @param $rhid
   * @param $nid
   * @param $month
   * @param $year
   */
  public function __construct($rdid, $rhid, $nid, $month, $year) {
    $this->rdid = $rdid;
    $this->rhid = $rhid;
    $this->nid = $nid;
    $this->month = $month;
    $this->year = $year;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_horaire_table_Line_form_' . $this->rhid;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ReservationHoraire $reservationHoraire = NULL,
                       $creneau = 1) {
    $today = (new \DateTime())->setTime(0, 0, 0, 0);
    $disabled = !$reservationHoraire || $today > $reservationHoraire->getDate()
      ->getDateTime();
    // $disabled = !$reservationHoraire
    // || (new \DateTime()) > $reservationHoraire->getHeureFin();
    $form['#attributes']['data-resa-type'] = 'tr';

    $form['table_horaire'] = [
      '#type' => 'reservation_div_table',
      '#tableselect' => FALSE,
      '#table' => FALSE,
      '#tbody' => FALSE,
      '#tr' => FALSE,
    ];

    $heure_debut = $reservationHoraire->getHeureDebut();
    $heure_fin = $reservationHoraire->getHeureFin();

    $formRow = [];

    $formRow['creneau'] = [
      '#type' => 'item',
      '#markup' => $creneau,
    ];

    $formRow['statut'] = [
      '#type' => 'checkbox',
      '#default_value' => $reservationHoraire->getStatut(),
      '#disabled' => $disabled,
      '#size' => 2,
    ];

    $formRow['heure_debut'] = [
      '#type' => 'time',
      '#default_value' => $heure_debut->format('H:i'),
      '#disabled' => $disabled,
    ];

    $formRow['heure_fin'] = [
      '#type' => 'time',
      '#default_value' => $heure_fin->format('H:i'),
      '#disabled' => $disabled,
    ];

    $formRow['jauge_statut'] = [
      '#type' => 'checkbox',
      '#default_value' => $reservationHoraire->getJaugeStatut(),
      '#disabled' => $disabled,
      '#size' => 2,
    ];

    $formRow['jauge_nombre'] = [
      '#type' => 'number',
      '#value' => $reservationHoraire->getJaugeNombre(),
      '#size' => 2,
    ];
    if ($disabled) {
      $formRow['jauge_nombre']['#disabled'] = TRUE;
    }
    else {
      $formRow['jauge_nombre']['#states'] = [
        'enabled' => [
          ':input[name="table_horaire[' . $this->rhid . '][jauge_statut]"]' => [
            'checked' => TRUE,
          ],
        ],
      ];
    }

    $formRow['edit'] = [
      '#type' => 'button',
      '#name' => 'edit',
      '#value' => $this->t('Save'),
      '#disabled' => $disabled,
      '#ajax' => [
        'event' => 'click',
        'callback' => '::editLineAjax',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
      '#suffix' => '<span class="valid-message_' . $this->rhid . '"></span>',
    ];

    $formRow['appliquer'] = [
      '#type' => 'link',
      '#title' => Markup::create('<em class="fa fa-share fa-2x fa-fw"></em>'),
      '#url' => Url::fromRoute('reservation.disponibilite.horaire.appliquer',
        [
          'rdid' => $this->rdid,
          'rhid' => $this->rhid,
          'nid' => $this->nid,
          'year' => $this->year,
          'month' => $this->month,
        ]),
    ];

    $delete_msg = $this->t('Etes-vous sûr de vouloir supprimer cet horaire ?');
    $formRow['supprimer'] = [
      '#type' => 'button',
      '#value' => $this->t('Delete'),
      '#attributes' => ['onclick' => 'if (confirm("' . $delete_msg . '")) { jQuery(this).trigger("confirmed"); } else { return false; }'],
      '#disabled' => $disabled,
      '#ajax' => [
        'event' => 'confirmed',
        'callback' => '::deleteLineAjax',
        'progress' => [
          'type' => 'throbber',
          // Graphic shown to indicate ajax. Options: 'throbber' (def.), 'bar'.
          'message' => NULL,
          // Message to show along progress graphic. Default: 'Please wait...'.
        ],
      ],
    ];

    $form['table_horaire'][$this->rhid] = $formRow;

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function editLineAjax(array &$form, FormStateInterface $form_state) {
    $validate = TRUE;
    $message = NULL;
    $response = new AjaxResponse();
    $css_valid = ['border' => '1px solid'];
    $css_error = ['border' => '2px solid red'];

    $heure_debut = $form_state->getValue([
      'table_horaire',
      $this->rhid,
      'heure_debut',
    ]);
    $heure_fin = $form_state->getValue([
      'table_horaire',
      $this->rhid,
      'heure_fin',
    ]);

    if ($heure_debut >= $heure_fin) {
      $message .= $this->t("<p>L'heure de fin doit être supérieure à l'heure de début.</p>");
      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-heure-debut', $css_error));
      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-heure-fin', $css_error));
      $validate = FALSE;
    }

    $jauge_statut = $form_state->getValue([
      'table_horaire',
      $this->rhid,
      'jauge_statut',
    ]);
    if ($jauge_statut == 1) {
      $jauge_nombre = $form_state->getValue([
        'table_horaire',
        $this->rhid,
        'jauge_nombre',
      ]);
      if ($jauge_nombre <= 0) {
        $message .= $this->t('<p>La jauge doit être supérieure à zéro.</p>');
        $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-jauge-nombre', $css_error));
        $validate = FALSE;
      }
    }

    if ($validate) {

      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-heure-debut', $css_valid));
      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-heure-fin', $css_valid));
      $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-jauge-nombre', $css_valid));

      try {
        $reservationHoraire = ReservationHoraire::load($this->rhid);
        if (!empty($reservationHoraire)) {
          $this->setValue($form_state, 'statut', $reservationHoraire);
          $this->setHeure($form_state, 'heure_debut', $reservationHoraire);
          $this->setHeure($form_state, 'heure_fin', $reservationHoraire);
          $this->setValue($form_state, 'jauge_statut', $reservationHoraire);
          $this->setValue($form_state, 'jauge_nombre', $reservationHoraire);
          $reservationHoraire->save();
        }
        else {
          $message = '<p>Horaire introuvable, veuillez rafraîchir la page.</p>';
        }
      }
      catch (\Exception $error) {
        $message = '<p>Erreur : ' . $error->getMessage() . '</p>';
      }
    }

    $response->addCommand(new HtmlCommand('.valid-message_' . $this->rhid, $message));

    return $response;
  }

  /**
   * @param $form_state
   * @param $field
   */
  public function setValue($form_state, $field, $reservationHoraire) {
    $value = $form_state->getValue(['table_horaire', $this->rhid, $field]);
    $reservationHoraire->set($field, $value);
  }

  /**
   * @param $form_state
   * @param $fieldName
   *
   * @throws \Exception
   */
  public function setHeure($form_state, $fieldName, $reservationHoraire) {
    $date = $reservationHoraire->getHeure($fieldName);
    $heure = new \DateTime($form_state->getValue([
      'table_horaire',
      $this->rhid,
      $fieldName,
    ]));
    $value = \DateTime::createFromFormat(
      "Y-m-d H:i", $date->format('Y-m-d ') . $heure->format('H:i'));
    $reservationHoraire->setHeure($fieldName, $value);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function deleteLineAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $reservationHoraire = ReservationHoraire::load($this->rhid);
    $reservationHoraire->delete();

    $response->addCommand(new InvokeCommand('.disponibilite-horaire-table-line-form-' . $this->rhid, 'remove'));

    /*$trigger = $form_state->getTriggeringElement();
    $title = "Confirmation de suppression";
    $content['#markup'] = "L'horaire a bien été supprimé.";
    $content['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $response->addCommand(
    new \Drupal\Core\Ajax\OpenModalDialogCommand\OpenModalDialogCommand(
    $title, $content, [
    'width' => '600',
    ]
    ));*/

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
