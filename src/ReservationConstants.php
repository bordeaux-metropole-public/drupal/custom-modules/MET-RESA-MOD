<?php

namespace Drupal\reservation;

/**
 * Reservation module's contants.
 */
class ReservationConstants {

  /**
   * Module's name.
   */
  const MODULE_NAME = 'reservation';

  /**
   * Module log channel's name.
   */
  const LOG_CHANNEL = self::MODULE_NAME;

  /**
   * Module settings name.
   */
  const MODULE_SETTINGS = self::MODULE_NAME . '.settings';

  /**
   * Nombre par défaut d'accompagnants.
   */
  const DEFAULT_NB_ACCOMPAGNANTS = 10;

  /**
   * Permission de visualiser ses demandes ????
   */
  const VIEW_OWN_DEMANDE_PERMISSION = 'visualisation propre demande';

  /**
   * Permission d'administration des demandes.
   */
  const ADMIN_DEMANDE_PERMISSION = 'administer reservation demande';

  /**
   * Permission de gestion manuelle des demandes.
   */
  const MANUELLE_DEMANDE_PERMISSION = 'creation manuelle demande';

  /**
   * Permission de gestion des demandes des ressources de l'utilisateur.
   */
  const MANAGE_OWN_RESERVATIONS_PERMISSION = 'manage my own resources reservations';

  /**
   * Permission de gestion des disponibilites des ressources de l'utilisateur.
   */
  const MANAGE_OWN_AVAILABILITIES_PERMISSION = 'manage my own resources availabilities';

  /**
   * Permission de gestion des demandes de toutes les ressources.
   */
  const MANAGE_ALL_DEMANDES_PERMISSION = 'administer reservation demande';

  /**
   * Permission de gestion des disponibilites de toutes les ressources.
   */
  const MANAGE_ALL_DISPOS_PERMISSION = 'administer reservation disponibilite';

}
